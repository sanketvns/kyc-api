<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => 'cors', 'prefix' => '/v1'], function () {
    Route::post('/register', 'UserController@create');
    Route::post('/email-avail', "UserController@emailAvailibity");

    // Route::post('/lead-entry', 'UserController@callLeadentry');

    Route::post('/login', 'UserController@authenticate');
    Route::post('/change-password', 'UserController@changePassword');
    Route::post('/logout', 'UserController@logout');
    Route::post('/check-email', 'UserController@checkEmail');
    Route::post('/forgot-password', 'UserController@forgotPassword');//generate token
    Route::post('/reset-password', 'UserController@resetPassword');//reset password

    Route::post('/update-bank-details', 'BankDetailsController@updateBankDetails');
    Route::post('/get-bank-details', 'BankDetailsController@getBankDetails');
    
    // Route::post('/bank-details-ifsc', 'BankDetailsController@getBankDetailsViaIFSC');

    Route::post('/trading-plan-summary', 'TradingPlanController@getTradingPlanDetails');
    Route::post('/update-plan-summary', 'TradingPlanController@updateTradingPlanDetails');

    // Route::post('/post-otp-adhaar', 'IdentityController@postOtpAdhaar');
    // Route::post('/get-otp-adhaar', 'IdentityController@getOtpAdhaar');

    Route::post('/get-pan-details', 'IdentityController@getUserDetailsViaCLVKRA');
    Route::post('/fetch-pan-details', 'IdentityController@fetchUserDetailsViaCLVKRA');

    Route::post('/update-user-details', 'IdentityController@updateUserDetails');
    Route::post('/get-user-details', 'IdentityController@getUserDetails');

    Route::post('/upload-ipv', 'DocumentController@uploadIPV');
    Route::post('/upload-doc', 'DocumentController@uploadDocument');

    Route::get('/get-offer-list', 'PaymentController@getOfferDetails');
    Route::post('/payment', 'PaymentController@calcPayment');

    Route::post('/client-code', 'IdentityController@getClientCode');

    Route::post('/pdf', 'PdfGeneratorController@generatePdf');

    Route::post("/esign", 'EsignController@esginRequest');

    Route::post("/esignRes/{status}", 'EsignController@esginResponse');

    Route::post('/fetch-pan-name', 'NsdlController@getPanName');

    Route::post('/fetch-bank', 'RbiController@getBankDetails');

    Route::get('/plan-list', 'BaseMasterController@getPlanList');
});
