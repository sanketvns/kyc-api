<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offers extends Model
{
    protected $connection='cvl_kra';
    protected $table='offers';
}
