<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

require_once app_path().'/helper/respond.php';

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //return fail(1, "Bad Url", "C-RTUL-00");
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

        return $next($request);
    }
}
