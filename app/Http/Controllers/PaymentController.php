<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BO\PaymentBO;

require_once app_path()."/helper/respond.php";
require_once app_path()."/helper/constants.php";

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->payment_bo=new PaymentBO();
    }

    public function getOfferDetails()
    {
        $data=$this->payment_bo->getOfferDetails();
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } else {
            return fail($data['status'], $data['msg'], $data['msg_code']);
        }
    }

    public function calcPayment(Request $req)
    {
        $data=$this->payment_bo->calcPayment($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } else {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
