<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BO\IdentityBO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/respond.php";

class IdentityController extends Controller
{
    public function __construct()
    {
        $this->identity_bo=new IdentityBO();
    }

    public function postOtpAdhaar(Request $req)
    {
        $data=$this->identity_bo->postOtpAdhaar($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], FUNCTION_CODE.'postOtpAdhaar'.CONTRL, $data['data']);//need to add constant
        }
    }

    public function getOtpAdhaar(Request $req)
    {
        $data=$this->identity_bo->getOtpAdhaar($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function getUserDetailsViaCLVKRA(Request $req)
    {
        $data=$this->identity_bo->getUserDetailsViaCLVKRA($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function fetchUserDetailsViaCLVKRA(Request $req)
    {
        $data=$this->identity_bo->fetchUserDetailsViaCLVKRA($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], "Data from PAN found", SUCCESS_CODE, $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function updateUserDetails(Request $req)
    {
        $data=$this->identity_bo->updateUserDetails($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } else {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function getUserDetails(Request $req)
    {
        $data=$this->identity_bo->getUserDetails($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } else {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function getClientCode(Request $req)
    {
        $data=$this->identity_bo->getClientCode($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } else {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
