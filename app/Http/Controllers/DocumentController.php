<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BO\DocumentBO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/respond.php";

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->document_bo=new DocumentBO();
    }

    public function uploadIPV(Request $req)
    {
        $data=$this->document_bo->uploadIPV($req);
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code']);
        } else {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function uploadDocument(Request $req)
    {
        $data=$this->document_bo->uploadDocument($req);
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code']);
        } else {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
