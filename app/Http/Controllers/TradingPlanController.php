<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\BO\TradingPlanBO;
use Exception;
use Log;

require_once app_path()."/helper/respond.php";
require_once app_path()."/helper/constants.php";

class TradingPlanController extends Controller
{
    public function __construct()
    {
        $this->trading_plan_bo=new TradingPlanBO();
    }
    public function updateTradingPlanDetails(Request $req)
    {
        $data=$this->trading_plan_bo->updateTradingPlanDetails($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
    public function getTradingPlanDetails(Request $req)
    {
        $data=$this->trading_plan_bo->getTradingPlanDetails($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function getPlanList()
    {
        $data=$this->trading_plan_bo->getPlanList();
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
