<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BO\RbiBO;

require_once app_path()."/helper/respond.php";
require_once app_path()."/helper/constants.php";

class RbiController extends Controller
{
    public function __construct()
    {
        $this->rbi_bo=new RbiBO();
    }

    public function getBankDetails(Request $req)
    {
        $data=$this->rbi_bo->getBankDetails($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
