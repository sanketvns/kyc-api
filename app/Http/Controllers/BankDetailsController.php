<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\BO\BankDetailsBO;
use Exception;
use Log;

require_once app_path()."/helper/respond.php";
require_once app_path()."/helper/constants.php";

class BankDetailsController extends Controller
{
    public function __construct()
    {
        $this->bank_details_bo=new BankDetailsBO();
    }

    // public function create(Request $req)
    // {
    //     $data=$this->bank_details_bo->create($req->all());
    //     if ($data['status']==SUCCESS_STATUS) {
    //         return success($data['status'], $data['msg'], $data['msg_code']);   /**NO USE**/
    //     } elseif ($data['status']==FAIL_STATUS) {
    //         return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
    //     }
    // }

    public function updateBankDetails(Request $req)
    {
        $data=$this->bank_details_bo->updateBankDetails($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function getBankDetails(Request $req)
    {
        $data=$this->bank_details_bo->getBankDetails($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function getBankDetailsViaIFSC(Request $req)
    {
        $data=$this->bank_details_bo->getBankDetailsViaIFSC($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
