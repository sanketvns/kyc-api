<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\BO\UserBO;
use Exception;
use Log;

require_once app_path()."/helper/respond.php";
require_once app_path()."/helper/constants.php";

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->user_bo=new UserBO();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        try {
            $data=$this->user_bo->create($req->all());
            if ($data['status']==SUCCESS_STATUS) {
                return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
            } elseif ($data['status']==FAIL_STATUS) {
                return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
            } //elseif ($data['status']==ERROR_STATUS) {
            //      return error("ERROR");
            // }
        } catch (Exception $ex) {
                Log::error($ex);
        }
    }

    public function emailAvailibity(Request $req)
    {
        try {
            $data=$this->user_bo->emailAvailibity($req->all());
            if ($data['status']==FAIL_STATUS) {
                return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
            } else {
                return success($data['status'], $data['msg'], $data['msg_code']);
            }
        } catch (Exception $ex) {
            Log::error($ex);
        }
    }

    public function callLeadentry(Request $req)
    {
        try {
            $data=$this->user_bo->callLeadentry($req->all());
            if ($data['status']==FAIL_STATUS) {
                return fail($data['status'], $data['msg'], $data['msg_code']);
            } else {
                return success($data['status'], $data['msg'], $data['msg_code']);
            }
        } catch (Exception $ex) {
            Log::error($ex);
        }
    }

    public function changePassword(Request $req)
    {
        $data=$this->user_bo->changePassword($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
    public function authenticate(Request $req)
    {
        try {
            $data=$this->user_bo->authenticate($req->all());
            if ($data['status']==SUCCESS_STATUS) {
                return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
            } elseif ($data['status']==FAIL_STATUS) {
                return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
            }
        } catch (Exception $ex) {
            Log::error($ex);
        }
    }

    public function checkEmail(Request $req)
    {
        try {
            $data=$this->user_bo->checkEmail($req->all());
            if ($data['status']==FAIL_STATUS) {
                return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
            } else {
                return success($data['status'], $data['msg'], $data['msg_code']);
            }
        } catch (Exception $ex) {
            Log:error($ex);
        }
    }

    public function logout(Request $req)
    {
      // print_r($req->all());exit;
        try {
            $data=$this->user_bo->logout($req->all());
            if ($data['status']==SUCCESS_STATUS) {
                return success($data['status'], $data['msg'], $data['msg_code']);
            } elseif ($data['status']==FAIL_STATUS) {
                return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
            }
        } catch (Exception $e) {
              Log::error($e);
        }
    }

    public function forgotPassword(Request $req)
    {
        $data=$this->user_bo->forgotPassword($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }

    public function resetPassword(Request $req)
    {
        $data=$this->user_bo->resetPassword($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
