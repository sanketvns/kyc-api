<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BO\BaseMasterBO;

require_once app_path()."/helper/respond.php";
require_once app_path()."/helper/constants.php";

class BaseMasterController extends Controller
{
    public function __construct()
    {
        $this->base_master_bo=new BaseMasterBO();
    }
    public function getPlanList()
    {
        $data=$this->base_master_bo->getPlanList();
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
