<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BO\NsdlBO;

require_once app_path()."/helper/respond.php";
require_once app_path()."/helper/constants.php";

class NsdlController extends Controller
{
    public function __construct()
    {
        $this->nsdl_bo=new NsdlBO();
    }

    public function getPanName(Request $req)
    {
        $data=$this->nsdl_bo->getPanName($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
