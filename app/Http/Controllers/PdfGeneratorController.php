<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BO\PdfGeneratorBO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/respond.php";

class PdfGeneratorController extends Controller
{
    public function __construct()
    {
        $this->pdf_generator=new PdfGeneratorBO();
    }
    public function generatePdf(Request $req)
    {
        $data=$this->pdf_generator->generatePdf($req->all());
        if ($data['status']==SUCCESS_STATUS) {
            return success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        } elseif ($data['status']==FAIL_STATUS) {
            return fail($data['status'], $data['msg'], $data['msg_code'], $data['data']);
        }
    }
}
