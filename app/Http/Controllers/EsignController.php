<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\BO\EsignBO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/respond.php";

class EsignController extends Controller
{
    public function __construct()
    {
        $this->esign_bo=new EsignBO();
    }

    public function esginRequest(Request $req)
    {
        try {
            $data=$this->esign_bo->eMudraRequest($req->all());
            if ($data['status']==SUCCESS_STATUS) {
                return View::make('esign.esign-emudra')->with('data',$data['data']);// success($data['status'], $data['msg'], $data['msg_code'], $data['data']);
            } else {
                return View::make('esign.esign-response')->with('data',$data);
            }
        } catch (Exception $ex) {
            Log::error("[EsignController_esginRequest] ".$ex);
        }
    }

    public function esginResponse(Request $req,$status='failed')
    {
        $data=$this->esign_bo->eSignMudraResponse($req,$status);
        return View::make('esign.esign-response')->with('data',$data);
        // $this->callcurl($data);
        // if ($data['status']==SUCCESS_STATUS) {
        //     return success($data['status'], $data['msg'], $data['msg_code']);
        // } else {
        //     return fail($data['status'], $data['msg'], $data['msg_code']);
        // }
    }
/*
    public function callcurl($data)
    {
        try {
            $ch = curl_init();
            $curlConfig = array(
              CURLOPT_URL            => "http://localhost/",//$data['url'],
              CURLOPT_POST           => true,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_POSTFIELDS     => array(
                'status' => $data['status'],
                'msg' => $data['msg'],
                'msg_code'=>$data['msg_code']
              )
            );
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
        } catch(Exception $ex) {
            Log::error("[EsignController_callcurl] ".$ex);
        }
    }*/
}
