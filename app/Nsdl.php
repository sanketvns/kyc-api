<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nsdl extends Model
{
    protected $table='nsdl_response';
    protected $fillable=['pan','pan_status','first_name','last_name','middle_name','pan_title','dob_incorp',
    'fathers_last_name','fathers_first_name','fathers_middle_name','last_update_date','filler1','filler2','filler3'];
}
