<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table='payment_details';
    protected $fillable=['user_id','pan','trade_count','discount','txn_id','name',
    'email','amt','final_amt','coupon_code','coupon_expiry_date','product_info',
    'mobile'];
}
