<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// use Illuminate\Database\Eloquent\SoftDeletes;
class Fatca extends Model
{
    protected $table='fatca_user';
    protected $fillable=['user_id','country','identification','identification_type'];
}
