<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;
use Exception;
use App\Nsdl;
use App\Identity;

class NsdlDAO extends Model
{
    public function checkNsdl($pan)
    {
        try {
            return Nsdl::where(['pan'=>$pan,'pan_status'=>'E'])->first();
        } catch (Exception $ex) {
            Log::error('[NsdlDAO_checkNsdl] '.$ex);
        }
    }
    public function updateOrCreate($pan, $data)
    {
        try {
            return Nsdl::updateOrCreate(
                ['pan'=>$pan],
                [
                  'pan_status'=>$data[2],
                  'first_name'=>$data[4],
                  'middle_name'=>$data[5],
                  'last_name'=>$data[3],
                  'pan_title'=>array_key_exists(6, $data)?$data [6]:'',
                  'dob_incorp'=>array_key_exists(7, $data)?$data [7]:'',
                  'fathers_last_name'=> array_key_exists(8, $data)?$data[8]:'',
                  'fathers_first_name'=> array_key_exists(9, $data)?$data[9]:'',
                  'fathers_middle_name'=> array_key_exists(10, $data)?$data[10]:'',
                  'last_update_date'=> array_key_exists(11, $data)?$data[11]:'',
                  'filler1'=> array_key_exists(12, $data)?$data[12]:'',
                  'filler2'=> array_key_exists(13, $data)?$data[13]:'',
                  'filler3'=> array_key_exists(14, $data)?$data[14]:''
                ]
            );
        } catch (Exception $ex) {
            Log::error('[NsdlDAO_checkNsdl] '.$ex);
        }
    }

    public function saveName($saveName, $id)
    {
        try {
            $user=Identity::where('user_id', $id)->first();
            $user->name(ucwords(strtolower($saveName)));
            $user->save();
            return $user;
        } catch (Exception $ex) {
            Log::error('[NsdlDAO_saveName] '.$ex);
        }
    }
}
