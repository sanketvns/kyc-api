<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;
use Exception;

class RbiDAO extends Model
{
    public function getBankDetails($ifsc)
    {
        try {
            return DB::connection('cvl_kra')->table('bank_master_rbi')->where('IFSC', $ifsc)->first();
        } catch (Exception $ex) {
            Log::error('[RbiDAO_checkNsdl] '.$ex);
        }
    }
}
