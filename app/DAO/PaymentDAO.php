<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;
use App\Offers;
use App\User;
use App\TradingPlan;
use App\Identity;
use App\Payment;

class PaymentDAO extends Model
{
    public function getOfferDetails()
    {
        $date=date('Y-m-d');
        try {
            $data=Offers::where([['flag',1],['from_date','<=',$date],['to_date','>=',$date]])
            ->select('name', 'description', 'discount', 'code', 'from_date as start date', 'to_date as end date', 'off', 'min_val', 'trade_count')
            ->orderBy('end date')
            ->orderBy('off', 'desc')
            ->get();
            $discount=$code=array();
            if ($data) {
                return $data->toArray();
            } else {
                return null;
            }
        } catch (Exception $ex) {
            Log::error('[PaymentDAO_getOfferDetails] '.$ex);
        }
    }

    public function getUserByEmail($email)
    {
        try {
            return User::where('email', $email)->first();
        } catch (Exception $ex) {
            Log::error('[PaymentDAO_getUserByEmail] '.$ex);
        }
    }

    public function getTradingDetails($id)
    {
        try {
            $data=TradingPlan::where('user_id', $id)->first();
            if ($data) {
                return [
                  'Equity_Currency'=>empty($data->equity_flag)?$data->currency_flag:$data->equity_flag,
                  'Commodity'=>$data->commodity_flag, //array key always equal to tbl 'trading_plan_charges' column name
                  'Demat'=>$data->demat_flag
                ];
            }
            return null;
        } catch (Exception $ex) {
            Log::error('[PaymentDAO_getTradingDetails] '.$ex);
        }
    }

    public function getCharges()
    {
          return DB::connection('cvl_kra')->table('trading_plan_charges')->first();
    }

    public function getTradingPlan($id)
    {
        try {
            return TradingPlan::where('user_id', $id)->select('equity_flag', 'commodity_flag', 'currency_flag', 'demat_flag')->first();
        } catch (Exception $ex) {
            Log::error('[PaymentDAO_getTradingPlan] '.$ex);
        }
    }

    public function getIdentityById($id)
    {
        try {
            return Identity::where('user_id', $id)->select('name', 'pan')->first();
        } catch (Exception $ex) {
            Log::error('[PaymentDAO_getIdentityById]'.$ex);
        }
    }

    public function createPaymentEntry($pay)
    {
        try {
            return Payment::create($pay);
        } catch (Exception $ex) {
            Log::error('[PaymentDAO_createPaymentEntry] '.$ex);
        }
    }
}
