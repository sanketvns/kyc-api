<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\AadharResponse;
use Log;

require_once app_path().'/helper/constants.php';

class AdhaarResponseDAO extends Model
{
    public function checkAadharResponseApi($aadhar_id)
    {
        try {
            $data = AadharResponse::where('UID', $aadhar_id)->first();
            return $data;
        } catch (Exception $exception) {
            Log::error("[AdhaarResponseDAO_checkAadharResponseApi] ".$exception);
        }
    }
    public function insertAadharResponseApi($aadhar_id)
    {
        try {
            $insert_status = AadharResponse::insert([
              'UID' => $aadhar_id,
              'created_at'=>date('Y-m-d H:i:s')
            ]);
            return $insert_status;
        } catch (Exception $exception) {
            Log::error("[AdhaarResponseDAO_insertAadharResponseApi] ".$exception);
        }
    }
    public function updateAadharResponseApi($aadhar_id, $personal_info, $address_details)
    {
        try {
            $aadhar = AadharResponse::where('UID', $aadhar_id)->first();
            if ($aadhar) {
                $aadhar->NAME = array_key_exists("NAME", $personal_info)?$personal_info ['NAME']:null;
                $aadhar->DOB = array_key_exists("DOB", $personal_info)?$personal_info['DOB']:null;
                $aadhar->EMAIL = array_key_exists("EMAIL", $personal_info)?$personal_info['EMAIL']:null;
                $aadhar->GENDER = array_key_exists("GENDER", $personal_info)?$personal_info ['GENDER']:null;
                $aadhar->PHONE = array_key_exists("PHONE", $personal_info)?$personal_info ['PHONE']:null;
                $aadhar->UID = $aadhar_id;
                $aadhar->CO = array_key_exists("CO", $address_details)?$address_details ['CO']:null;
                $aadhar->DIST = array_key_exists("DIST", $address_details)?$address_details ['DIST']:null;
                $aadhar->PC = array_key_exists("PC", $address_details)?$address_details ['PC']:null;
// 				if ($address_details ['PO'])
                $aadhar->PO = array_key_exists("PO", $address_details)?$address_details ['PO']:null;
// 				if (isset($address_details ['HOUSE']))
                $aadhar->HOUSE = array_key_exists("HOUSE", $address_details)?$address_details ['HOUSE']:null;
// 				if (isset($address_details ['STREET']))
                $aadhar->STREET = array_key_exists("STREET", $address_details)?$address_details ['STREET']:null;
                $aadhar->STATE = array_key_exists("STATE", $address_details)?$address_details ['STATE']:null;
                $aadhar->SUBDIST = array_key_exists("SUBDIST", $address_details)?$address_details ['SUBDIST']:null;
                $aadhar->VTC = array_key_exists("VTC", $address_details)?$address_details ['VTC']:null;
                $aadhar->VTCCODE = array_key_exists("VTCCODE", $address_details)?$address_details ['VTCCODE']:null; // date('Y-m-d H:i:s');
      /**/
                $address = (isset($address_details['HOUSE'])?$address_details['HOUSE'].', ':'').
                (isset($address_details['LM'])?$address_details['LM'].', ':' ').
                (isset($address_details['STREET'])?$address_details['STREET'].', ':' ').
                (isset($address_details['LOC'])?$address_details['LOC']:'');
                $aadhar->ADDR = $address;
                return $aadhar->save();
            }
        } catch (Exception $exception) {
            Log::error('[AdhaarResponseDAO_updateAadharResponseApi] '.$exception);
        }
    }
}
