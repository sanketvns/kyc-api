<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
use App\BankDetails;
use App\Identity;
use App\TradingPlan;
use Log;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

require_once app_path()."/helper/constants.php";

class UserDAO extends Model
{
    public function create($request)
    {
        try {
            $user = User::create([
              'name' => trim($request['name']),
              'phone'=>$request['phone'],
              'email' => trim(strtolower($request['email'])),
              'password' => \Hash::make($request['password'])
            ]);
            $this->defaultValues($user->id);
            return $user;
        } catch (Exception $ex) {
            Log::error("[UserDAO_register] ".$ex);
        }
    }

    public function authenticate($email, $password)
    {
        $user = User::where('email', $email)->first();
        try {
            if ($user->api_token!=null) {
                try {
                    JWTAuth::setToken($user->api_token)->invalidate();
                    $user->api_token=null;
                    $user->save();
                } catch (JWTException $ex) {
                    $user->api_token=null;
                    $user->save();
                    return [
                      'status'=>FAIL_STATUS,
                      'msg'=>"Token is expired"
                    ];
                }
            }
            $credentials = ['email' => $email, 'password' => $password];
            if (!$token = JWTAuth::attempt($credentials)) {
                return [
                  'status'=>FAIL_STATUS,
                  'msg'=>null
                ];
            }
            $user = JWTAuth::toUser($token);
            $user->api_token = $token;
            $user->save();
            return [
              'status'=>SUCCESS_STATUS,
              'data'=>[
                'email'=>$user->email,
                'api_token'=>$user->api_token
                ]
            ];
        } catch (Exception $ex) {
            Log::error('[UserDAO_authenticate] '.$ex);
        }
    }

    public function changePassword($id, $req)
    {
        try {
            $user = User::where('id', $id)->first();
            if (\Hash::check($req['old_password'], $user->password)) {
                $user->password=\Hash::make($req['password']);
                $user->save();
                return [
                  'status'=>SUCCESS_STATUS,
                  'data'=>['email'=>$user->email]
                ];
            } else {
                  return [
                    'status'=>FAIL_STATUS,
                    'msg'=>'Old password is wrong'
                  ];
            }
        } catch (JWTException $ex) {
            return [
            'status'=>FAIL_STATUS,
            'msg'=>null
            ];
        } catch (Exception $ex) {
            Log::error('[UserDAO_changePassword] '.$ex);
        }
    }

    public function logout($req)
    {
        try {
            $user = JWTAuth::toUser($req['api_token']);
            $user->api_token = null;
            $user->save();
            JWTAuth::setToken($req['api_token'])->invalidate();
            return [
              'status' => SUCCESS_STATUS
            ];
        } catch (JWTException $e) {
            log::error('[UserDAO_logout] '.$e);
            $user->api_token = null;
            $user->save();
            return [
              'status'=>FAIL_STATUS
            ];
        } catch (Exception $ex) {
            Log::error('[UserDAO_logout] '.$ex);
        }
    }

    public function getUserViaEmailId($email)
    {
        try {
            $user = User::where('email', $email)->first();
            return $user;
        } catch (Exception $ex) {
            Log::error("[USerDAO_getUserViaEmailId] ".$ex);
        }
    }

    public function addTokenToResetPwd($code, $email)
    {
        try {
            $user=User::where('email', $email)->first();
            $user->resetpassword=$code.time();
            $user->save();
            return $user;
        } catch (Exception $ex) {
            Log::error("[UserDAO_addTokenToResetPwd] ".$ex);
        }
    }

    public function getTokenfromDB($token)
    {
        try {
            $user=User::where('resetpassword', $token)->first();
            return $user;
        } catch (Exception $ex) {
            Log::error("[UserDAO_getTokenfromDB] ".$ex);
        }
    }

    public function resetPassword($token, $password)
    {
        try {
            $user=User::where('resetpassword', $token)->first();
            $user->password=\Hash::make($password);
            $user->resetpassword=null;
            $user->save();
            return $user;
        } catch (Exception $ex) {
            Log::error("[UserDAO_resetPassword] ".$ex);
        }
    }

    private function defaultValues($user_id)
    {
        try {
            $bank_details=BankDetails::create(['user_id'=>$user_id]);
            $trade_details=TradingPlan::create(['user_id'=>$user_id]);
            $identity_details=Identity::create(['user_id'=>$user_id]);
        } catch (Exception $ex) {
            Log::error("[UserDAO_defaultValues] ".$ex);
        }
    }
}
