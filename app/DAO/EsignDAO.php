<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;
use Exception;
use App\User;
use App\Identity;
use App\Esign;

class EsignDAO extends model
{
    public function userDetails($id)
    {
        try {
            return User::where('id', $id)->first();
        } catch (Exception $ex) {
            Log::error("[EsignDAO_userDetails] ".$ex);
        }
    }

    public function identityDetails($id)
    {
        try {
            return Identity::where('user_id', $id)->first();
        } catch (Exception $ex) {
            Log::error("[EsignDAO_identityDetails] ".$ex);
        }
    }

    public function createEsignEntry($id, $ref, $url)
    {
        try {
            return Esign::create(['user_id'=>$id,
                                  'Referencenumber'=>$ref,
                                  'callbackurl'=>$url]);
        } catch (Exception $ex) {
            Log::error("[EsignDAO_createEsignEntry] ".$ex);
        }
    }
    public function getEsignEntry($Referencenumber)
    {
        try {
            return Esign::where('Referencenumber',$Referencenumber)->orderBy('updated_at','desc')->first();
        } catch (Exception $ex) {
            Log::error("[EsignDAO_getEsignEntry] ".$ex);
        }
    }

    public function updateKycstatus($id)
    {
        try {
            return User::where('id', $id)->update([
                'kyc_status'=>1
            ]);
        } catch (Exception $ex) {
            Log::error("[EsignDAO_updateKycstatus] ".$ex);
        }
    }
}
