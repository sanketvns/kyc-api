<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
use App\BankDetails;
use App\Identity;
use App\TradingPlan;
use App\Fatca;
use App\Payment;
use Log;

class PdfGeneratorDAO extends Model
{
    public function getAllFlag($id)
    {
        try {
            return User::where('id', $id)->select('identity_flag', 'trading_flag', 'bank_flag', 'doc_flag')->first();
        } catch (Exception $ex) {
            Log::error("[PdfGeneratorDAO_getAllFlag] ".$ex);
        }
    }

    public function getIdentity($id)
    {
        try {
            return Identity::where('user_id', $id)->first();
        } catch (Exception $ex) {
            Log::error("[PdfGeneratorDAO_getIdentity] ".$ex);
        }
    }

    public function getTrading($id)
    {
        try {
            return TradingPlan::where('user_id', $id)->first();
        } catch (Exception $ex) {
            Log::error("[PdfGeneratorDAO_getTrading] ".$ex);
        }
    }

    public function getBankDetails($id)
    {
        try {
            return BankDetails::where('user_id', $id)->first();
        } catch (Exception $ex) {
            Log::error("[PdfGeneratorDAO_getBankDetails] ".$ex);
        }
    }

    public function getFatca($id)
    {
        try {
            return Fatca::where(['user_id'=>$id,'deleted_at'=>0])
            ->select('country', 'identification', 'identification_type')->get()->toArray();
        } catch (Exception $ex) {
            Log::error("[PdfGeneratorDAO_getFatca] ".$ex);
        }
    }
    public function getUser($id)
    {
        try {
            return User::where('id', $id)->first();
        } catch (Exception $ex) {
            Log::error("[PdfGeneratorDAO_getUser] ".$ex);
        }
    }

    public function getPlanDetails()
    {
        try {
            return DB::connection('mysql')->table("plans")->get()->toArray();
        } catch (Exception $ex) {
            Log::error("[PdfGeneratorDAO_getUser] ".$ex);
        }
    }

    public function getPayment($id)
    {
        try {
            return Payment::where('user_id', $id)->orderBy('created_at', 'desc')->first();
        } catch (Exception $ex) {
            Log::error("[PdfGeneratorDAO_getPayment] ".$ex);
        }
    }
}
