<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
use App\TradingPlan;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

require_once app_path()."/helper/constants.php";

class TradingPlanDAO extends Model
{
    public function getTradingPlanDetails($id)
    {
        try {
            $trading_plan=TradingPlan::where('user_id', $id)->first();
            return ['status'=>SUCCESS_STATUS,'data'=>$trading_plan];
        } catch (Exception $ex) {
            Log::error("[TradingPlanDAO_getTradingPlanDetails] ".$ex);
        }
    }

    public function updateTradingPlanDetails($id, $req)
    {
        try {
            $user=User::where('id', $id)->first();
            $trading_plan=TradingPlan::where('user_id', $id)->first();
            $trading_plan->equity_flag=$req['equity_f'];
            $trading_plan->commodity_flag=$req['commodity_f'];
            $trading_plan->currency_flag=$req['currency_f'];
            $trading_plan->equity=$req['equity_seg'];
            $trading_plan->currency=$req['currency_seg'];
            $trading_plan->commodity=$req['commodity_seg'];
            $trading_plan->plan_equity=$req['equity_plan'];
            $trading_plan->plan_currency=$req['currency_plan'];
            $trading_plan->plan_commodity=$req['commodity_plan'];
            $trading_plan->demat_flag=$req['demat_flag'];
            $trading_plan->demat_name=$req['demat_acc_name'];
            $trading_plan->demat_dp=$req['demat_DP_no'];
            $trading_plan->demat_bo=$req['demat_BO_no'];
            $trading_plan->sec_demat_name=$req['sec_demat_acc_name'];
            $trading_plan->sec_demat_dp=$req['sec_demat_DP_no'];
            $trading_plan->sec_demat_bo=$req['sec_demat_BO_no'];
            $trading_plan->DIS_flag=$req['DIS_flag'];
            $user->trading_flag=1;
            $user->save();
            $trading_plan->save();
            return ['status'=>SUCCESS_STATUS];
        } catch (Exception $ex) {
            Log::error("[TradingPlanDAO_updateTradingPlanDetails] ".$ex);
        }
    }
}
