<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\BankDetails;
use App\User;
use Log;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class BankDetailsDAO extends Model
{
    public function getBankDetails($id)
    {
        try {
            $bank_details=BankDetails::where('user_id', $id)->first();
            return ['status'=>SUCCESS_STATUS,'data'=>$bank_details];
        } catch (Exception $e) {
            Log::error("[BankDetailsDAO_getBankDetails] ".$ex);
        }
    }

    public function updateBankDetails($id, $req)
    {
        try {
            $user=User::where('id', $id)->first();
            $bank_details=BankDetails::where('user_id', $id)->first();
            $bank_details->bankname=trim($req['bankname']);
            $bank_details->branch_address=trim($req['branch_address']);
            $bank_details->branch=trim($req['branch']);
            $bank_details->micr=trim(strtoupper($req['micr']));
            $bank_details->ifsc=trim(strtoupper($req['ifsc']));
            $bank_details->acc_type=trim($req['acc_type']);
            $bank_details->acc_no=trim($req['accno']);
            $bank_details->sec_bankname=trim($req['secbankname']);
            $bank_details->sec_branch_address=trim($req['secbranch_address']);
            $bank_details->sec_branch=trim($req['secbranch']);
            $bank_details->sec_acc_no=trim($req['secaccno']);
            $bank_details->sec_acc_type=trim($req['secacc_type']);
            $bank_details->sec_MICR=trim(strtoupper($req['secmicr']));
            $bank_details->sec_IFSC=trim(strtoupper($req['secifsc']));
            $bank_details->save();
            $user->bank_flag=1;
            $user->save();
            $this->addBankDeatailsInTemp($req);
            return ['status'=>SUCCESS_STATUS];
        } catch (Exception $ex) {
            Log::error("[BankDetailsDAO_getBankDetails] ".$ex);
        }
    }

    public function getBankDetailsViaIFSC($req)
    {
        try {
            $data=DB::connection('old-kyc')
            ->table('bank_master_rbi')
            ->where('IFSC', '=', $req)->first();
            return $data;
        } catch (Exception $ex) {
            Log::error("[BankDetailsDAO_getBankDetailsViaIFSC] ".$ex);
        }
    }

    private function addBankDeatailsInTemp($req)
    {
        $keys=array('ifsc','secifsc');//dynamic
        foreach ($keys as $key) {
            if ($req[$key]!=null&&$this->getBankDetailsViaIFSC($req[$key])==null) {
                DB::connection('mysql')->table('bank_master_rbi')->insert([
                  'IFSC'=>strtoupper($req[$key]),
                  'BANK'=>$key=='ifsc'?$req['bankname']:$req['secbankname'],
                  'MICR'=>$key=='ifsc'?strtoupper($req['micr']):strtoupper($req['secmicr']),
                  'BRANCH'=>$key=='ifsc'?$req['branch']:$req['secbranch']
                ]);
            }
        }
    }
}
