<?php
namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\AadharResponse;
use Log;

class BaseMasterDAO extends model
{
    public function getPlanList()
    {
        try {
            return DB::connection('mysql')->table('plans')
                      ->select('plan_name as Plan Code', 'plan_desc as Description', 'plan_value as Value in INR')
                      ->get()->toArray();
        } catch (Exception $ex) {
            Log::error("[BaseMasterDAO_getPlanList] ".$ex);
        }
    }
}
