<?php
namespace App\DAO;

use SimpleXmlElement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Fatca;
use Log;
use App\User;
use App\Identity;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/common.php";

class IdentityDAO extends Model
{
    public function updateOtpTxnIdAndTimestamp($id, $data)
    {
        try {
            $user=Identity::where('user_id', $id)->first();
            $user->otp_txn_id=$data['txn_id'];
            $user->otp_timestamp=$data['ts'];
            $user->save();
        } catch (Exception $ex) {
            Log::error(" [IdentityDAO_updateOtpTxnIdAndTimestamp] ".$ex);
        }
    }

    // public function getOtpAdhaar($req)
    // {
    //     try {
    //         $user = JWTAuth::toUser($req['api_token']);
    //         return $user->id;
    //     } catch (JWTException $ex) {
    //         return null;
    //     } catch (Exception $ex) {
    //         Log::error(" [IdentityDAO_getOtpAdhaar] ".$ex);
    //     }
    // }

    public function updatePersonalDetailsAadharResponseApi($id, $data, $aadhar_id)
    {
        Log::debug(' [IdentityDAO] ' . ' [updatePersonalDetailsAadharResponseApi] ' . "Entered in updatePersonalDetailsAadharResponseApi function.");
        try {
            $user_detail = Identity::where('user_id', $id)->first();
            if ($user_detail) {
                $user_detail->name=empty($data['NAME'])?null:$data['NAME'];
                $user_detail->adhaar_no = $aadhar_id;
                $user_detail->gender = empty($data['GENDER'])?null:$data['GENDER'];
                $user_detail->c_address = empty($data['ADDR'])?null:$data['ADDR'];
                $user_detail->c_city= empty($data ['DIST'])?null:$data ['DIST'];
                $user_detail->c_pincode = empty($data ['PC'])?null:$data ['PC'];
                $user_detail->father_name =empty($data ['CO'])?null:$data ['CO'];
                $user_detail->c_state = empty($data ['STATE'])?null:$data ['STATE'];
                $user_detail->c_address1=empty($data['ADDR'])?null:$data['ADDR'];
                $user_detail->c_address2=empty($data['STREET'])?null:$data['STREET'];
                $user_detail->c_address3=empty($data['PO'])?null:$data['PO'];
                $user_detail->dob=empty($data['DOB'])?null:str_replace('-', '/', $data['DOB']);
                $user_detail->addr_chk_flg=1;
                $user_detail->c_address_proof='Aadhar Card';
                $user_detail->save();
                User::where('id', $id)->update(['user_group' => 3]);
                return true;
            }
        } catch (Exception $exception) {
            Log::error(" [IdentityDAO_updatePersonalDetailsAadharResponseApi] ".$ex);
        }
    }

    public function getPanDetailsViaAPI($pan)
    {
        $pan=strtoupper($pan);
        $userName = CVL_USERNAME;//'WEBVNSF';
        $PosCode = CVL_POSCODE;//'1400964330';
        $password = CVL_PASSWORD;//'EOeg1YnoQC3pjb5oB%2fl4LA%3d%3d';
        $PassKey = CVL_PASSKEY;//'vns321';

        $url = "http://www.cvlkra.com/PanInquiry.asmx/GetPanStatus"."?panNo=$pan&userName=$userName&PosCode=$PosCode&password=$password&PassKey=$PassKey";

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $response =curl_exec($handle);
        curl_close($handle);
        $xml = new SimpleXmlElement($response);

        $data = (array) $xml;

        $input_array = array();
        $APP_PAN_INQ = (array)$data['APP_PAN_INQ'];
        $APP_PAN_SUMM = (array)$data['APP_PAN_SUMM'];
        $input_array = $APP_PAN_INQ + $APP_PAN_SUMM;
        $final_array = array ();
        foreach ($input_array as $key => $val) {
            $obj_checker = is_object($val);
            if ($obj_checker == '' || $obj_checker == null) {
                $final_array [$key] = $val;
            } else {
                $obj_checker =get_object_vars($val);
                $final_array [$key] = implode(', ', $obj_checker);
            }
            $obj_checker = '';
        }
        if (empty($final_array['APP_NAME'])) {
            return null;
        }
        $final_array['inserted_at']=date('Y-m-d H:i:s');
        if ($this->getPanDetails($pan)) {
            DB::connection('cvl_kra')->table('user_pan_status')->where('APP_PAN_NO', $pan)->update($final_array);
        } else {
            DB::connection('cvl_kra')->table('user_pan_status')->insert($final_array);
        }
        return $this->getPanDetails($pan);
    }

    private function getPanDetails($pan)
    {
        return DB::connection('cvl_kra')->table('user_pan_status')->where('APP_PAN_NO', $pan)->first();
    }

    public function fetchPanDetailsViaAPI($req, $id)
    {
        try {
            $user=User::where('id', $id)->first();
            $Identity=Identity::where('user_id', $id)->first();
            $Identity->pan=strtoupper($req['pan']);
            $Identity->save();
            /*
            $getPanDetailsViaAPI=$this->getPanDetailsViaAPI(strtoupper($req['pan']));
            if ($getPanDetailsViaAPI) {
                $cvl_kra_response=DB::connection('cvl_kra')->table('CVL_KRA_RESPONSE')->where('APP_PAN_NO', strtoupper($req['pan']))->first();
                if ($cvl_kra_response) {
                      return [
                        'status'=>SUCCESS_STATUS,
                        'data'=>$cvl_kra_response
                      ];
                } else {
            */

            $dirpath=getDir1($id);
            if (file_exists($dirpath.'/response.xml')) {
                /*
                        if ($Identity->pan==$req['pan']) {
                            if ($Identity->dob!=$req['dob']) {
                                return [
                                'status'=>FAIL_STATUS,
                                'msg'=>'Date of birth is wrong',//need to contanst
                                ];
                            }
                */
                $data=$this->xmlFileParser($dirpath.'/response.xml');
                if ($data) {
                    if ($data['APP_PAN_NO']==strtoupper($req['pan'])) {
                        if ($data['APP_PAN_NO']==strtoupper($req['pan'])&&str_replace('-', '/', $data['APP_DOB_DT'])==$req['dob']) {
                            $red=Identity::where('user_id', $id)->first();
                            return [
                              'status'=>SUCCESS_STATUS,
                              'data'=>[
                                  'name'=>$red->name,
                                  'father_name'=>$red->father_name,
                                  'dob'=>$red->dob,
                                  'marital_status'=>$red->marital_status,
                                  'annual_income'=>$red->annual_income,
                                  'c_address1'=>$red->c_address1,
                                  'c_address2'=>$red->c_address2,
                                  'c_address3'=>$red->c_address3,
                                  'c_pincode'=>$red->c_pincode,
                                  'c_state'=>$red->c_state,
                                  'c_address_proof'=>$red->c_address_proof,
                                  'p_address1'=>$red->p_address1,
                                  'p_address2'=>$red->p_address2,
                                  'p_address3'=>$red->p_address3,
                                  'p_pincode'=>$red->p_pincode,
                                  'p_state'=>$red->p_state,
                                  'p_address_proof'=>$red->p_address_proof,
                              ]
                            ];
                        } else {
                            return [
                              'status'=>FAIL_STATUS,
                              'msg'=>'Date of birth is wrong'
                            ];
                        }
                    } else {
                        return $this->callBackFunction($req, $id, $user, $dirpath);
                    }
                } else {
                    Log::debug("[IdentityDAO_fetchPanDetailsViaAPI] Unable parse xml response1");
                    return [
                      'status'=>FAIL_STATUS,
                      'msg'=>'Unable parse xml response',//need to contanst
                    ];
                }
            } else {
                return $this->callBackFunction($req, $id, $user, $dirpath);
            }
            /*
                    // } elseif ($Identity->adhaar_no&&file_exists(public_path().'/uploads/'.$Identity->adhaar_no.'/response.xml')) {
                    //     if ($Identity->pan==$req['pan']) {
                    //         if ($Identity->dob!=$req['dob']) {
                    //             return [
                    //             'status'=>FAIL_STATUS,
                    //             'msg'=>'Date of birth is wrong',//need to contanst
                    //             ];
                    //         }
                    //         $data=$this->xmlFileParser(public_path().'/uploads/'.$Identity->adhaar_no.'/response.xml');
                    //         if ($data) {
                    //             return [
                    //             'status'=>SUCCESS_STATUS,
                    //             'data'=>$data
                    //             ];
                    //         } else {
                    //             Log::debug("[IdentityDAO_fetchPanDetailsViaAPI] Unable parse xml response2");
                    //             return [
                    //             'status'=>FAIL_STATUS,
                    //             'msg'=>'Unable parse xml response',//need to contanst
                    //             ];
                    //         }
                    //     } else {
                    //         return $this->callBackFunction($req, $id, $user);
                    //     }
                    // } else {
                    //     $data=$this->callCurlPaidAPI($req, $id);
                    //     print_r($data);exit;
                    //     if ($data['status']==SUCCESS_STATUS) {
                    //
                    //         $uploads_dir=getDir($id, strtoupper($req['pan']), null);
                    //         $myfile = fopen($uploads_dir."/response.xml", "w");
                    //         fwrite($myfile, $data['xml']);
                    //         fclose($myfile);
                    //         $this->updateAdditionalDetails($id, $data['data']);
                    //         if ($user->user_group==1) {
                    //             $user->user_group=2;
                    //             $user->save();
                    //         }
                    //         return[
                    //           'status'=>SUCCESS_STATUS,
                    //           'data'=>$data['data']
                    //         ];
                    //     } else {
                    //         return [
                    //         'status'=>FAIL_STATUS,
                    //         'msg'=>'PAN or DOB wrong'
                    //         ];
                    //     }
                    // }
                // }
            // } else {
            //     return [
            //     'status'=>FAIL_STATUS,
            //     'msg'=>'NPAN number is wrong'
            //     ];
            // }
            */
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_fetchPanDetailsViaAPI] '.$ex);
        }
    }

    private function callBackFunction($req, $id, $user, $uploads_dir)
    {
        try {
          $pan_status=$this->getPanDetails(strtoupper($req['pan']));
          if(!$pan_status) {
              $getPanDetailsViaAPI=$this->getPanDetailsViaAPI(strtoupper($req['pan']));
          }

            if ($pan_status||$getPanDetailsViaAPI) {
                $data=$this->callCurlPaidAPI($req, $id);
                if ($data['status']==SUCCESS_STATUS) {
                    $myfile = fopen($uploads_dir."/response.xml", "w");
                    fwrite($myfile, $data['xml']);
                    fclose($myfile);
                    chmod($uploads_dir."/response.xml", 0777);
                    $red=$this->updateAdditionalDetails($id, $data['data']);
                    if ($user->user_group==1) {
                        $user->user_group=2;
                        $user->save();
                    }
                    return[
                      'status'=>SUCCESS_STATUS,
                      'data'=>[
                          'name'=>$red->name,
                          'father_name'=>$red->father_name,
                          'dob'=>$red->dob,
                          'marital_status'=>$red->marital_status,
                          'annual_income'=>$red->annual_income,
                          'c_address1'=>$red->c_address1,
                          'c_address2'=>$red->c_address2,
                          'c_address3'=>$red->c_address3,
                          'c_pincode'=>$red->c_pincode,
                          'c_state'=>$red->c_state,
                          'c_address_proof'=>$red->c_address_proof,
                          'p_address1'=>$red->p_address1,
                          'p_address2'=>$red->p_address2,
                          'p_address3'=>$red->p_address3,
                          'p_pincode'=>$red->p_pincode,
                          'p_state'=>$red->p_state,
                          'p_address_proof'=>$red->p_address_proof,
                      ]
                    ];
                } else {
                    return [
                    'status'=>FAIL_STATUS,
                    'msg'=>'PAN or DOB wrong'
                    ];
                }
            } else {
                return [
                'status'=>FAIL_STATUS,
                'msg'=>'Data about PAN not Found'
                ];
            }
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_callBackFunction] '.$ex);
        }
    }

    private function callCurlPaidAPI($req, $id)
    {
        try{
            $panNo=$req['pan'];
            $dob=$req['dob'];

            $userName = CVL_USERNAME;//'WEBVNSF';
            $PosCode = CVL_POSCODE;//'1400964330';
            $password = CVL_PASSWORD;//'EOeg1YnoQC3pjb5oB%2fl4LA%3d%3d';
            $PassKey = CVL_PASSKEY;//'vns321';

            $xml_data = '<APP_REQ_ROOT><APP_PAN_INQ><APP_PAN_NO>'.$panNo.'</APP_PAN_NO><APP_DOB_INCORP>'.$dob.'</APP_DOB_INCORP><APP_POS_CODE>'.$PosCode.'</APP_POS_CODE><APP_RTA_CODE>'.$PosCode.'</APP_RTA_CODE><APP_KRA_CODE></APP_KRA_CODE><FETCH_TYPE>I</FETCH_TYPE></APP_PAN_INQ></APP_REQ_ROOT>';

            $options = array (
              CURLOPT_RETURNTRANSFER => true, // return web page
              CURLOPT_HEADER => 'Content-Type:application/xml',
              CURLOPT_FOLLOWLOCATION => true, // follow redirects
              CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
              CURLOPT_AUTOREFERER => true, // set referrer on redirect
              CURLOPT_CONNECTTIMEOUT => 120, // time-out on connect
              CURLOPT_TIMEOUT => 120
            );

            $url = "www.cvlkra.com/PanInquiry.asmx/SolicitPANDetailsFetchALLKRA?inputXML=$xml_data&userName=$userName&PosCode=$PosCode&password=$password&PassKey=$PassKey";

            $ch =curl_init($url);
            curl_setopt_array($ch, $options);//CALL CURL
            $content = curl_exec($ch);
            curl_close($ch);

            $xml = new SimpleXMLElement($content);
            $data =(array)$xml;
            $input_array = array();

            if (array_key_exists('APP_PAN_INQ', $data)) {
                $input_array =(array)$data['APP_PAN_INQ'];
            } elseif (array_key_exists('KYCDATA', (array)$data)) {
                $input_array =(array)$data['KYCDATA'];
            } else {
                return ['status'=>FAIL_STATUS];
            }

            $final_array = array ();
            foreach ($input_array as $key => $val) {
                $obj_checker = is_object($val);
                if ($obj_checker == '' || $obj_checker == null) {
                    $final_array [$key] = $val;
                } else {
                    $obj_checker = get_object_vars($val);
                    $final_array [$key] = implode(', ', $obj_checker);
                }
                $obj_checker = '';
            }
            if (empty($final_array['APP_NAME'])||empty($final_array)) {
                return ['status'=>FAIL_STATUS];
            } else {
                return [
                  'status'=>SUCCESS_STATUS,
                  'data'=>$final_array,
                  'xml'=>$content
                ];
            }
        } catch(Exception $ex){
            Log::error("[IdentityDAO_callCurlPaidAPI] ".$ex);
        }
    }

    private function xmlFileParser($path)
    {
        try {
            if (!file_exists($path)) {
                return null;
            }
            $xmlfile = new SimpleXMLElement(file_get_contents($path));
            $data = (array)$xmlfile;
            $input_array = array ();
            if (array_key_exists('APP_PAN_INQ', $data)) {
                $input_array = ( array ) $data ['APP_PAN_INQ'];
            } elseif (array_key_exists('KYCDATA', (array)$data)) {
                $input_array = ( array ) $data ['KYCDATA'];
            } else {
                return null;
            }
            $final_array = array ();
            foreach ($input_array as $key => $val) {
                $obj_checker = is_object($val);
                if ($obj_checker == '' || $obj_checker == null) {
                    $final_array [$key] = $val;
                } else {
                    $obj_checker = get_object_vars($val);
                    $final_array [$key] = implode(', ', $obj_checker);
                }
                $obj_checker = '';
            }
            return $final_array;
        } catch (Exception $ex) {
            Log::error("[IdentityDAO_xmlFileParser] ".$ex->meassges());
        }
    }

    public function getPanNumber($id)
    {
        try {
            $user_detail = Identity::where('user_id', $id)->first();
            return $user_detail->pan;
        } catch(Exception $ex) {
            Log::error("[IdentityDAO_getPanNumber] ".$ex);
        }
    }

    // public function getDir($id, $pan = null, $adhaar = null) //old logic of getDir
    // {
    //     $pflag=$aflag=0;
    //     try {
    //         $user_detail=Identity::where('user_id', $id)->first();
    //         if ($adhaar) {
    //             if ($user_detail->pan) {
    //                 if (file_exists(public_path().'/uploads/'.($user_detail->pan))) {
    //                     return public_path().'/uploads/'.($user_detail->pan);
    //                 } else {
    //                     $pflag=1;
    //                 }
    //             } else {
    //                 if (file_exists(public_path().'/uploads/'.$adhaar)) {
    //                     return public_path().'/uploads/'.$adhaar;
    //                 } else {
    //                     $aflag=1;
    //                 }
    //             }
    //         } elseif ($pan) {
    //             if ($user_detail->adhaar_no) {
    //                 if (file_exists(public_path().'/uploads/'.($user_detail->adhaar_no))) {
    //                     return public_path().'/uploads/'.($user_detail->adhaar_no);
    //                 } else {
    //                     $aflag=1;
    //                 }
    //             } else {
    //                 if (file_exists(public_path().'/uploads/'.$pan)) {
    //                     return public_path().'/uploads/'.$pan;
    //                 } else {
    //                     $pflag=1;
    //                 }
    //             }
    //         }
    //         if ($pflag==1) {
    //             mkdir(public_path().'/uploads/'.$pan, 0777);
    //             chmod(public_path().'/uploads/'.$pan, 0777);
    //             return public_path().'/uploads/'.$pan;
    //         }
    //         if ($aflag==1) {
    //             mkdir(public_path().'/uploads/'.$adhaar, 0777);
    //             chmod(public_path().'/uploads/'.$adhaar, 0777);
    //             return public_path().'/uploads/'.$adhaar;
    //         }
    //         Log::error('[IdentityDAO_getDir]  '.' something went wrong');
    //     } catch (Exception $ex) {
    //         Log::error($ex->getMessages());
    //     }
    // }

    public function addPANDetails($data)
    {
        try {
            $final_array=array();
            foreach ($data as $key => $value) {
                if ($key == 'APP_DOB_DT') {
                    $value = str_replace("-", "/", $value);
                }
                if ($key == 'APP_MAR_STATUS') {
                    if ($value == '01') {
                        $value = 'Married';
                    }
                    if ($value == '02') {
                        $value = 'Single';
                    }
                }
                if ($key == 'APP_OCC') {
                    if ($value>0&&$value<12||$value==99) {
                        $temp = DB::connection('cvl_kra')->table('Occupation')->where('id', $value)->first();
                        $value=ucwords(strtolower($temp->Description));//ucwords(strtolower($this->Personal_dao->getCvlKraOcc($value)));
                        if ($value == 'Others') {
                            $value = 'Other';
                        }
                    } else {
                        $value ='';
                    }
                }
                if ($key == 'APP_COR_STATE') {
                    $temp = DB::connection('cvl_kra')->table('state')->where('id', $value)->first();
                    if ($temp) {
                        $value=ucwords(strtolower($temp->description));
                    } else {
                        $value='';
                    }//ucwords(strtolower($this->Personal_dao->getCvlKraState($value)));
                }
                if ($key == 'APP_PER_STATE') {
                    $temp = DB::connection('cvl_kra')->table('state')->where('id', $value)->first();
                    if ($temp) {
                        $value=ucwords(strtolower($temp->description));
                    } else {
                        $value='';
                    }
                }
                if ($key == 'APP_COR_ADD_PROOF') {
                    if ($value=='NA') {
                        $value ='';
                    } elseif (!empty(trim($value))&&$value>0&&$value<33||$value!=30) {
                        $value = DB::connection('cvl_kra')->table('address_proof')->where('id', $value)->first();
                        $value=ucwords(strtolower($value->description));//ucwords(strtolower($this->Personal_dao->getCvlKraAddrProof($value)));
                        if ($value == 'Any Other Proof Of Address' || $value == 'Others') {
                            $value = 'Other';
                        }
                    } else {
                        $value='';
                    }
                }
                if ($key == 'APP_PER_ADD_PROOF') {
                    if ($value=='NA') {
                        $value ='';
                    } elseif ($value>0&&$value<33||$value!=30) {
                        $temp = DB::connection('cvl_kra')->table('address_proof')->where('id', $value)->first();
                        $value=ucwords(strtolower($temp->description));//ucwords(strtolower($this->Personal_dao->getCvlKraAddrProof($value)));
                        if ($value == 'Any Other Proof Of Address' || $value == 'Others') {
                            $value = 'Other';
                        }
                    } else {
                        $value='';
                    }
                }
                if ($key == 'APP_NETWORTH_DT') {
                    $value = str_replace("-", "/", $value);
                }
                if ($key == 'APP_INCOME') {
                    if ($value>0&&$value<19) {
                        $temp = DB::connection('cvl_kra')->table('annual_income')->where('id', $value)->first();
                        $value=$temp->description;
                    } else {
                        $value ='';
                    }
                }
                $final_array[$key]=$value;
            }
            return $final_array;
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_addPANDetails] '.$ex);
        }
    }

    private function updateAdditionalDetails($id, $data)
    {
        try {
            $identity=Identity::where('user_id', $id)->first();
            $data=$this->addPANDetails($data);
            $identity->pan=$data['APP_PAN_NO'];
            if ($this->getUserGroupById($id)!=3) {
                $identity->name=$data['APP_NAME'];
                $identity->father_name=$data['APP_F_NAME'];
                $identity->c_address1=$data['APP_COR_ADD1'];
                $identity->c_address2=$data['APP_COR_ADD2'];
                $identity->c_address3=$data['APP_COR_ADD3'];
                $identity->c_pincode=$data['APP_COR_PINCD'];
                $identity->c_address_proof=$data['APP_COR_ADD_PROOF'];
                $identity->c_state=$data['APP_COR_STATE'];
                $identity->dob=$data['APP_DOB_DT'];
            }
            $identity->p_address1=$data['APP_PER_ADD1'];
            $identity->p_address2=$data['APP_PER_ADD2'];
            $identity->p_address3=$data['APP_PER_ADD3'];
            $identity->p_pincode=$data['APP_PER_PINCD'];
            $identity->marital_status=$data['APP_MAR_STATUS'];
            $identity->p_state=$data['APP_PER_STATE'];
            $identity->p_address_proof=$data['APP_PER_ADD_PROOF'];
            $identity->annual_income=$data['APP_INCOME'];
            $identity->save();
            $identity=Identity::where('user_id', $id)->first();
            return $identity;
        } catch (Exception $ex) {
              Log::error('[IdentityDAO_updateAdditionalDetails] '.$ex);
        }
    }

    public function getUserGroup($user_id, $group)
    {
        try {
            $up_user=User::where('id', $user_id)->update(['user_group'=>$group]);
            $user=User::where('id', $user_id)->first();
            return $user->user_group;
        } catch (Exception $ex) {
            Log::error("[IdentityDAO_getUserGroup] ".$ex);
        }
    }

    public function updateUserDetails($id, $user_group, $data, $fatca_pos)
    {
        try {
            $identity=Identity::where('user_id', $id)->update(
                [
                  'pan'=>$data['pan'],
                  'mother_name'=>$data['m_name'],
                  'marital_status'=>$data['marital_status'],
                  'addr_chk_flg'=>$data['addr_chk_flg'],
                  'p_address1'=>$data['addr_chk_flg']==1?$data['c_address1']:$data['p_address1'],
                  'p_address2'=>$data['addr_chk_flg']==1?$data['c_address2']:$data['p_address2'],
                  'p_address3'=>$data['addr_chk_flg']==1?$data['c_address3']:$data['p_address3'],
                  'p_city'=>$data['addr_chk_flg']==1?$data['c_city']:$data['p_city'],
                  'p_state'=>$data['addr_chk_flg']==1?$data['c_state']:$data['p_state'],
                  'p_pincode'=>$data['addr_chk_flg']==1?$data['c_pincode']:$data['p_pincode'],
                  'p_address_proof'=>$data['addr_chk_flg']==1?$data['c_address_proof']:$data['p_address_proof'],
                  'annual_income'=>$data['annual_income'],
                  'net_worth'=>$data['net_worth'],
                  'occ_date'=>$data['occ_date'],
                  'occupation'=>$data['occupation']=='OTHERS'?$data['occup_other']:$data['occupation'],
                  'inv_exp'=>$data['inv_exp'],
                  'exp_year'=>$data['exp_year'],
                  'exp_month'=>$data['exp_month'],
                  'fatca_flag'=>$data['fatca_flg'],
                  'father_name'=>$data['f_name'],
                  'gender'=>$data['gender'],
                  'dob'=>$data['dob'],
                  'adhaar_no'=>$data['adhaar'],
                  'c_address1'=>$data['c_address1'],
                  'c_address2'=>$data['c_address2'],
                  'c_address3'=>$data['c_address3'],
                  'c_city'=>$data['c_city'],
                  'c_state'=>$data['c_state'],
                  'c_pincode'=>$data['c_pincode'],
                  'c_address_proof'=>$data['c_address_proof']
                ]
            );
            if ($data['fatca_flg']==1) {
                for ($i=0; $i<count($fatca_pos); $i++) {
                    $t=$fatca_pos[$i]+1;
                    Fatca::create([
                      'user_id'=>$id,
                      'country'=>$data['country'.$t],
                      'identification'=>$data['identification'.$t],
                      'identification_type'=>$data['identification_type'.$t]
                    ]);
                }
            }
            // if ($user_group!=3) {
                // $identity=Identity::where('user_id', $id)->update(
                //     [
                //     'father_name'=>$data['f_name'],
                //     'gender'=>$data['gender'],
                //     'dob'=>$data['dob'],
                //     'adhaar_no'=>$data['adhaar'],
                //     'c_address1'=>$data['c_address1'],
                //     'c_address2'=>$data['c_address2'],
                //     'c_address3'=>$data['c_address3'],
                //     'c_city'=>$data['c_city'],
                //     'c_state'=>$data['c_state'],
                //     'c_pincode'=>$data['c_pincode'],
                //     'c_address_proof'=>$data['c_address_proof']
                //     ]
                // );
            // }
            User::where('id', $id)->update(['identity_flag'=>1]);
            return $identity;
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_updateUserDetails] '.$ex);
        }
    }

    public function getUserDetails($id)
    {
        try {
            $identity=Identity::where('user_id', $id)->first();
            $fatca=Fatca::where(['user_id'=>$id,'deleted_at'=>0])->select('country', 'identification', 'identification_type')->get();
            return[
                'status'=>SUCCESS_STATUS,
                'person'=>$identity,
                'fatca'=>$fatca
            ];
        } catch (Exception $ex) {
            Log::error("[IdentityDAO_getUserDetails] ".$ex);
        }
    }

    public function getIdentity($id)
    {
        try {
            return Identity::where('user_id', $id)->first();
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_getIdentity] '.$ex);
        }
    }

    public function getStateCode($state)
    {
        try {
            $st= DB::connection('cvl_kra')->table('state_master')->where('state_name', $state)->first();
            return $st->state_code;
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_getStateCode] '.$ex);
        }
    }

    public function assignClientCode($id, $client_code)
    {
        try {
            return Identity::where('user_id', $id)->update(['client_code'=>$client_code]);
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_assignClientCode] '.$ex);
        }
    }

    public function getIdentityByAdhaar($adhaar)
    {
        try {
            return DB::table('users')
                    ->join('identity', 'users.id', "identity.user_id")
                    ->select('users.id')
                    ->where('identity.adhaar_no', $adhaar)
                    ->where('users.status', "1")->first();
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_getIdentityByAdhaar] '.$ex);
        }
    }
    public function getUserGroupById($id)
    {
        try {
            $user=User::where('id', $id)->first();
            return $user->user_group;
        } catch (Exception $ex) {
            Log::error('[IdentityDAO_getUserGroupById] '.$ex);
        }
    }
}
