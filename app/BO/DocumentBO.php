<?php
namespace App\BO;

use Illuminate\Http\Request;
use Validator;
use Exception;
use File;
use App\Identity;
use Log;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

class DocumentBO
{
    public function uploadIPV($req)
    {
        try {
            $inputs = $req->all();
/*suraj code
            // print_r($inputs['ipv']);exit;
          //print_r();exit;
            // $x = (base64_decode($inputs['ipv']));
            // file_put_contents ( public_path()."/".$inputs['pan'].".jpg", $x );
            // exit;
 // $fileType = $_FILES['ipv']['type'];
 // $tmpname = $_FILES['ipv']['tmp_name']
            /* suraj code
             print_r($inputs);die;
            $x = (base64_decode ($inputs['ipv']));
            file_put_contents ( public_path()."/".$inputs['pan'].".jpg", $x );
          // return  file_get_contents($x);
          return 1;

          //  Log::Debug($file_name);exit;
          //  Log::Debug(file_get_contents('https://accelerator-origin.kkomando.com/wp-content/uploads/2017/04/maxresdefault-5.jpg'));exit;
            // $a = file_get_contents(base64_decode ($inputs['ipv']));
            // return $a;
            return $inputs;


            suraj code */
            $mandatory=mandatory(M_UPLOAD_IPV, $inputs);
            if ($mandatory['status']==SUCCESS_STATUS) {
                $temp1=required(M_UPLOAD_IPV, $inputs);
                if (!$temp1) {
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>REQUIRED_VALIDATION,
                    "msg_code"=>REQUIRED_CODE."uploadIPV".BO,
                    "data"=>null
                    ];
                } else {
                    $validator=Validator::make($temp1, V_UPLOAD_IPV, V_PAN);
                    if ($validator->fails()) {
                        $errors = $validator->errors();
                        foreach ($errors->getMessages() as $key => $val) {
                            $return_error[$key] = $val[0];
                        }
                        return [
                        "status"=>FAIL_STATUS,
                        "msg"=>FIELD_VALIDATION,
                        "msg_code"=>VALIDATION_CODE."uploadIPV".BO,
                        "data"=>$return_error
                        ];
                    } else {
                        $user_id=authUser($inputs['api_token']);
                        if ($user_id) {
                            $user_pan=getIdentityByPAN($inputs['pan']);
                            if ($user_pan) {
                                if ($user_pan->id!=$user_id) {
                                    return [
                                    'status'=>FAIL_STATUS,
                                    'msg'=>"Pan registered with another client.",
                                    'msg_code'=>FUNCTION_CODE.'uploadIPV'.BO,
                                    'data'=>null
                                    ];
                                }
                            }
                            $update_pan=updatePan($user_id, $inputs['pan']);
                            if ($update_pan['status']==FAIL_STATUS) {
                                return $update_pan;
                            }
                            $data=getDir1($user_id);
                            if ($data) {
                                $content = file_get_contents($inputs['ipv']);
                                $d=file_put_contents($data."/".$inputs['pan'].".jpg", $content);
                                // $guessExtension = $req->file('ipv')->guessExtension();    /*laravel logic*/
                                // $file = $req->file('ipv')->move($data.'/', 'ipv.'.$guessExtension);
                                if ($d) {
                                    return [
                                    "status"=>SUCCESS_STATUS,
                                    "msg"=>"IPV Successful",
                                    "msg_code"=>SUCCESS_CODE,
                                    "data"=>null
                                    ];
                                } else {
                                    return [
                                    "status"=>FAIL_STATUS,
                                    "msg"=>"IPV unSuccessful",
                                    "msg_code"=>FUNCTION_CODE."uploadIPV".BO,
                                    "data"=>null
                                    ];
                                }
                            } else {
                                return [
                                "status"=>FAIL_STATUS,
                                "msg"=>"Failed in get Directory",
                                "msg_code"=>FUNCTION_CODE."uploadIPV".BO,
                                "data"=>null
                                ];
                            }
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>"Unauthorised Access",
                            "msg_code"=>UNAUTH,
                            "data"=>null
                            ];
                        }
                    }
                }
            } else {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>MANDATORY_VALIDATION,
                "msg_code"=>MANDATORY_CODE.'uploadIPV'.BO,
                "data"=>$mandatory['data']
                ];
            }
        } catch (Exception $ex) {
            Log::error('[DocumentBO_uploadIPV] '.$ex);
        }
    }

    public function uploadDocument($req)
    {
        try {
            $input=$req->all();
            $return_error=array();
        // $not_array=$this->notArray($input);
        // if ($not_array) {
        //     return [
        //       "status"=>FAIL_STATUS,
        //       "msg"=>FIELD_VALIDATION,
        //       "msg_code"=>VALIDATION_CODE."uploadDocument".BO,
        //       "data"=>$not_array
        //     ];
        // }
        $validator1=Validator::make($input, V_DOC_REQUIRED, V_PAN);
        if ($validator1->fails()) {
            $errors = $validator1->errors();
            foreach ($errors->getMessages() as $key => $val) {
                $return_error[$key] = $val[0];
            }
            return [
            "status"=>FAIL_STATUS,
            "msg"=>FIELD_VALIDATION,
            "msg_code"=>VALIDATION_CODE."uploadDocument".BO,
            "data"=>$return_error
            ];
        }
        $validator=Validator::make($input, V_UPLOAD_DOC);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->getMessages() as $key => $val) {
                $return_error[$key] = $val[0];
            }
            return [
              "status"=>FAIL_STATUS,
              "msg"=>FIELD_VALIDATION,
              "msg_code"=>VALIDATION_CODE."uploadDocument".BO,
              "data"=>$return_error
            ];
        } else {
            $user_id=authUser($input['api_token']);
            if ($user_id) {
                $user_pan=getIdentityByPAN($input['pan'], $user_id);
                if (!empty($user_pan)) {
                    return [
                        'status'=>FAIL_STATUS,
                        'msg'=>"Pan registered with another client.",
                        'msg_code'=>FUNCTION_CODE.'uploadIPV'.BO,
                        'data'=>null
                    ];
                }
                $update_pan=updatePan($user_id, $input['pan']);
                if ($update_pan['status']==FAIL_STATUS) {
                    return $update_pan;
                }
                $data=getDir1($user_id);
                if ($data) {
                    return  uploadDoc($user_id, $data, $req);
                } else {
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>"Failed in get Directory",
                    "msg_code"=>FUNCTION_CODE."uploadDocument".BO,
                    "data"=>null
                    ];
                }
            } else {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>"Unauthorised Access",
                "msg_code"=>UNAUTH,
                "data"=>null
                ];
            }
        }
      } catch (Exception $ex) {
          Log::error("[DocumentBO_uploadDocument] ".$ex);
      }
    }

    private function notArray($temp)
    {
        $keys=array('pan_file','sign','cancelled_cheque','ipv');//Constant
        foreach ($keys as $key) {
            if (array_key_exists($key, $temp)&&is_array($temp[$key])) {
                return $key.' should not be an array';
            }
        }
        return null;
    }
}
