<?php
namespace App\BO;

use Illuminate\Http\Request;
use Validator;
use App\BO\IdentityBO;
use App\DAO\EsignDAO;
use Exception;
use Log;
use Mail;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

class EsignBO
{
    public function __construct()
    {
        $this->esign_dao=new EsignDAO();
        $this->identity_bo=new IdentityBO();
    }
/*
    public function esginRequest($req)
    {
        $key=['api_token'];
        $mandatory=mandatory($key, $req);
        if ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE."esignRequest".BO,
            "data"=>$mandatory['data']
            ];
        }
        $temp1=required($key, $req);
        if (!$temp1) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>REQUIRED_VALIDATION,
              "msg_code"=>REQUIRED_CODE."esignRequest".BO,
              "data"=>null
            ];
        }
        $validator=Validator::make($req, array('api_token' =>'required|max:300'));
        if ($validator-> fails()) {
            $errors = $validator->errors();
            $return_error = array();
            foreach ($errors->getMessages() as $key => $val) {
                $return_error[$key] = $val[0];
            }
            return [
              "status"=>FAIL_STATUS,
              "msg"=>FIELD_VALIDATION,
              "msg_code"=>VALIDATION_CODE."esignRequest".BO,
              "data"=>$return_error
            ];
        }
        $user_id=authUser($temp1['api_token']);
        // $user_id=54;
        if (!$user_id) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>"Unauthorised Access",
            "msg_code"=>UNAUTH,
            "data"=>null
            ];
        }
        $user=$this->esign_dao->userDetails($user_id);
        if ($user->user_group!=3) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>"Please continue with Aadhaar",
            "msg_code"=>FUNCTION_CODE."esignRequest".BO,
            "data"=>null
            ];
        }
        $uploads_dir=getDir($user_id);
        $uploads_dir.='/';
        $identity=$this->esign_dao->identityDetails($user_id);
        if (!$identity->client_code) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>"Client code is not generated",
            "msg_code"=>FUNCTION_CODE."esignRequest".BO,
            "data"=>null
            ];
        }
        $files=array('profile_picture.jpeg','final_merged.pdf','ekyc-response.txt');
        foreach ($files as $key) {
            if (!file_exists($uploads_dir.$key)) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>"$key not found",
                "msg_code"=>FUNCTION_CODE."esignRequest".BO,
                "data"=>null
                ];
            }
        }
        $data=$this->callEsign($identity, $uploads_dir, $user);
        // if ($data['status']==SUCCESS_STATUS) {
        //     print_r(121232);exit;
        // }
        return $data;
    }

    private function callEsign($identity, $uploads_dir, $user)
    {
        $pdf_name = 'final_merged.pdf';
        $ekyc_resp = $uploads_dir. "ekyc-response.txt";
        $KYCRES = '';
        if (file_exists($ekyc_resp)) {
            $EKYCRESP = file_get_contents($ekyc_resp);
            $EKYCRESP = base64_decode($EKYCRESP);
            $EKYCRESP = $this->identity_bo->xmlToArrayApi($EKYCRESP);

            if ($EKYCRESP['status'] == SUCCESS_STATUS) {
                $EKYCRESP = $EKYCRESP['data'];
            } else {
                return $EKYCRESP;
            }

            try {
                if ($EKYCRESP [0] ['attributes'] ['ERRCODE'] != 'NA') {
                    $this->mailAPIError($txn_id, $EKYCRESP [0] ['attributes'] ['ERRCODE'], $EKYCRESP [0] ['attributes'] ['ERRMSG'], $identity->adhaar_no);
                    return [
                      'status'=>1,
                      'data'=>'',
                      'msg'=>$EKYCRESP [0] ['attributes'] ['ERRMSG'],
                      'msg_code'=>$EKYCRESP [0] ['attributes'] ['ERRCODE'],
                    ];
                }
            } catch (Exception $exception) {
                return [
                'status'=>1,
                'data'=>'',
                'msg'=>'catch exception',
                'msg_code'=>'123',
                ];
            }
            $KYCRES = '';
            foreach ($EKYCRESP as $key => $value) {
                if ($value ['tag'] == 'KYCRES') {
                    $KYCRES = $value ['value'];
                }
            }
        } else {
            return [
              'status'=>1,
              'data'=>'',
              'msg'=>'re do ekyc post otp',
              'msg_code'=>'123',
              ];
        }
        $otp ='';
        $asp_id = ESIGN_ASP_ID;
        $txn_id = substr(hash('sha256', mt_rand().microtime()), 0, 20).'-es-post-ekyc-resp';
        $cert_path = public_path().NESIGN_CERT_PATH;
        $cert_pwd = NESIGN_CERT_PWD;
        $issue_date = date('Y-m-d H:i:s');

        $type_otp = 1;
        $type_finger_print = 2;
        $type_iris = 3;
        $adhar_id = $identity->adhaar_no;
        $url = NESIGN_ESIGN_REQ;

        $unsignedPDFName = '';
        $unsignedPDFName1 = '';
        $pre_verified = "Y";

        $resident_name = $identity->name;

        $xml_data = $this->signXmlDataApi($issue_date, $txn_id, $adhar_id, $asp_id, $type_otp, $otp, $cert_path, $uploads_dir, $uploads_dir, $cert_pwd, "$pdf_name", $resident_name, $KYCRES, $pre_verified);


        Log::debug(' [AadharBO] '.' [esignRequest] '."E-Sign XML Prepared");

            // $dom = new \DOMDocument ();
            // $dom->preserveWhiteSpace = FALSE;
            // $dom->loadXML ( $xml_data );
            // $dom->save ( 'post-pdf.xml' );

        Log::debug(' [AadharBO] '.' [esignRequest] '."E-Sign API Called");
        $response = $this->identity_bo->postXMLApi($xml_data, $url);

        if ($response['status'] == 3) {
            $response = $response['data'];
        } else {
            return $response;
        }

        try {
            if ($response [0] ['attributes'] ['ERRORCODE'] != 'NA') {
                Log::debug(' [AadharBO] '.' [esignRequest] '."Unable to sign document.");
              // print_r($response [0] ['attributes'] );exit;
                $this->identity_bo->mailAPIError($txn_id, $response [0] ['attributes'] ['ERRORCODE'], $response [0] ['attributes']['ERRORMESSAGE'], $identity->adhaar_no);
                return [
                'status'=>1,
                'data'=>'',
                'msg'=>$response [0] ['attributes']['ERRORMESSAGE'],//ERROR_GENERIC
                'msg_code'=>$response [0] ['attributes'] ['ERRORCODE'],
                ];
            }
        } catch (Exception $exception) {
            return [
            'status'=>1,
            'data'=>'',
            'msg'=>$exception->getMessage(),//ERROR_GENERIC
            'msg_code'=>'catch errroorss',
            ];
        }

    // mail to user with attach
        $from_mail = FROM_EMAIL;
        $fromname = FROM_NAME;
        $c_code=$identity->client_code;
        $ClientEmail = $user->email;
      // 			$ClientEmail = 'sandip.desai@vnsfin.com';
        $ClientName = $identity->name;
        $date = date('Y/m/d');
        $data = array ();


        Mail::send('email.pdf-email', [
              'data' => $ClientName
          ], function ($message) use ($c_code, $ClientName, $ClientEmail, $fromname, $from_mail, $date, $uploads_dir) {
            $message->to($ClientEmail, $ClientName)->subject(ESIGN_MAIL_SUBJECT_CLIENT);
            $message->from($from_mail, $fromname);
          //$message->attach ( $uploads_dir."final_merged_Sign.pdf" );
            $message->attach($uploads_dir."final_merged_Sign.pdf", array(
              'as' =>$c_code.'.pdf',
              'mime' => 'application/pdf'));
          });

    // mail to  KYC and PRAVEEN
        // $emails = [ESIGN_MAILTO_VNS_1, ESIGN_MAILTO_VNS_2];
        $emails=["sanket.gawde@vnsfin.com"];
        Mail::send('email.pdf-email-internal', [
            'data' => $ClientName
        ], function ($message) use ($c_code, $ClientName, $emails, $fromname, $from_mail, $date, $uploads_dir) {
            $message->to($emails, $ClientName)->subject(ESIGN_MAIL_SUBJECT_VNS.$ClientName);
            $message->bcc('sanket.gawde@vnsfin.com', 'terminator2');//(ESIGN_MAILTO_VNS_BCC_EMAIL,ESIGN_MAILTO_VNS_BCC_NAME);
            $message->from($from_mail, $fromname);
          //$message->attach ( $uploads_dir."final_merged_Sign.pdf" );
            $message->attach(
                $uploads_dir."final_merged_Sign.pdf",
                array(
                  'as' =>$c_code.'.pdf',
                  'mime' => 'application/pdf')
            );
        });

        Log::debug(' [AadharBO] ' . ' [esignRequest] ' . "Email sent to client and department.");

        return [
          'status'=>3,
          'data'=>[
                  'txn_id'=>$txn_id,
                  'aadhar'=>$adhar_id,
                  'ts'=>$issue_date,
                  ],
          'msg'=>'E-Sign Compelete',
          'msg_code'=>SUCCESS_CODE,
        ];
    }

    private function signXmlDataApi($issue_date, $txn_id, $adhar_id, $asp_id, $type_otp, $otp, $cert_path, $unsignedPDF, $signedPDF, $cert_pwd, $unsignedPDFName, $resident_name = "", $key_resp = "", $pre_verified = "N")
    {
        try {
            Log::debug(' [AadharBO] '.' [signXmlDataApi] '."Entered in signXmlData function.");
            $xml_data = '
    		<neSignRequest ts="' . $issue_date . '" txn="' . $txn_id . '" uid="' . $adhar_id . '" aspId="' . $asp_id . '"
    		preVerified="'.$pre_verified.'" AuthMode="' . $type_otp . '"
    		Lr="N" Pfr ="Y" Ra="O" Rc="Y" EkycId="' . $adhar_id . '"
    		De="Y" EkycIdType="A" EkycMode="U" Kycver="2.1" responseurl=""
    		>
    		<KycRes>'.$key_resp.'</KycRes>
    		<otp>' . $otp . '</otp>
    		<bfd></bfd>
    		<FingerPos></FingerPos>
    		<Udc></Udc>
    		<signCert>' . $cert_path . '</signCert>
    		<pdfPath>' . $unsignedPDF . '</pdfPath>
    		<signPdf>' . $signedPDF . '</signPdf>
    		<password>' . $cert_pwd . '</password>
    		<xx>11</xx>
    		<xy>22</xy>
    		<yx>33</yx>
    		<yy>44</yy>
    		<pageno>A</pageno>
    		<reason>This pdf is e-signed</reason>
    		<location>Location</location>
    		<Docs>
    		<InputPDF id="1"  docInfo="All attachment and account opening pdf merged.">' . $unsignedPDFName . '</InputPDF>
    		</Docs>
    		<name>'.$resident_name.'</name>
    		</neSignRequest>
    		';
            return $xml_data;
        } catch (Exception $exception) {
            Log::error("[EsignBO_signXmlDataApi]".$exception);
        }
    }
*/
    public function eMudraRequest($req)
    {
        $mandatory=mandatory(M_ESIGN, $req);
        if ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE."eMudraRequest".BO,
            "data"=>$mandatory['data']
            ];
        }
        $temp1=required(M_ESIGN, $req);
        if (!$temp1) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>REQUIRED_VALIDATION,
              "msg_code"=>REQUIRED_CODE."eMudraRequest".BO,
              "data"=>[
              'url'=>$temp1['url']
            ]
            ];
        }
        $validator=Validator::make($req, V_ESIGN);
        if ($validator-> fails()) {
            $errors = $validator->errors();
            $return_error = array();
            foreach ($errors->getMessages() as $key => $val) {
                $return_error[$key] = $val[0];
            }
            return [
              "status"=>FAIL_STATUS,
              "msg"=>FIELD_VALIDATION,
              "msg_code"=>VALIDATION_CODE."esignRequest".BO,
              "data"=>$return_error
            ];
        }
        $user_id=authUser($temp1['api_token']);
        //$user_id=54;
        if (!$user_id) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>"Unauthorised Access",
            "msg_code"=>UNAUTH,
            "data"=>[
              'url'=>$temp1['url']
            ]
            ];
        }
        $identity=$this->esign_dao->identityDetails($user_id);
        if(empty($identity->adhaar_no)){
            return [
            "status"=>FAIL_STATUS,
            "msg"=>"Aadhaar number is not registered",
            "msg_code"=>FUNCTION_CODE."esignRequest".BO,
            "data"=>[
              'url'=>$temp1['url']
            ]
            ];
        }
        $user_dir=getdir1($user_id);
        if(!file_exists($user_dir."/final_merged.pdf")){
            return [
            "status"=>FAIL_STATUS,
            "msg"=>"final_merged.pdf not found",
            "msg_code"=>FUNCTION_CODE."esignRequest".BO,
            "data"=>[
              'url'=>$temp1['url']
            ]
            ];
        }
        if (file_exists($user_dir.'/final_merged_Sign.pdf')) {
            unlink($user_dir.'/final_merged_Sign.pdf');
        }
        $pdf_path=$user_dir."/final_merged.pdf";
        $pdf = file_get_contents($pdf_path);
        $base64 = base64_encode($pdf);
        $ref=$user_id.'M'.time();
        $this->esign_dao->createEsignEntry($user_id, $ref, $temp1['url']);
        // print_r($this->esign_dao->createEsignEntry($user_id,$ref));exit;

        $data = [
          'Filetype'=>'PDF',
          'File'=>$base64,
          'Iscompressed'=>'FALSE',
          'Referencenumber'=>$ref,
          'Name'=>$identity->name,
          'AuthToken'=>auth_token,//Config::get('helpers.constants.auth_token'),
          'SignatureType'=>3,
          'IsCosign'=>'FALSE',
          'AadhaarNumber'=>$identity->adhaar_no,
          'SelectPage'=>'ALL',
          'PageNumber'=>1,
          'PagelevelCoordinates'=>'',
          'SignaturePosition'=>'Bottom-Left',
          'PreviewRequired'=>'FALSE',
          'Enableuploadsignature'=>'FALSE',
          'Enablefontsignature'=>'FALSE',
          'EnableDrawSignature'=>'FALSE',
          'EnableeSignaturePad'=>'FALSE',
          'SUrl'=>"http://".$_SERVER['SERVER_NAME'].':9000/api/v1/esignRes/success',
          'Furl'=>"http://".$_SERVER['SERVER_NAME'].':9000/api/v1/esignRes/failed',
          'Curl'=>"http://".$_SERVER['SERVER_NAME'].':9000/api/v1/esignRes/cancelled'
          //live
          /*
          'SUrl'=>"http://".$_SERVER['SERVER_NAME'].'/esign/success',
          'Furl'=>"http://".$_SERVER['SERVER_NAME'].'/esign/failed',
          'Curl'=>"http://".$_SERVER['SERVER_NAME'].'/esign/cancelled'
          */
        ];
        // print_r($data);exit;
        // $curl_result=$this->callCurl($data);
        return[
          'status'=>SUCCESS_STATUS,
          'data'=>$data
        ];//View::make('esign.esign-emudra')->with('data',$data)
        // print_r($_SERVER['SERVER_NAME'].':8000');exit;
    }

    public function eSignMudraResponse($request ,$status)
    {
        // $user_details = $this->user_profile_dao->fetchUserDetail ( Auth::user ()->id );

        // $esign = eSign::where('user_id',Auth::user ()->id)->orderBy('id','DESC')->first();
        // print_r("qweqwe");exit;
        if(!$request->Referencenumber){
            Log::error("Referencenumber not getting");
            $this->identity_bo->mailAPIError(time(), "none", "empty response from eMudra ", time());
            return [
              'status'=>FAIL_STATUS,
              'msg'=>"empty response from eMudra",
              'data'=>null,
              'msg_code'=>FUNCTION_CODE."eSignMudraResponse".BO
            ];
            // return redirect()->route('esign-otp')->with('message','No response recieved')->with('message_type',"danger");
        }

      try {
        $esign =$this->esign_dao->getEsignEntry($request->Referencenumber);
      // $esign = new eSign;
      // $esign->user_id = Auth::user()->id;
      $esign->Returnvalue = $request->Returnvalue;
      $esign->FileType = $request->FileType;
      $esign->Referencenumber = $request->Referencenumber;
      $esign->Transactionnumber = $request->Transactionnumber;
      $esign->ReturnStatus = $request->ReturnStatus;
      $esign->save();
      // Log::debug (Auth::user()->username.  ' [eSignController] ' . ' [eSignMudraResponse] ' . 'response saved' );


      // $uploads_dir = public_path () . UPLOAD_DIR . $user_details ['pancard_no'] . "/";
      $uploads_dir = getDir1($esign->user_id);

      $user_details=$this->esign_dao->userDetails($esign->user_id);
      $identity=$this->esign_dao->identityDetails($esign->user_id);


      if($status != 'success'){
        Log::debug ( "ref=$request->Referencenumber" .' [eSignController] ' . ' [eSignMudraResponse] ' . 'NO success status ' );

        $this->identity_bo->mailAPIError($esign->Referencenumber,"none",$request->ReturnStatus,$identity->adhaar_no);
        return [
        "status"=>FAIL_STATUS,
        "msg"=>"Transaction $status for $identity->adhaar_no",
        "msg_code"=>FUNCTION_CODE."eSignMudraResponse".BO,
        "data"=>[
            'url'=>$esign->callbackurl
        ]
        ];
        // return redirect()->route('esign-otp')->with('message',"Esign $status")->with('message_type',"danger");

      } else {
        // Log::debug (Auth::user()->username.  ' [eSignController] ' . ' [eSignMudraResponse] ' . ' success status ' );
        // User::where ( 'id', '=', $esign->user_id )->update ( [
        //   'page_status' => '',
        //   'kyc_status' => 1,
        //   'user_group' => 3,
        //   'profile_status' => 14,
        //   'kyc_esign_version' => 'emudra',
        //   ] );

          $pdf_decoded = base64_decode ($request->Returnvalue);
          //Write data back to pdf file
          $pdf = fopen ($uploads_dir.'/final_merged_Sign.pdf','w');
          fwrite ($pdf,$pdf_decoded);
          //close output file
          fclose ($pdf);
          chmod($uploads_dir.'/final_merged_Sign.pdf',0777);
          $from_mail = FROM_EMAIL;
          $fromname = FROM_NAME;
          $ClientEmail = $user_details->email;
          $ClientName =$identity->name;
          $date = date ('Y/m/d');
          // $data = array ();
          $client_code=$identity->client_code;
          // $uploads_dir = public_path () . UPLOAD_DIR . $user_details ['pancard_no'] . "/";
          // $uploads_dir = getDir1($esign->user_id);

          // print_r($client_code);exit;
          $this->esign_dao->updateKycstatus($esign->user_id);
          Mail::send ( 'email.pdf-email', [
            'data' => $ClientName
          ], function ($message) use ($user_details,$ClientName, $ClientEmail, $fromname, $from_mail, $date, $uploads_dir,$client_code) {
            $message->to ( $ClientEmail, $ClientName )->subject ( ESIGN_MAIL_SUBJECT_CLIENT );
            $message->from ( $from_mail, $fromname );
            //$message->attach ( $uploads_dir."final_merged_Sign.pdf" );
            $message->attach( $uploads_dir."/final_merged_Sign.pdf", array(
              'as' =>$client_code.'.pdf',
              'mime' => 'application/pdf')
            );

          } );
          // Log::debug (Auth::user()->username.  ' [eSignController] ' . ' [eSignMudraResponse] ' . 'Mail to CLient sent' );

          // mail to  KYC and PRAVEEN
          $emails = [ESIGN_MAILTO_VNS_1, ESIGN_MAILTO_VNS_2];
          Mail::send ( 'email.pdf-email-internal',[
            'data' => $ClientName
          ],  function ($message) use ($user_details,$ClientName, $emails, $fromname, $from_mail, $date, $uploads_dir,$client_code) {
            $message->to ( $emails, $ClientName )->subject ( ESIGN_MAIL_SUBJECT_VNS.$ClientName );

            $message->bcc('sanket.gawde@vnsfin.com', 'terminator2');//bcc(ESIGN_MAILTO_VNS_BCC_EMAIL,ESIGN_MAILTO_VNS_BCC_NAME);

            $message->from ( $from_mail, $fromname );
            //$message->attach ( $uploads_dir."final_merged_Sign.pdf" );
            $message->attach( $uploads_dir."/final_merged_Sign.pdf", array(
              'as' =>$client_code.'.pdf',
              'mime' => 'application/pdf')
            );

          } );
          return [
            "status"=>SUCCESS_STATUS,
            "msg"=>"eMudra is successfully done for $identity->name",
            "msg_code"=>SUCCESS_CODE,
            "data"=>[
                'url'=>$esign->callbackurl
            ]
          ];
          // Log::debug (Auth::user()->username.  ' [eSignController] ' . ' [eSignMudraResponse] ' . 'Mail to department sent' );

          // return redirect('download-form');

        }
      } catch (Exception $ex) {
        Log::error("[EsignBO_eSignMudraResponse] ".$ex);
      }

      }
}
