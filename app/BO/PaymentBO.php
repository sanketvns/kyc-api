<?php
namespace App\BO;

use Illuminate\Http\Request;
use Validator;
use Exception;
use File;
use Log;
use App\DAO\PaymentDAO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";

class PaymentBO
{
    public function __construct()
    {
        $this->payment_dao=new PaymentDAO();
    }

    public function getOfferDetails()
    {
        $data = $this->payment_dao->getOfferDetails();
        if ($data) {
            $discount1=$code1=$discount=$code=array();
            $key1=array('name','description','code','off','min_val','trade_count','start date','end date');
            $key2=array('name','description','discount','start date','end date');
            for ($i=0,$coupon_counter=0,$code_counter = 0; $i<count($data); $i++) {
                if (!empty($data[$i]['discount'])) {
                    $discount['discount-'.++$coupon_counter]=$data[$i];
                } else {
                    $code['coupon-'.++$code_counter]=$data[$i];
                }
            }
            foreach ($discount as $key => $value) {
                $discount1[$key]=$this->removeGarbage($key2, $value);
            }
            foreach ($code as $key => $value) {
                $code1[$key]=$this->removeGarbage($key1, $value);
            }
            return [
              "status"=>SUCCESS_STATUS,
              "msg"=>"Offers found",
              "msg_code"=>SUCCESS_CODE,
              "data"=>[
                  'discount'=>empty($discount1)?"No discount found":$discount1,
                  'coupon'=>empty($code1)?"No coupon found":$code1
              ]
            ];
        } else {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>"No Offer found",
              "msg_code"=>FUNCTION_CODE."getOfferDetails".BO,
              "data"=>null
            ];
        }
    }

    private function removeGarbage($keys, $data)
    {
        $t=array();
        foreach ($keys as $key) {
            $t[$key]=$data[$key];
        }
        return $t;
    }

    public function calcPayment($req)
    {
        $price=$dis_price=0;
        try {
            $validator=Validator::make($req, V_CALC_PAYMENT);
            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($errors->getMessages() as $key => $val) {
                    $return_error[$key] = $val[0];
                }
                return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE."calcPayment".BO,
                    "data"=>$return_error
                ];
            }
            $original=$this->calcOriginal($req['email']);
            if ($original['status']==FAIL_STATUS) {
                return $original;
            }
            $enabled_segment=$this->enabledSegment($req['email']);
            $price=$original['data'];
            $user=$original['user'];
            $identity_user=$this->payment_dao->getIdentityById($user->id);
            $plan=$this->payment_dao->getTradingPlan($user->id);
            $info=$this->formatProductInfo($plan);
            $offers=$this->payment_dao->getOfferDetails();
            if(empty($offers))
            {
                //save data;
                $entry=[
                  'user_id'=>$user->id, //Array key should be match with tbl payment_details columns name
                  'name'=>$identity_user->name,
                  'pan'=>$identity_user->pan,
                  'mobile'=>$user->phone,
                  'amt'=>$price,
                  'final_amt'=>$price,
                  'product_info'=>$info
                ];
                $data=$this->payment_dao->createPaymentEntry($entry);
                  return [
                  'status'=>SUCCESS_STATUS,
                  'msg'=>'No Such discount or coupon',
                  'msg_code'=>SUCCESS_CODE,
                  'data'=>[
                    'name'=>$identity_user->name,
                    'phone'=>$user->phone,
                    'segment'=>$enabled_segment,
                    'amount'=>$price,
                    'final_amount'=>$price,
                    'product_info'=>$info,
                    'discount'=>array_key_exists('offer', $req)?($data['coupon_code']?$data['coupon_code']:DEFAULT_DISCOUNT):'',
                    'trade_count'=>$data['coupon_code']?$data['trade_count']:'',
                    'coupon_validity_date'=>$data['coupon_code']?$data['coupon_expiry_date']:''
                  ]
                  ];
            } else {
              $offers=$this->getOfferDetails();
            }
            if (!(is_array($offers['data']['discount'])||is_array($offers['data']['coupon']))) {
                $entry=[
                  'user_id'=>$user->id, //Array key should be match with tbl payment_details columns name
                  'name'=>$identity_user->name,
                  'pan'=>$identity_user->pan,
                  'mobile'=>$user->phone,
                  'amt'=>$price,
                  'final_amt'=>$price,
                  'product_info'=>$info
                ];
                $data=$this->payment_dao->createPaymentEntry($entry);
                return [
                  'status'=>SUCCESS_STATUS,
                  'msg'=>'No Such discount or coupon',
                  'msg_code'=>SUCCESS_CODE,
                  'data'=>[
                    'name'=>$identity_user->name,
                    'phone'=>$user->phone,
                    'segment'=>$enabled_segment,
                    'amount'=>$price,
                    'final_amount'=>$price,
                    'product_info'=>$info,
                    'discount'=>array_key_exists('offer', $req)?($data['coupon_code']?$data['coupon_code']:DEFAULT_DISCOUNT):'',
                    'trade_count'=>$data['coupon_code']?$data['trade_count']:'',
                    'coupon_validity_date'=>$data['coupon_code']?$data['coupon_expiry_date']:''
                  ]
                ];
            }
            if (array_key_exists('offer', $req)) {
                $v_offer=$this->getDiscountCoupon($offers['data'], $req['offer']);
                if (!$v_offer) {
                  $entry=[
                    'user_id'=>$user->id, //Array key should be match with tbl payment_details columns name
                    'name'=>$identity_user->name,
                    'pan'=>$identity_user->pan,
                    'mobile'=>$user->phone,
                    'amt'=>$price,
                    'final_amt'=>$price,
                    'product_info'=>$info
                  ];
                  $data=$this->payment_dao->createPaymentEntry($entry);
                  return [
                    'status'=>SUCCESS_STATUS,
                    'msg'=>'No Such discount or coupon',
                    'msg_code'=>SUCCESS_CODE,
                    'data'=>[
                      'name'=>$identity_user->name,
                      'phone'=>$user->phone,
                      'segment'=>$enabled_segment,
                      'amount'=>$price,
                      'final_amount'=>$price,
                      'product_info'=>$info,
                      'discount'=>array_key_exists('offer', $req)?($data['coupon_code']?$data['coupon_code']:DEFAULT_DISCOUNT):'',
                      'trade_count'=>$data['coupon_code']?$data['trade_count']:'',
                      'coupon_validity_date'=>$data['coupon_code']?$data['coupon_expiry_date']:''
                    ]
                  ];
                }
                $data='';
                if ($v_offer['type']==0) {
                    $data=$this->calcDiscountPayment($price, $v_offer['data']);
                    // print_r($data);exit;
                } else {
                    $data=$this->calcCouponPayment($price, $v_offer['data']);
                }
                if ($data['status']==FAIL_STATUS) {
                    return $data;
                }
                $dis_price=$data['data'];
                //saivng data in Payment table with offer
                $pay=$this->formatPaymentData($user, $price, $dis_price, $req['offer'], $v_offer['data']);
            } else {
                //saivng data in Payment table
                $dis_price=$price;
                $pay=$this->formatPaymentData($user, $price);
            }
            $data=$this->payment_dao->createPaymentEntry($pay);
            if ($data) {
                return [
                'status'=>SUCCESS_STATUS,
                'msg'=>'Paymnet Entry Added',
                'msg_code'=>SUCCESS_CODE,
                'data'=>[
                  'name'=>$pay['name'],
                  'phone'=>$pay['mobile'],
                  'segment'=>$enabled_segment,
                  'amount'=>$pay['amt'],
                  'final_amount'=>$dis_price,
                  'product_info'=>$pay['product_info'],
                  'discount'=>array_key_exists('offer', $req)?($data['coupon_code']?$data['coupon_code']:DEFAULT_DISCOUNT):'',
                  'trade_count'=>$data['coupon_code']?$data['trade_count']:'',
                  'coupon_validity_date'=>$data['coupon_code']?$data['coupon_expiry_date']:''
                ]
                ];
            }
            return [
            'status'=>FAIL_STATUS,
            'msg'=>'Unable to add payment entry',
            'msg_code'=>FUNCTION_CODE.'calcPayment'.BO,
            'data'=>null
            ];
        } catch (Exception $ex) {
            Log::error('[PaymentBO_calcPayment] '.$ex);
        }
    }

    private function calcOriginal($email)
    {
        $price=0;
        $trade_charge=array();
        $product_info='';
        try {
            $user=$this->payment_dao->getUserByEmail($email);
            if ($user) {
                if ($user->trading_flag==1) {
                    $trading_charges=$this->payment_dao->getCharges();
                    // print_r($trading_charges);exit;
                    $segment_flags=$this->payment_dao->getTradingDetails($user->id);
                    // print_r($segment_flags);exit;
                    foreach ($segment_flags as $key => $value) {
                        if (!empty($value)) {
                            $price+=$trading_charges->$key;
                        }
                    }
                    // print_r($segment_flags);exit;
                    return [
                      'status'=>SUCCESS_STATUS,
                      'data'=>$price,
                      'user'=>$user
                    ];
                } else {
                    return [
                    'status'=>FAIL_STATUS,
                    'msg_code'=>FUNCTION_CODE.'calcOriginal'.BO,
                    'msg'=>"User doesn't have Trading Details",
                    'data'=>null
                    ];
                }
            } else {
                return [
                  'status'=>FAIL_STATUS,
                  'msg_code'=>FUNCTION_CODE.'calcOriginal'.BO,
                  'msg'=>"User not register with this ($email).",
                  'data'=>null
                ];
            }
        } catch (Exception $ex) {
            Log::error('[PaymentBO_calcOriginal] '.$ex);
        }
    }

    private function getDiscountCoupon($offers, $input_offer)
    {
      // print_r($offers);echo "<br>";print_r($input_offer);echo "<br>";print_r(DEFAULT_DISCOUNT);exit;
        try {
            if($offers['discount']=='No discount found'){
              $offers['discount']=null;
            }
            if($offers['coupon']=='No discount found'){
              $offers['coupon']=null;
            }
            if ($input_offer==DEFAULT_DISCOUNT&&!empty($offers)) {
                return ['type'=>0,'data'=>$offers['discount']['discount-1']];
            } elseif (!empty($offers['coupon'])&&array_key_exists($input_offer, $offers['coupon'])) {
                return ['type'=>1,'data'=>$offers['coupon'][$input_offer]];
            } else {
                return null;
            }
        } catch(Exception $ex) {
            Log::error("[PaymentBO_getDiscountCoupon] ".$ex);
        }
    }

    private function calcCouponPayment($price, $offer)
    {
        try {
            if ($price<$offer['min_val']) {
                return[
                'status'=>FAIL_STATUS,
                'msg'=>'Minimum payment should be greater than Rs.'.$offer['min_val'],
                'msg_code'=>FUNCTION_CODE.'calcCouponPayment'.BO,
                'data'=>null
                ];
            }
            $price-=$offer['min_val'];
            return[
              'status'=>SUCCESS_STATUS,
              'data'=>$price
            ];
        } catch (Exception $ex) {
            Log::error('[PaymentBO_calcCouponPayment] '.$ex);
        }
    }

    private function calcDiscountPayment($price, $offer)
    {
        try {
            $price=$price-$price*$offer['discount']*0.01;
            return [
              'status'=>SUCCESS_STATUS,
              'data'=>$price
            ];
        } catch (Exception $ex) {
            Log::error('[PaymentBO_calcDiscountPayment] '.$ex);
        }
    }

    private function formatPaymentData($user, $price, $dis_price = null, $input_offer = null, $offer = null)
    {
        try {
            $trading_plan=$this->payment_dao->getTradingPlan($user->id);
            $identity=$this->payment_dao->getIdentityById($user->id);
            $product_info=$this->formatProductInfo($trading_plan);
            if ($input_offer==DEFAULT_DISCOUNT) {
                return [
                'user_id'=>$user->id,//Array key should be match with tbl payment_details columns name
                'name'=>$identity->name,
                'pan'=>$identity->pan,
                'mobile'=>$user->phone,
                'amt'=>$price,
                'final_amt'=>$dis_price,
                'discount'=>1,
                'product_info'=>$product_info,
                ];
            }
            if (!$input_offer) {
                return[
                'user_id'=>$user->id, //Array key should be match with tbl payment_details columns name
                'name'=>$identity->name,
                'pan'=>$identity->pan,
                'mobile'=>$user->phone,
                'amt'=>$price,
                'final_amt'=>$price,
                'product_info'=>$product_info
                ];
            }
            if ($input_offer) {
                return [
                'user_id'=>$user->id, //Array key should be match with tbl payment_details columns name
                'name'=>$identity->name,
                'pan'=>$identity->pan,
                'mobile'=>$user->phone,
                'amt'=>$price,
                'final_amt'=>$dis_price,
                'coupon_code'=>$offer['code'],
                'coupon_expiry_date'=>$offer['trade_count']?date('Y-m-d', strtotime("+14 days")):null, //critical
                'trade_count'=>$offer['trade_count']?$offer['trade_count']:null,
                'product_info'=>$product_info
                ];
            }
        } catch (Exception $ex) {
            Log::error('[PaymentBO_formatPaymentData] '.$ex);
        }
    }
    private function formatProductInfo($trading_plan)
    {
        try {
            $t=array();
            $tradecharge=$this->payment_dao->getCharges();
            $production_info='';
            if ($trading_plan->currency_flag&&$trading_plan->equity_flag) {
                $production_info='Equity and Currency trade charges:Rs.'.$tradecharge->Equity_Currency.',';
            } elseif ($trading_plan->currency_flag) {
                $production_info='Currency trade charges:Rs.'.$tradecharge->Equity_Currency.',';
            } elseif ($trading_plan->equity_flag) {
                $production_info='Equity trade charges:Rs.'.$tradecharge->Equity_Currency.',';
            }
            if ($trading_plan->commodity_flag) {
                $production_info.='Commodity trade charges:Rs.'.$tradecharge->Commodity.',';
            }
            if ($trading_plan->demat_flag) {
                $production_info.='Demat charges:Rs.'.$tradecharge->Demat;
            }
            return $production_info;
        } catch (Exception $ex) {
            Log::error('[PaymentBO_formatProductInfo] '.$ex);
        }
    }

    private function enabledSegment($email)
    {
        $user=$this->payment_dao->getUserByEmail($email);
        $trading_charges=$this->payment_dao->getCharges();
        $segment_flags=$this->payment_dao->getTradingDetails($user->id);
        // print_r($segment_flags);exit;
        $enabled_segment=[];
        foreach ($segment_flags as $key => $value) {
            if (!empty($value)) {
                $enabled_segment[$key]=$trading_charges->$key;
            } else {
                $enabled_segment[$key]=0;
            }
        }
        return $enabled_segment;
    }
}
