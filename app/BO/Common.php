<?php
namespace App\BO;

class Common
{
    public static function getURL($timeout = 2)
    {
        $arrURL = array(
        'http://bodb1.vnsfin.com:8084/client-code/index.php',
        'http://bodb2.vnsfin.com:8084/client-code/index.php',
        'http://bodb3.vnsfin.com:8084/client-code/index.php'
        );
        $status='';
        $i = 0;
        while ($i<count($arrURL)) {
            $curl_request = curl_init();
            curl_setopt($curl_request, CURLOPT_URL, $arrURL[$i]);
            curl_setopt($curl_request, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($curl_request, CURLOPT_HEADER, 0);
            curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
            $result = curl_exec($curl_request);
            $status = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
            if ($status == 200) {
                return $arrURL[$i];
            }
            curl_close($curl_request);
            $i++;
        }
    }
    public static function callCurl($data)
    {
        $url = self::getURL();
        $fields = '';
        foreach ($data as $key => $value) {
            $fields.=$key.'='.$value.'&';
        }
        rtrim($fields, '&');
        $curl_request = curl_init();
        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, count($data));
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($curl_request, CURLOPT_HEADER, 0);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
        $result =curl_exec($curl_request);
        curl_close($curl_request);
        return json_decode($result);
    }
}
