<?php
namespace App\BO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Log;
use App\DAO\BankDetailsDAO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";

/**
 *
 */
class BankDetailsBO
{

    public function __construct()
    {
        $this->bank_details_dao=new BankDetailsDAO();
    }

    // public function create($req)
    // {
    //     $mandatory=mandatory(BANKDETAILS, $req);
    //     if ($mandatory['status']==SUCCESS_STATUS) {
    //         $temp1=required(BANKDETAILSR, $req);
    //         if (!$temp1) {
    //             return [
    //               "status"=>FAIL_STATUS,
    //               "msg"=>REQUIRED_VALIDATION,
    //               "msg_code"=>REQUIRED_CODE.BANK_DETAILS.BO,
    //               "data"=>null
    //             ];
    //         } else {
    //             $validator = Validator::make($temp1, V_BANK_DETAILS, V_REG_USER_MSG);
    //             if ($validator-> fails()) {
    //                 $errors = $validator->errors();
    //                 $return_error = array();
    //                 foreach ($errors->getMessages() as $key => $val) {
    //                     $return_error[$key] = $val[0];
    //                 }
    //                 return [
    //                   "status"=>FAIL_STATUS,
    //                   "msg"=>FIELD_VALIDATION,
    //                   "msg_code"=>VALIDATION_CODE.BANK_DETAILS.BO,                 /**NO USE**/
    //                   "data"=>$return_error
    //                 ];
    //             } else {
    //                 try {
    //                     $data=$this->bank_details_dao->create($temp1);
    //                     if ($data['status']==SUCCESS_STATUS) {
    //                         return [
    //                           "status"=>SUCCESS_STATUS,
    //                           "msg"=>BANK_DETAILS_SUCC,
    //                           "msg_code"=>SUCCESS_CODE
    //                         ];
    //                     } else {
    //                         return [
    //                           "status"=>FAIL_STATUS,
    //                           "msg"=>BANK_DETAILS_FAIL,
    //                           "msg_code"=>FUNCTION_CODE.BANK_DETAILS.BO,
    //                           'data'=>null
    //                         ];
    //                     }
    //                 } catch (Exception $ex) {
    //                     Log::error("[UserBO_create] ".$ex);
    //                 }
    //             }
    //         }
    //     } elseif ($mandatory['status']==FAIL_STATUS) {
    //         return [
    //           "status"=>FAIL_STATUS,
    //           "msg"=>MANDATORY_VALIDATION,
    //           "msg_code"=>MANDATORY_CODE.BANK_DETAILS.BO,
    //           "data"=>$mandatory['data']
    //         ];
    //     }
    // }

    public function getBankDetails($req)
    {
        $key=array('api_token');
        $mandatory=mandatory($key, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required($key, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.GET_BANK_DETAILS.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, array('api_token'=>'required|max:300'));
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.GET_BANK_DETAILS.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        if (!$user_id) {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthrised Access",
                              "msg_code"=>UNAUTH,
                              'data'=>null
                            ];
                        }
                        $data=$this->bank_details_dao->getBankDetails($user_id);
                        if ($data['status']==SUCCESS_STATUS) {
                            return [
                            "status"=>SUCCESS_STATUS,
                            "msg"=>BANK_DETAILS_SUCC,
                            "msg_code"=>SUCCESS_CODE,
                            'data'=>[
                              'micr'=>$data['data']['MICR'],
                              'ifsc'=>$data['data']['IFSC'],
                              'bankname'=>$data['data']['bankname'],
                              'branch'=>$data['data']['branch'],
                              'branch_address'=>$data['data']['branch_address'],
                              'acc_type'=>$data['data']['acc_type'],
                              'accno'=>$data['data']['acc_no'],
                              'secmicr'=>$data['data']['sec_MICR'],
                              'secifsc'=>$data['data']['sec_IFSC'],
                              'secbankname'=>$data['data']['sec_bankname'],
                              'secbranch'=>$data['data']['sec_branch'],
                              'secbranch_address'=>$data['data']['sec_branch_address'],
                              'secacc_type'=>$data['data']['sec_acc_type'],
                              'secaccno'=>$data['data']['sec_acc_no']
                            ]
                            ];
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>BANK_DETAILS_FAIL,
                            "msg_code"=>FUNCTION_CODE.GET_BANK_DETAILS.BO,
                            'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_create] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.GET_BANK_DETAILS.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    public function updateBankDetails($req)
    {
        $mandatory=mandatory(BANKDETAILS, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(BANKDETAILSR, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.UPDATE_BANK_DETAILS.BO,
                "data"=>null
                ];
            } else {
                $return_error = array();
                $validator = Validator::make($temp1, V_BANK_DETAILS1, array_merge(V_REG_USER_MSG, V_IFSC));
                // print_r($validator);exit;
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                }
                if (!$this->bankDetailsSanitizer($temp1)) {
                    $validator1 = Validator::make($temp1, V_BANK_DETAILS2, array_merge(V_REG_USER_MSG, V_IFSC));
                    if ($validator1-> fails()) {
                        $errors = $validator1->errors();
                        foreach ($errors->getMessages() as $key => $val) {
                            $return_error[$key] = $val[0];
                        }
                    }
                }
                if (!empty($return_error)) {
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.UPDATE_BANK_DETAILS.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        if (!$user_id) {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthrised Access",
                              "msg_code"=>UNAUTH,
                              'data'=>null
                            ];
                        }
                        $data=$this->bank_details_dao->updateBankDetails($user_id, $temp1);
                        if ($data['status']==SUCCESS_STATUS) {
                            return [
                            "status"=>SUCCESS_STATUS,
                            "msg"=>U_BANK_DETAILS_SUCC,
                            "msg_code"=>SUCCESS_CODE
                            ];
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>U_BANK_DETAILS_FAIL,
                            "msg_code"=>FUNCTION_CODE.UPDATE_BANK_DETAILS.BO,
                            'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_create] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.UPDATE_BANK_DETAILS.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    public function getBankDetailsViaIFSC($req)
    {
        $key=array('ifsc');
        $mandatory=mandatory($key, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required($key, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.BANK_DETAILS_AJAX.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, array('ifsc'=>'required|AlphaNum|min:11|max:11'));
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.BANK_DETAILS_AJAX.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $data=$this->bank_details_dao->getBankDetailsViaIFSC($temp1['ifsc']);
                        if ($data) {
                            return [
                            "status"=>SUCCESS_STATUS,
                            "msg"=>BANK_DETAILS_AJAX_SUCC,
                            "msg_code"=>SUCCESS_CODE,
                            'data'=>$data
                            ];
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>BANK_DETAILS_AJAX_FAIL,
                            "msg_code"=>FUNCTION_CODE.BANK_DETAILS_AJAX.BO,
                            'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_create] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.BANK_DETAILS_AJAX.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    private function bankDetailsSanitizer($postdata)
    {
        $keys=array('secbankname','secbranch','secmicr','secifsc','secaccno','secaccno_confirmation','secacc_type','secbranch_address');//dynamic
        foreach ($keys as $key) {
            if ($postdata[$key]!=null) {
                return false;
            }
        }
        return true;
    }
}
