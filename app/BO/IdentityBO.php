<?php
namespace App\BO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Mail;
use Log;
use App\DAO\IdentityDAO;
use App\BO\Common;
use App\DAO\AdhaarResponseDAO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

class IdentityBO
{
    public function __construct()
    {
        $this->identity_dao=new IdentityDAO();
        $this->adhaar_response_dao=new AdhaarResponseDAO();
    }

    public function postOtpAdhaar($req)
    {
        $mandatory=mandatory(M_P_ADHAAR_OTP, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(M_P_ADHAAR_OTP, $req);
            if (!$temp1) {
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>REQUIRED_VALIDATION,
                  "msg_code"=>REQUIRED_CODE.P_OTP_ADHAAR.BO,
                  "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_P_ADHAAR_OTP);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                      "status"=>FAIL_STATUS,
                      "msg"=>FIELD_VALIDATION,
                      "msg_code"=>VALIDATION_CODE.P_OTP_ADHAAR.BO,
                      "data"=>$return_error
                    ];
                } else {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        if ($user_id) {
                            $adhaar_user=$this->identity_dao->getIdentityByAdhaar($req['adhaar']);
                            if ($adhaar_user->id!=$id) {
                                return [
                                'status'=>FAIL_STATUS,
                                'msg'=>"Adhaar registered with another client.",
                                "msg_code"=>FUNCTION_CODE.P_OTP_ADHAAR.BO,
                                'data'=>null
                                ];
                            }
                            return $this->postEkycOtp($temp1, $user_id);
                        } else {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthorised Access",
                              "msg_code"=>UNAUTH,
                              'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[IdentityBO_postOtpAdhaar] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>MANDATORY_VALIDATION,
              "msg_code"=>MANDATORY_CODE.P_OTP_ADHAAR.BO,
              "data"=>$mandatory['data']
            ];
        }
    }

    public function getOtpAdhaar($req)
    {
        $mandatory=mandatory(M_G_ADHAAR_OTP, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(M_G_ADHAAR_OTP, $req);
            if (!$temp1) {
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>REQUIRED_VALIDATION,
                  "msg_code"=>REQUIRED_CODE.G_OTP_ADHAAR.BO,
                  "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_G_ADHAAR_OTP);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                      "status"=>FAIL_STATUS,
                      "msg"=>FIELD_VALIDATION,
                      "msg_code"=>VALIDATION_CODE.G_OTP_ADHAAR.BO,
                      "data"=>$return_error
                    ];
                } else {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        if ($user_id) {
                            $data=$this->generateEkycOtpApi($temp1);
                            if ($data['status']==SUCCESS_STATUS) {
                                $this->identity_dao->updateOtpTxnIdAndTimestamp($user_id, $data['data']);
                            }
                            return $data;
                        } else {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthorised Access",
                              "msg_code"=>UNAUTH,
                              'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[IdentityBO_getOtpAdhaar] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>MANDATORY_VALIDATION,
              "msg_code"=>MANDATORY_CODE.G_OTP_ADHAAR.BO,
              "data"=>$mandatory['data']
            ];
        }
    }

    private function generateEkycOtpApi($input)
    {
        try {
            $asp_id = EKYC_ASP_ID;//'nasp10077'
            $txn_id = substr(hash('sha256', mt_rand().microtime()), 0, 20).'-ek-get-otp';
            $cert_path =public_path().NESIGN_CERT_PATH;//NESIGN_CERT_PATH;
            $cert_pwd = NESIGN_CERT_PWD;//'ncode';
            $issue_date = date('Y-m-d H:i:s');
            $adhar_id = $input ['adhaar'];
            $url = NESIGN_EKYC_OTP_REQ;//"http://localhost:8080/neSignv2/EkycReq_nekycOTPRequest";
            $otp_xml = '<NekycOtpRequest uid="' . $adhar_id . '" aspid="' . $asp_id . '"
  										ts="' . $issue_date . '" txn="' . $txn_id . '">
  									<signCert>' . $cert_path . '</signCert>
  									<password>' . $cert_pwd . '</password>
  									</NekycOtpRequest>';
            $response = $this->postXMLApi($otp_xml, $url);
            if ($response['status'] == SUCCESS_STATUS) {//API_SUCCESS
                $response = $response['data'];
            } else {
                return $response;
            }
            try {
                if ($response [0] ['attributes'] ['ERRORCODE'] != 'NA') {
                    $this->mailAPIError($txn_id, $response [0] ['attributes'] ['ERRORCODE'], $response [0] ['attributes'] ['ERRORMESSAGE'], $adhar_id);
                    return [
                        'status'=>FAIL_STATUS,//API_ERROR,
                        'data'=>[
                          'txn_id'=>$txn_id,
                          'adhaar'=>$adhar_id,
                          'ts'=>$issue_date
                        ],
                        'msg'=>$response [0] ['attributes'] ['ERRORMESSAGE'],
                        'msg_code'=>FUNCTION_CODE.'generateEkycOtpApi'.BO
                    ];
                }
            } catch (Exception $exception) {
                Log::error(' [IdentityBO] ' . ' [generateEkycOtpApi] ' . "Error msg: ".$exception->getMessage());
                return [
                  'status'=>FAIL_STATUS,//API_ERROR,
                  'data'=>null,
                  'msg'=>"API_CATCH_MSG", //ERROR_GENERIC
                  'msg_code'=>'ERROR_TYPE_CATCH'.'generateEkycOtpApi'.'ERROR_IN_BO'
                ];
            }
            return [
              'status'=>SUCCESS_STATUS,//API_SUCCESS,
              'data'=>[
                'txn_id'=>$txn_id,
                'adhaar'=>$adhar_id,
                'ts'=>$issue_date
              ],
              'msg'=>'OTP_SUCC',
              'msg_code'=>SUCCESS_CODE
            ];
        } catch (Exception $exception) {
            Log::error(' [IdentityBO] ' . ' [postXMLApi] ' . "Error msg: ".$exception->getMessage());
            return [
              'status'=>FAIL_STATUS,//API_ERROR,
              'data'=>null,
              'msg'=>'Oopss! Something went wrong. Please try again',
              'msg_code'=>TRYCATCH.'generateEkycOtpApi'.BO
            ];
        }
    }

    public function postXMLApi($xml_data, $url)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/xml'
            ));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $content = curl_exec($ch);
            if (curl_error($ch)) {
                Log::debug(' [IdentityBO] ' . ' [postXMLApi] ' . "error: ".curl_error($ch));
                return [
                  'status'=>FAIL_STATUS,//API_ERROR,
                  'data'=>null,
                  'msg'=>'Oopss! Something went wrong. Please try again',//ERROR_GENERIC
                  'msg_code'=>FUNCTION_CODE.'postXMLApi'.BO
                ];
            }
            curl_close($ch);
            $response = $this->xmlToArrayApi($content);
            Log::debug(' [IdentityBO] ' . ' [postXMLApi] ' . "ends");
            if ($response['status'] == SUCCESS_STATUS) {//API_SUCCESS
                $response = $response['data'];
            } else {
                return $response;
            }
            return [
              'status'=>SUCCESS_STATUS,//API_SUCCESS,
              'data'=>$response,
              'msg'=>'API_SUCCESS_MSG_CODE',
              'msg_code'=>'API_SUCCESS_MSG_CODE'
            ];
        } catch (Exception $exception) {
            Log::debug(' [IdentityBO] ' . ' [postXMLApi] ' . "Error msg: ".$exception->getMessage());
            return [
              'status'=>FAIL_STATUS,//API_ERROR,
              'data'=>null,
              'msg'=>'Oopss! Something went wrong. Please try again',//ERROR_GENERIC
              'msg_code'=>TRYCATCH.'postXMLApi'.BO
            ];
        }
    }

    public function xmlToArrayApi($xml)
    {
        try {
            Log::debug(' [IdentityBO] ' . ' [xmlToArrayApi] ' . "xmlToArray");
            $simple = $xml;
            $p = xml_parser_create();
            xml_parse_into_struct($p, $simple, $response, $index);
            xml_parser_free($p);
            return [
              'status'=>SUCCESS_STATUS,//API_SUCCESS,
              'data'=>$response,
              'msg'=>'API_SUCCESS_MSG_CODE',
              'msg_code'=>'API_SUCCESS_MSG_CODE'
            ];
        } catch (Exception $exception) {
            Log::debug(' [IdentityBO] ' . ' [xmlToArrayApi] ' . "Error: ".$exception->getMessage());
            return [
              'status'=>FAIL_STATUS,
              'data'=>null,
              'msg'=>'Oopss! Something went wrong. Please try again',
              'msg_code'=>TRYCATCH.'xmlToArrayApi'.BO
            ];
        }
    }

    public function mailAPIError($txn_id, $error_code, $error_msg, $user_aadhaar)
    {
        try {
            $ClientName=NCODE_API_ISSUE_CONTACT_NAME;//'TradSmart Staff';
            $ClientEmail=NCODE_API_ISSUE_CONTACT_EMAIL;//'ekycalert@vnsfin.com';//nCODE_API_ISSUE_CONTACT_EMAIL;
            $from_mail=FROM_EMAIL;//'contactus@vnsfin.com';
            $fromname=FROM_NAME;//'TradeSmart Online';
            return Mail::send(
                'email.API-error-email',
                [
                  'name' => $ClientName,
                  'error_msg' => $error_msg,
                  'user_aadhaar' => $user_aadhaar,
                  'error_code' => $error_code,
                  'txn_id' => $txn_id,
                ],
                function ($message) use ($error_msg, $ClientName, $ClientEmail, $fromname, $from_mail, $user_aadhaar, $error_code, $txn_id) {
                    $message->to($ClientEmail, $ClientName)->subject(NCODE_API_ISSUE_EMAIL_SUBJECT);//nCode API error Alert
                    $message->from($from_mail, $fromname);
                }
            );
        } catch (Exception $ex) {
            Log::error("[IdentityBO_mailAPIError] ".$ex);
        }
    }

    private function postEkycOtp($request, $user_id)
    {
        try {
            $post_otp = $this->postEkycOtpApi($request, $user_id);
            if ($post_otp['status'] == SUCCESS_STATUS) {//API_SUCCESS
                $this->identity_dao->updateOtpTxnIdAndTimestamp($user_id, $post_otp['data']);
            }
            return $post_otp;
        } catch (Exception $exception) {
            Log::error(' [IdentityBO] ' . ' [postEkycOtp] ' . $exception->getMessage());
            return [
              'status'=>FAIL_STATUS,
              'data'=> null,
              'msg'=>$ermsg,
              'msg_code'=>TRYCATCH.'postEkycOtp'.BO
            ];
        }
    }

    private function postEkycOtpApi($input_data, $id)
    {
        try {
            Log::error(' [IdentityBO] ' . ' [postEkycOtpApi] ' . "postEkycOtpApi");

            $asp_id =EKYC_ASP_ID; //'nasp10077';
            $txn_id = substr(hash('sha256', mt_rand() . microtime()), 0, 20).'-ek-post-otp';
            $cert_path =public_path().NESIGN_CERT_PATH;//public_path(). '/sign/TEST_IIB_ONLY_SIGN.pfx';
            $cert_pwd = NESIGN_CERT_PWD;//'ncode';
            $issue_date = date('Y-m-d H:i:s');
            $url = NESIGN_EKYC_REQ;//'http://localhost:8080/neSignv2/EkycReq_nekycRequest';
            $adhar_id = $input_data['adhaar'];
            $txn_id =  $input_data['txn_id'];
            $otp =  $input_data['pin'];
            $otp_xml = '
    		<nekycRequest ts="' . $issue_date . '" txn="' . $txn_id . '" uid="' . $adhar_id . '"
    		aspId="' . $asp_id . '" eSignClass="1" ver="2.1" pfr="Y"
    		de="N" lr="N" ra="O" rc="Y">
    		<otp>' . $otp . '</otp>
    		<bfd></bfd>
    		<FingerPos></FingerPos>
    		<Udc></Udc>
    		<signCert>' . $cert_path . '</signCert>
    		<password>' . $cert_pwd . '</password>
    		</nekycRequest>';
            $response = $this->postXMLApi($otp_xml, $url);
            if ($response['status'] == SUCCESS_STATUS) {//API_SUCCESS
                $response = $response['data'];
            } else {
                return $response;
            }
            try {
                if ($response [0] ['attributes'] ['ERRORCODE'] != 'NA') {
                    Log::debug(' [IdentityBO] ' . ' [postEkycOtpApi] ' . "Unable to fetch data from aadhar api ");
                    $this->mailAPIError($txn_id, $response [0] ['attributes'] ['ERRORCODE'], $response [0] ['attributes'] ['ERRORMESSAGE'], $adhar_id);
                    return [
                      'status'=>FAIL_STATUS,
                      'data'=>null,
                      'msg'=>$response [0] ['attributes'] ['ERRORMESSAGE'],//ERROR_GENERIC
                      'msg_code'=>$response [0] ['attributes'] ['ERRORCODE'],
                    ];
                }
            } catch (Exception $exception) {
                return [
                  'status'=>FAIL_STATUS,
                  'data'=>null,
                  'msg'=>$exception->getMessage(),//ERROR_GENERIC
                  'msg_code'=>'catch error1'
                ];
            }
            $EKYCREQ = $EKYCRESP = '';
            foreach ($response as $key => $value) {
                if ($value ['tag'] == 'EKYCREQ') {
                    $EKYCREQ = $value ['value'];
                }
                if ($value ['tag'] == 'EKYCRESP') {
                    $EKYCRESP = $value ['value'];
                }
            }
            $EKYCRESP_raw = ( $EKYCRESP );
            $EKYCRESP = base64_decode($EKYCRESP);
            $EKYCRESP = $this->xmlToArrayApi($EKYCRESP);
            if ($EKYCRESP['status'] == SUCCESS_STATUS) {//API_SUCCESS
                $EKYCRESP = $EKYCRESP['data'];
            } else {
                return $EKYCRESP;
            }
            try {
                if ($EKYCRESP [0] ['attributes'] ['ERRCODE'] != 'NA') {
                    $this->mailAPIError($txn_id, $EKYCRESP [0] ['attributes'] ['ERRCODE'], $EKYCRESP [0] ['attributes'] ['ERRMSG'], $adhar_id);
                    return [
                      'status'=>FAIL_STATUS,
                      'data'=>null,
                      'msg'=>$EKYCRESP [0] ['attributes'] ['ERRMSG'],//ERROR_GENERIC
                      'msg_code'=>$EKYCRESP [0] ['attributes'] ['ERRCODE']
                    ];
                }
            } catch (Exception $exception) {
                return [
                  'status'=>FAIL_STATUS,
                  'data'=>null,
                  'msg'=>$exception->getMessage(),//ERROR_GENERIC
                  'msg_code'=>'catcherror2'
                ];
            }
            $KYCRES = '';
            foreach ($EKYCRESP as $key => $value) {
                if ($value ['tag'] == 'KYCRES') {
                    $KYCRES = $value ['value'];
                }
            }
            $KYCRES = base64_decode($KYCRES);
            $KYCRES = $this->xmlToArrayApi($KYCRES);
            if ($KYCRES['status'] == SUCCESS_STATUS) {//API_SUCCESS
                $KYCRES = $KYCRES['data'];
            } else {
                return $KYCRES;
            }
            $aadhar_pdf = $KYCRES [7] ['value'];
            $personal_info = $KYCRES [3] ['attributes'];
            $address_details = $KYCRES [4] ['attributes'];
            $aadhar_id = $KYCRES [2] ['attributes'] ['UID'];
            Log::debug(' [IdentityBO] ' . ' [postEkycOtpApi] ' . "Fetched adhar details along with image . ");
/** Sanket --start--**/
            $t=$this->identity_dao->getDir($id, null, $aadhar_id);
            $uploads_dir=$t.'/';
/** Sanket --end--**/
            $img_data =base64_decode($KYCRES [6] ['value']);
            $file = $uploads_dir . 'profile_picture.jpeg';
            $success = file_put_contents($file, $img_data);

            Log::debug(' [IdentityBO] ' . ' [postEkycOtpApi] ' . "Image saved . ");

            $aadhar_pdf_decoded = base64_decode($aadhar_pdf);
            $pdf_writer = fopen($uploads_dir.'aadhaar.pdf', 'w');
            fwrite($pdf_writer, $aadhar_pdf_decoded);
            fclose($pdf_writer);
            $myfile = fopen($uploads_dir."ekyc-response.txt", "w");
            fwrite($myfile, $EKYCRESP_raw);
            fclose($myfile);

            Log::debug(' [IdentityBO] ' . ' [postEkycOtpApi] ' . "created txt file for KYC response");

            $check_aadhar = $this->adhaar_response_dao->checkAadharResponseApi($adhar_id);
            if ($check_aadhar==null) {
                $inert_aadhar = $this->adhaar_response_dao->insertAadharResponseApi($adhar_id);
            }
            $upadte_aadhar = $this->adhaar_response_dao->updateAadharResponseApi($adhar_id, $personal_info, $address_details);
            Log::debug(' [IdentityBO] ' . ' [postEkycOtpApi] ' . "Updated aadhar response in aadhar table. ");

            $data = $this->adhaar_response_dao->checkAadharResponseApi($adhar_id);
            $data = $this->filterSpecialChars($data['attributes']);
            $upadte_user_details = $this->identity_dao->updatePersonalDetailsAadharResponseApi($id, $data, $adhar_id);

            Log::debug(' [IdentityBO] ' . ' [postEkycOtpApi] ' . "Updated details in user details table. ");
            Log::debug(' [IdentityBO] ' . ' [postEkycOtpApi] ' . "set time-stamp for expiry of Kyc aadhar response");
            return [
            'status'=>SUCCESS_STATUS,
            'data'=>[
              'txn_id'=>$txn_id,
              'ts'=>$issue_date,
              'adhaar'=>$adhar_id,
              'adhaar_details'=>$data,
              'adhaar_kyc_string'=>$EKYCRESP_raw,
            ],
            'msg'=>'complete e-Sign within 15 minutes from '.$issue_date,
            'msg_code'=>SUCCESS_CODE
            ];
        } catch (Exception $exception) {
            return [
              'status'=>FAIL_STATUS,
              'data'=>null,
              'msg'=>$exception->getMessage(),//ERROR_GENERIC
              'msg_code'=>TRYCATCH.'IdentityBO_postEkycOtpApi'.BO
            ];
        }
    }

    private function filterSpecialChars($data)
    {
        $return_array = array();
        foreach ($data as $key => $value) {
            if ($key == 'CO') {
                $char = trim(preg_replace('/[^a-zA-Z. ]/', "", $value));
                $return_array[$key] = substr($char, strpos($char, "/O") + 2);
            } elseif ($key == 'DOB') {
                $return_array[$key] = trim(preg_replace('/[^a-zA-Z0-9\/,-. ]/', "", $value));
            } else {
                $return_array[$key] = trim(preg_replace('/[^a-zA-Z0-9\/,. ]/', "", $value));
            }
        }
        return $return_array;
    }

    public function getUserDetailsViaCLVKRA($req)
    {
        $mandatory=mandatory(M_G_PAN_CVLKRA, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(M_G_PAN_CVLKRA, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.G_PAN_CLV.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_G_PAN_CVLKRA);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.G_PAN_CLV.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        if ($user_id) {
                            $data=$this->identity_dao->getPanDetailsViaAPI($temp1['pan']);
                            if ($data) {
                                return[
                                'status'=>SUCCESS_STATUS,
                                'msg'=>G_PAN_CLV_SUCC,
                                'msg_code'=>SUCCESS_CODE,
                                'data'=>$data
                                ];
                            } else {
                                return [
                                  "status"=>FAIL_STATUS,
                                  "msg"=>"PAN number not found",
                                  "msg_code"=>FUNCTION_CODE.G_PAN_CLV.BO,
                                  "data"=>null
                                ];
                            }
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>"Unauthorised Access",//G_PAN_CLV_FAIL,
                            "msg_code"=>UNAUTH,
                            'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[IdentityBO_getUserDetailsViaCLVKRA] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.G_PAN_CLV.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    public function fetchUserDetailsViaCLVKRA($req)
    {
        $mandatory=mandatory(M_F_PAN_CVLKRA, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(M_F_PAN_CVLKRA, $req);
            if (!$temp1) {
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>REQUIRED_VALIDATION,
                  "msg_code"=>REQUIRED_CODE.F_PAN_CLV.BO,
                  "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_F_PAN_CVLKRA);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                      "status"=>FAIL_STATUS,
                      "msg"=>FIELD_VALIDATION,
                      "msg_code"=>VALIDATION_CODE.F_PAN_CLV.BO,
                      "data"=>$return_error
                    ];
                } else {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        //$user_id=54;
                        if ($user_id) {
                            $user_pan=getIdentityByPAN($temp1['pan'], $user_id);
                            if (!empty($user_pan)) {
                                return [
                                    'status'=>FAIL_STATUS,
                                    'msg'=>"Pan registered with another client.",
                                    'msg_code'=>FUNCTION_CODE.F_PAN_CLV.BO,
                                    'data'=>null
                                ];
                            }
                            $data=$this->identity_dao->fetchPanDetailsViaAPI($temp1, $user_id);
                            if ($data['status']==FAIL_STATUS) {
                                $data['msg_code']=FUNCTION_CODE.F_PAN_CLV.BO;
                                $data['data']=null;
                            }
                            return $data;
                        } else {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthorised Access",
                              "msg_code"=>UNAUTH,
                              'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[IdentityBO_getUserDetailsViaCLVKRA] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>MANDATORY_VALIDATION,
              "msg_code"=>MANDATORY_CODE.F_PAN_CLV.BO,
              "data"=>$mandatory['data']
            ];
        }
    }

    public function updateUserDetails($req)
    {
        try {
            $temp1=required(R_UPDATE_USER_DETAILS, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE."updateUserDetails".BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, array_merge(V_UPDATE_USER_DETAILS, V_A_FIELD), array_merge(V_DECIMAL, V_REG_USER_MSG, V_PAN));
                $return_error = array();
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE."updateUserDetails".BO,
                    "data"=>$return_error
                    ];
                } else {
                    $temp1=$this->setNull($temp1);
                    if ($temp1['occupation']=='OTHERS') {
                        $validator3=Validator::make($temp1, array('occup_other'=>'required|alpha_spaces'), V_REG_USER_MSG);
                        if ($validator3->fails()) {
                            $errors = $validator3->errors();
                            foreach ($errors->getMessages() as $key => $val) {
                                $return_error[$key] = $val[0];
                            }
                        }
                    }
                    if ($temp1['inv_exp']==1) {
                        $validator2=Validator::make($temp1, V_INV_EXP);
                        if ($validator2->fails()) {
                            $errors = $validator2->errors();
                            foreach ($errors->getMessages() as $key => $val) {
                                $return_error[$key] = $val[0];
                            }
                        }
                        if ($temp1['exp_year']=='0'&&$temp1['exp_month']=='0') {
                            $return_error['investment_exp']="exp_year and exp_month both cant be zero";
                        }
                        if (!empty($return_error)) {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>FIELD_VALIDATION,
                            "msg_code"=>VALIDATION_CODE."updateUserDetails".BO,
                            "data"=>$return_error
                            ];
                        }
                    }
                    $position=array();
                    if ($temp1['fatca_flg']==1) {
                        $position=$this->checkFatca($temp1);
                        if (!empty($position)) {
                            for ($i=0; $i<count($position); $i++) {
                                if ($position[$i]==0) {
                                    $validator4=Validator::make($temp1, V_FATCA1, V_REG_USER_MSG);
                                } elseif ($position[$i]==1) {
                                    $validator4=Validator::make($temp1, V_FATCA2, V_REG_USER_MSG);
                                } elseif ($position[$i]==2) {
                                    $validator4=Validator::make($temp1, V_FATCA3, V_REG_USER_MSG);
                                }
                                if ($validator4->fails()) {
                                    $errors = $validator4->errors();
                                    foreach ($errors->getMessages() as $key => $val) {
                                        $return_error[$key] = $val[0];
                                    }
                                }
                            }
                        } else {
                            $return_error['fatca']="Fatca Details insuffient";
                        }
                    }
                    $user_id=authUser($temp1['api_token']);
                    if ($user_id) {
                        $validator1=null;
                        $user_group=$this->identity_dao->getUserGroup($user_id, $req['user_group']);
                        if ($user_group!=3&&$temp1['addr_chk_flg']!=1) {
                            $validator1=Validator::make(
                                $temp1,
                                array_merge(V_A_FIELD, V_COR_USER_DETAILS, V_PER_USER_DETAILS),
                                array_merge(V_REG_USER_MSG, V_ADDRESS)
                            );
                        } elseif ($user_group!=3&&$temp1['addr_chk_flg']==1) {
                            $validator1=Validator::make(
                                $temp1,
                                array_merge(V_A_FIELD, V_COR_USER_DETAILS),
                                array_merge(V_REG_USER_MSG, V_ADDRESS)
                            );
                        } elseif ($user_group==3&&$temp1['addr_chk_flg']!=1) {
                            $validator1=$validator1=Validator::make(
                                $temp1,
                                V_PER_USER_DETAILS,
                                array_merge(V_REG_USER_MSG, V_ADDRESS)
                            );
                        }
                        if ($validator1&&$validator1->fails()) {
                            $errors = $validator1->errors();
                            foreach ($errors->getMessages() as $key => $val) {
                                $return_error[$key] = $val[0];
                            }
                        }
                        if (!empty($return_error)) {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>FIELD_VALIDATION,
                            "msg_code"=>VALIDATION_CODE."updateUserDetails".BO,
                            "data"=>$return_error
                            ];
                        } else {
                            $user_update=$this->identity_dao->updateUserDetails($user_id, $user_group, $temp1, $position);
                            if ($user_update) {
                                if(empty($user_update->client_code)) {
                                    $this->getClientCode($temp1);
                                }
                                $identity_details=$this->identity_dao->getIdentity($user_id);
                                return[
                                'status'=>SUCCESS_STATUS,
                                'msg'=>"User details updated",
                                'msg_code'=>SUCCESS_CODE,
                                'data'=>[
                                  'client_code'=>$identity_details->client_code
                                ]
                                ];
                            } else {
                                return[
                                'status'=>FAIL_STATUS,
                                'msg'=>"unable to update user details",
                                'msg_code'=>FUNCTION_CODE.'updateUserDetails'.BO,
                                'data'=>null,
                                ];
                            }
                        }
                    } else {
                        return [
                        "status"=>FAIL_STATUS,
                        "msg"=>"Unauthorised Access",
                        "msg_code"=>UNAUTH,
                        "data"=>null
                        ];
                    }
                }
            }
        } catch (Exception $ex) {
            Log::error('[IdentityBO_updateUserDetails]'.$ex);
        }
    }

    private function setNull($temp)
    {
        $per=array('p_address1','p_address2','p_address3','p_city','p_state','p_pincode','p_address_proof');
        $inv=array('exp_year','exp_month');
        if ($temp['addr_chk_flg']==1) {
            foreach ($per as $key) {
                $temp[$key]=null;
            }
        }
        if ($temp['inv_exp']==0) {
            foreach ($inv as $key) {
                $temp[$key]=0;
            }
        }
        if ($temp['occupation']!='OTHERS') {
            $temp['occup_other']=null;
        }
        return $temp;
    }

    private function checkFatca($temp)
    {
        $position=array();
        $checkArray1=array('country1','country2','country3');
        $checkArray2=array('identification1','identification2','identification3');
        $checkArray3=array('identification_type1','identification_type2','identification_type3');
        for ($i=0; $i<3; $i++) {
            if (!empty($temp[$checkArray1[$i]])||!empty($temp[$checkArray2[$i]])||!empty($temp[$checkArray3[$i]])) {
                $position[]=$i;
            }
        }
        return $position;
    }

    public function getUserDetails($req)
    {
        try {
            $key=array('api_token');
            $mandatory=mandatory($key, $req);
            if ($mandatory['status']==SUCCESS_STATUS) {
                $temp1=required($key, $req);
                if (!$temp1) {
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>REQUIRED_VALIDATION,
                    "msg_code"=>REQUIRED_CODE."getUserDetails".BO,
                    "data"=>null
                    ];
                } else {
                    $validator=Validator::make($temp1, array('api_token' =>'required|max:300'));
                    if ($validator->fails()) {
                        $errors = $validator->errors();
                        foreach ($errors->getMessages() as $key => $val) {
                            $return_error[$key] = $val[0];
                        }
                        return [
                          "status"=>FAIL_STATUS,
                          "msg"=>FIELD_VALIDATION,
                          "msg_code"=>VALIDATION_CODE."getUserDetails".BO,
                          "data"=>$return_error
                        ];
                    } else {
                        $user_id=authUser($temp1['api_token']);
                        if ($user_id) {
                            $data=$this->identity_dao->getUserDetails($user_id);
                            if ($data) {
                                return [
                                "status"=>SUCCESS_STATUS,
                                "msg"=>"User personal details found",
                                "msg_code"=>SUCCESS_CODE,
                                "data"=>[
                                  'person'=>[
                                'client_code'=>$data['person']['client_code'],
                                'name'=>$data['person']['name'],
                                'mother_name'=>$data['person']['mother_name'],
                                'father_name'=>$data['person']['father_name'],
                                'dob'=>$data['person']['dob'],
                                'marital_status'=>$data['person']['marital_status'],
                                'gender'=> $data['person']['gender'],
                                'adhaar'=>$data['person']['adhaar_no'],
                                'pan'=>$data['person']['pan'],
                                'correspondence_address'=>$data['person']['c_address'],
                                'correspondence_address1'=>$data['person']['c_address1'],
                                'correspondence_address2'=>$data['person']['c_address2'],
                                'correspondence_address3'=>$data['person']['c_address3'],
                                'correspondence_city'=>$data['person']['c_city'],
                                'correspondence_state'=>$data['person']['c_state'],
                                'correspondence_pincode'=>$data['person']['c_pincode'],
                                'correspondence_address_proof'=>$data['person']['c_address_proof'],
                                'address_flag'=>$data['person']['addr_chk_flg'],
                                'permanent_address1'=>$data['person']['p_address1'],
                                'permanent_address2'=>$data['person']['p_address2'],
                                'permanent_address3'=>$data['person']['p_address3'],
                                'permanent_city'=>$data['person']['p_city'],
                                'permanent_state'=>$data['person']['p_state'],
                                'permanent_pincode'=>$data['person']['p_pincode'],
                                'permanent_address_proof'=>$data['person']['p_address_proof'],
                                'experince_investment'=>$data['person']['inv_exp'],
                                'experince_year'=>$data['person']['exp_year'],
                                'experince_month'=>$data['person']['exp_month'],
                                  ],
                                  'fatca'=>$data['fatca']
                                ]
                                ];
                            } else {
                                return [
                                "status"=>FAIL_STATUS,
                                "msg"=>"Unable to fetch user data",
                                "msg_code"=>FUNCTION_CODE."getUserDetails".BO,
                                "data"=>null
                                ];
                            }
                        } else {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthorised Access",
                              "msg_code"=>UNAUTH,
                              "data"=>null
                            ];
                        }
                    }
                }
            } else {
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>MANDATORY_VALIDATION,
                  "msg_code"=>MANDATORY_CODE.'getUserDetails'.BO,
                  "data"=>$mandatory['data']
                ];
            }
        } catch (Exception $ex) {
            Log::error('[IdentityBO_getUserDetails] '.$ex);
        }
    }

    public function getClientCode($req)
    {
        try {
            $rule=array(
              'api_token'=>'required|max:300',
            );
            $validator=Validator::make($req, $rule);
            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($errors->getMessages() as $key => $val) {
                    $return_error[$key] = $val[0];
                }
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>FIELD_VALIDATION,
                  "msg_code"=>VALIDATION_CODE."getClientCode".BO,
                  "data"=>$return_error
                ];
            }
            $user_id=authUser($req['api_token']);
            if ($user_id) {
                $data=$this->identity_dao->getIdentity($user_id);
                if ($data->client_code) {
                    return [
                      "status"=>SUCCESS_STATUS,
                      "msg"=>'Client Code found',
                      "msg_code"=>SUCCESS_CODE,
                      "data"=>[
                        'client_code'=>$data->client_code
                      ]
                    ];
                }
                $keys=array('c_state','c_city','pan','name');
                foreach ($keys as $key) {
                    if (empty($data->$key)) {
                        return [
                          "status"=>FAIL_STATUS,
                          "msg"=>$key." not found",
                          "msg_code"=>FUNCTION_CODE."getClientCode".BO,
                          "data"=>null
                        ];
                    }
                }
                $state_code=$this->identity_dao->getStateCode($data->c_state);
                if (!$state_code) {
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>"State not found",
                    "msg_code"=>FUNCTION_CODE."getClientCode".BO,
                    "data"=>null
                    ];
                }
                $curl =new Common();
                $post_array = array (
                  "state_code"=>$state_code,
                  "client_city"=>$data->c_city,
                  "name"=>$data->name,
                  "pan_card"=>$data->pan,
                  "mode"=>'get_client_code'//need to constant
                );
                $execute_curl_result=$curl->callCurl($post_array);
                if ($execute_curl_result) {
                    $d=$this->identity_dao->assignClientCode($user_id, $execute_curl_result);
                    if ($d) {
                        return [
                        "status"=>SUCCESS_STATUS,
                        "msg"=>'Client Code Created',
                        "msg_code"=>SUCCESS_CODE,
                        "data"=>[
                          'client_code'=>$execute_curl_result
                        ]
                        ];
                    } else {
                        return [
                        "status"=>FAIL_STATUS,
                        "msg"=>"Something Went Worng!DB",//db
                        "msg_code"=>FUNCTION_CODE."getClientCode".BO,
                        "data"=>null
                        ];
                    }
                } else {
                    return [
                      "status"=>FAIL_STATUS,
                      "msg"=>"Something Went Worng!Cl",//curl
                      "msg_code"=>FUNCTION_CODE."getClientCode".BO,
                      "data"=>null
                    ];
                }
            } else {
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>"Unauthorised Access",
                  "msg_code"=>UNAUTH,
                  "data"=>null
                ];
            }
        } catch (Exception $ex) {
            Log::error('[IdentityBO_getClientCode] '.$ex);
        }
    }
}
