<?php
namespace App\BO;

use Validator;
use App\DAO\BaseMasterDAO;
use Log;
use Exception;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

class BaseMasterBO
{
    public function __construct()
    {
        $this->base_master_dao=new BaseMasterDAO();
    }
    public function getPlanList()
    {
        try {
            $data=$this->base_master_dao->getPlanList();
            if ($data) {
                return [
                  'data'=>json_decode(json_encode($data)),
                  'msg'=>"Active Plan List",
                  'msg_code'=>SUCCESS_CODE,
                  'status'=>SUCCESS_STATUS
                ];
            } else {
                return [
                  'msg'=>"Active Plan List",
                  'msg_code'=>FUNCTION_CODE."getPlanList".BO,
                  'status'=>FAIL_STATUS
                ];
            }
        } catch (Exception $ex) {
            Log::error("[BaseMasterBO_getPlanList] ".$ex);
        }
    }
}
