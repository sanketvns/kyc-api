<?php
namespace App\BO;

use Request;
use App\DAO\PdfGeneratorDAO;
use Validator;
use Exception;
use \setasign\Fpdi\Fpdi as FPDI;
use mikehaertl\pdftk\Pdf;
use File;
use Log;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

class PdfGeneratorBO
{
    public function __construct()
    {
        $this->pdf_generator_dao=new PdfGeneratorDAO();
    }

    public function generatePdf($req)
    {
        // $key=array('api_token');
        // $mandatory=mandatory($key, $req);
        // if ($mandatory==FAIL_STATUS) {
        //     return [
        //     'status'=>FAIL_STATUS,
        //     'msg'=>MANDATORY_VALIDATION,
        //     'msg_code'=>MANDATORY_CODE."generatePdf".BO,
        //     'data'=>$mandatory['data']
        //     ];
        // }
        // $temp1=required($key, $req);
        // if (empty($temp1)) {
        //     return [
        //     "status"=>FAIL_STATUS,
        //     "msg"=>REQUIRED_VALIDATION,
        //     "msg_code"=>REQUIRED_CODE."generatePdf".BO,
        //     "data"=>null
        //     ];
        // }

        $validator=Validator::make($req, V_PDFGEN);//array('api_token'=>'required|max:300'));
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->getMessages() as $key => $val) {
                $return_error[$key] = $val[0];
            }
            return [
            "status"=>FAIL_STATUS,
            "msg"=>FIELD_VALIDATION,
            "msg_code"=>VALIDATION_CODE."generatePdf".BO,
            "data"=>$return_error
            ];
        }
        $t=$this->validateMode($req);
        if ($t['status']==FAIL_STATUS) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>FIELD_VALIDATION,
              "msg_code"=>VALIDATION_CODE."generatePdf".BO,
              "data"=>$t['msg']
            ];
        }
        $user_id=authUser($req['api_token']);
        // $user_id=54;
        if (!$user_id) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>"Unauthorised Access",
            "msg_code"=>UNAUTH,
            "data"=>null
            ];
        }
        $user=$this->pdf_generator_dao->getUser($user_id);
        $flag_array=array('identity_flag','trading_flag','bank_flag');
        foreach ($flag_array as $key) {
            if ($user->$key==0) {
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>str_replace('_flag', '', $key)." details not filled",
                  "msg_code"=>FUNCTION_CODE."generatePdf".BO,
                  "data"=>null
                ];
            }
        }
        $identity=$this->pdf_generator_dao->getIdentity($user_id);
        if (!$identity->client_code) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>"Client code is not generated",
              "msg_code"=>FUNCTION_CODE."generatePdf".BO,
              "data"=>null
            ];
        }
        $fatca="";
        if ($identity->fatca_flag) {
            $fatca=$this->pdf_generator_dao->getFatca($user_id);
        }
        $trading_details=$this->pdf_generator_dao->getTrading($user_id);
        $bank_details=$this->pdf_generator_dao->getBankDetails($user_id);
        $payment=$this->pdf_generator_dao->getPayment($user_id);
        $mode=array_key_exists('mode', $req)?$req['mode']:'';
        $pdfArray=$this->pdfArray($user, $identity, $trading_details, $bank_details, $fatca, $payment->final_amt, $req['txn_id'], $mode);
        $uploads_dir=getdir1($user_id);
        $removefile=array('final_merged.pdf','final_merged_Sign.pdf','combined_images.pdf');
        foreach ($removefile as $key) {
            if(file_exists("$uploads_dir/$key")){
                unlink("$uploads_dir/$key");
            }
        }
        if (!$uploads_dir) {
            return [
              "status"=>FAIL_STATUS,
              'msg'=>"Directory not found",
              'msg_code'=>FUNCTION_CODE."generatePdf".BO,
              'data'=>null
            ];
        }
        $upload_doc=$this->uploadKYCDoc($user->user_group, $trading_details, $uploads_dir, $pdfArray, $fatca);
        convertImagesToPDF($uploads_dir);
        mergePDFs($uploads_dir);
        $dir=substr(strrchr($uploads_dir, "/"), 1);
        if (file_exists($uploads_dir.'/final_merged.pdf')) {
            return [
              'status'=>SUCCESS_STATUS,
              'msg'=>'final_merged.pdf generated',
              'data'=>Request::server('SERVER_NAME')."/public/uploads/$dir/final_merged.pdf",//
              //'data'=> Request::server('SERVER_NAME')."/public/upload/$dir/final_merged_sign.pdf",
              'msg_code'=>SUCCESS_CODE
            ];
        } else {
            return [
              'status'=>FAIL_STATUS,
              'msg'=>'final_merged.pdf is not generated',
              'data'=>null,
              'msg_code'=>FUNCTION_CODE."generatePdf".BO,
            ];
        }
    }

    private function pdfArray($user, $identity, $trading_details, $bank_details, $fatca, $final_amt, $txn_id, $mode)
    {
        $fdf_data_strings = array (
          "Today"=>date("d-M-Y"),
          "Mumbai" => "Mumbai",
          "OWN DP NAME" => strtoupper($identity->name),
          "Check Box1" => 'Yes',
          "Check Box8" => 'Yes',
          "Check Box12" => 'Yes',
          "Check Box13" => 'Yes',
          "Check Box18" => 'Yes',
          "Check Box25" => 'Yes',
          "Account opening Payment mode" => $mode,
          "A/c opening charges" => $final_amt,
          "Transaction ID" => $txn_id,
          "Name"=>strtoupper($identity->name),
          "Father's/Spouse Name" => $identity->father_name,
          'Gender' => $identity->gender=="M"?"Male":"Female",
          "Marital Status" => $identity->marital_status,
          "Date Of Birth" => date("d/m/Y", strtotime($identity->dob)),
          "Nationality" => "Indian",
          'Status' => 'Resident Individual',//DON'T KNOW status
          "Pan" => $identity->pan,
          "Aadhar" => $identity->adhaar_no,
          "Correspondance Address" => $identity->c_address1.' '.$identity->c_address2.' '.$identity->c_address3,
          "City" => $identity->c_city,
          "Pin" => $identity->c_pincode,
          "State" => $identity->c_state,
          "Country" => "India",//,

          /*address Proof*/
          "Proof Of Iden" => $identity->c_address_proof,//DON'T KNOW
          "Proof Of Identity" => $identity->c_address_proof,
          "pProof Of Identity" => $identity->p_address_proof,
          //
          "pProof Of Address Correspondence" => $identity->c_address_proof,
          "pProof Of Address permanent" => $identity->p_address_proof,

          /*ends*/
          "Permanent Address" => $identity->p_address1.' '.$identity->p_address2.' '.$identity->p_address3,
          "Permanent City" => $identity->p_city,
          "Permanent Pin" => $identity->p_pincode,
          "Permanent State" => $identity->p_state,
          "Permanent Country" => "India",
          "Email" => $user->email,
          "Mobile" => $user->phone,
          // "Phone Office" => $row ['phone_office'],
          "Phone Res" => $user->phone,
          // "Fax" => $row ['fax'],
          "Gross Annual Income Details" => $identity->annual_income,
          "Check Box16" => $identity->annual_income,
          "Net Worth" => $identity->net_worth,
          "As On" => date("d/m/Y", strtotime($identity->occ_date)),
          "Occupation" =>ucwords(strtolower($identity->occupation)),
          "Pri Bank Name" => $bank_details->bankname,
          "Pri Branch Name" => $bank_details->branch,
          "Pri Bank Address" =>  $bank_details->branch_address,
          "Pri Bank AC NO" => $bank_details->acc_no,
          "Primary Account Type" => $bank_details->acc_type,
          "Pri MICR" => $bank_details->MICR,
          "Pri IFSC" => $bank_details->IFSC,
          "Sec Bank Name" =>$bank_details->sec_bankname,
          "Sec Branch Name" => $bank_details->sec_branch,
          "Sec Bank Address" => $bank_details->sec_branch_address,
          "Sec Bank AC NO" => $bank_details->sec_acc_no,
          "Sec Account Type" => $bank_details->sec_acc_type,
          "Sec MICR" => $bank_details->sec_MICR,
          "Sec IFSC" => $bank_details->sec_IFSC,
//           "Introducer's name" => $row ['introducer_name'],
//           "Introducer's Phone Number" => $row ['introducer_phone_no'],
//           "Introducer's Client Id" => $row ['introducer_client_id'],
          "DP1 Name" => $trading_details->demat_name,
          "DP1 Id" => $trading_details->demat_dp,
          "DP1 Acc ID" => $trading_details->demat_bo,
          "DP2 Name" => $trading_details->sec_demat_name,
          "DP2 Id" => $trading_details->sec_demat_dp,
          "DP2 Acc ID" => $trading_details->sec_demat_bo,
          "I want to trade in Currency Derivatives" => empty($trading_details->currency_flag)?'':"I want to trade in Currency Derivatives",
          "Client Name" => strtoupper($identity->name),
          "Mobile No" => $user->phone,
          'ekyc_client_code' => $identity->client_code,//set atstrick not implemented
          'Check Pan' => 'Yes',
          "Mother's Name" =>$identity->mother_name,
          "Mother Name" =>$identity->mother_name,
//           /**/
          "fatca"=>empty($identity->fatca_flag)?'fatca_no':'fatca_yes',
          "dis"=>$trading_details->DIS_flag==1?'dis_yes':'dis_no',
          "sno1"=>1,
          "sno2"=>2,
          "sno3"=>3,
          "nse_curr"=>$trading_details->currency_flag?'Yes':'',
          "mcx"=>preg_match("/MCX/", $trading_details->commodity)?'Yes':'',
          "ncdex"=>preg_match("/NCDEX/", $trading_details->commodity)?'Yes':'',
          "branch"=>'INTERNET',
          "preference"=>'quarterly',
//           "intro_by"=>$intro_by,
          "doc_verify"=>'Paresh S.',
          "ipv_perform"=>'Paresh S.',
          "emp_des"=>'Sr.Executive',
          "trading_platform"=>"NEST",
          "check aadhar"=>$user->user_group==3?'Yes':''
        );
        $Occupation=$this->occupationDetails(['occupation'=>$identity->occupation]);
        if ($Occupation) {
            $fdf_data_strings=array_merge($fdf_data_strings, $Occupation);
        }
        if ($trading_details->equity_flag) {
            $equity=$this->equityDetails($trading_details->equity);
            $fdf_data_strings=array_merge($fdf_data_strings, $equity);
        }
        if ($identity->fatca_flag) {
            $fatca1=$this->getFatca($fatca);
            $fdf_data_strings=array_merge($fdf_data_strings, $fatca1);
        }
        $plan=$this->getPlanDetails($trading_details);
        $fdf_data_strings=array_merge($fdf_data_strings, $plan);
        return $fdf_data_strings;
    }

    private function uploadKYCDoc($user_group, $trading_details, $uploads_dir, $pdfArray)
    {
        try {
            $uploads_dir.='/';
            $files = glob($uploads_dir.'KYC-*');
            foreach ($files as $file) {
                unlink($file);
            }
            $trading_seg=$this->formatSeg($trading_details);
            foreach ($trading_seg as $key) {
                if ($key == 'equity') {
                    try {
                        $filename = 'KYC-FIN-'.str_replace(" ", "-", trim($pdfArray["Client Name"])).'.pdf';
                        File::delete($uploads_dir.$filename);
                        $pdf = new Pdf(EQUITY_PDF_PATH_SERVER);
                        $t=$pdf->fillForm($pdfArray)
                        ->flatten()
                        ->saveAs($uploads_dir.$filename);
                        $return_file_names= $filename;
                        chmod($uploads_dir.$filename, 0777);
                        Log::debug('[PdfGeneratorBO_uploadKYCDoc] '.'Equity file created from Remote centralised server.');
                    } catch (Exception $ex) {
                        $pdf = new Pdf(EQUITY_PDF_PATH_LOCAL);
                        $filename = 'KYC-FIN-'.str_replace(" ", "-", trim($pdfArray["Client Name"])).'.pdf';
                        File::delete($uploads_dir.$filename);
                        $pdf->fillForm($pdfArray)
                        ->flatten()
                        ->saveAs($uploads_dir.$filename);
                        $return_file_names= $filename;
                        chmod($uploads_dir.$filename, 0777);
                        Log::debug('[PdfGeneratorBO_uploadKYCDoc] '.'Equity file created from Local server.');
                    }
                }
                if ($key=='commodity') {
                    try {
                        $pdf = new Pdf(COMMODITY_PDF_PATH_SERVER);
                        $filename = 'KYC-COMM-'.str_replace(" ", "-", trim($pdfArray["Client Name"])).'.pdf';
                        File::delete($uploads_dir.$filename);
                        $pdf->fillForm($pdfArray)
                        ->flatten()
                        ->saveAs($uploads_dir.$filename);
                        $return_file_names= $filename;
                        chmod($uploads_dir.$filename, 0777);
                        Log::debug('[PdfGeneratorBO_uploadKYCDoc] '.'Commodity file created  from Remote centralised server.');
                    } catch (Exception $ex) {
                        $pdf = new Pdf(COMMODITY_PDF_PATH_LOCAL);
                        $filename = 'KYC-COMM-'.str_replace(" ", "-", trim($pdfArray["Client Name"])).'.pdf';
                        File::delete($uploads_dir.$filename);
                        $pdf->fillForm($pdfArray)
                        ->flatten()
                        ->saveAs($uploads_dir.$filename);
                        $return_file_names= $filename;
                        chmod($uploads_dir.$filename, 0777);
                        Log::debug('[PdfGeneratorBO_uploadKYCDoc] '.'Commodity file created  from Local server.');
                    }
                }
            }

            if ($user_group==3 && file_exists($uploads_dir.'/profile_picture.jpeg')) {
                foreach ($trading_seg as $key) {
                    if ($key == 'equity') {
                        $filename = 'KYC-FIN-'.str_replace(" ", "-", trim($pdfArray["Client Name"])).'.pdf';
                        $pdf = new FPDI();
                        $pdf->setSourceFile($uploads_dir.$filename);
                        $pageCount = $pdf->setSourceFile($uploads_dir.$filename);
                        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                            $tplIdx = $pdf->importPage($pageNo);
                            $pdf->addPage();
                            $pdf->useTemplate($tplIdx);//, null, null, 100, 100, true);
                            if ($pageNo == 4) {
                                $pdf->Image($uploads_dir.'/profile_picture.jpeg', 168, 9, 32, 32);
                            }
                            if ($pageNo == 21) {
                                $pdf->Image($uploads_dir.'/profile_picture.jpeg', 177, 95, 28, 28);
                            }
                        }
                        $pdf->Output($uploads_dir.$filename, 'F');
                        chmod($uploads_dir.$filename, 0777);
                    }
                    if ($key == 'commodity') {
                        $filename = 'KYC-COMM-'.str_replace(" ", "-", trim($pdfArray["Client Name"])).'.pdf';
                        $pdf = new FPDI();
                        $pdf->setSourceFile($uploads_dir.$filename);
                        $pageCount = $pdf->setSourceFile($uploads_dir.$filename);

                        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                            $tplIdx = $pdf->importPage($pageNo);
                            $pdf->addPage();
                            $pdf->useTemplate($tplIdx);//, null, null, 100, 100, true);
                            if ($pageNo == 4) {
                                $pdf->Image($uploads_dir.'/profile_picture.jpeg', 168, 9, 32, 32);
                            }
                        }
                        $pdf->Output($uploads_dir.$filename, 'F');
                        chmod($uploads_dir.$filename, 0777);
                    }
                }
            }
        } catch (Exception $ex) {
            Log::error('[PdfGeneratorBO_uploadKYCDoc] '.$ex);
        }
    }

    private function formatSeg($trading_details)
    {
        $temp=array();
        if ($trading_details->equity_flag) {
            $temp[]='equity';
        }
        if ($trading_details->commodity_flag) {
            $temp[]='commodity';
        }
        return $temp;
    }

    private function occupationDetails($identity)
    {
        $validator=Validator::make($identity, ['occupation'=>'exists:cvl_kra.Occupation,Description']);
        if ($identity['occupation']!='OTHERS'&&$validator->fails()) {
            return [
              "Occupation"=>"Other",
              "Others"=>$identity['occupation']
            ];
        }
        return null;
    }

    private function equityDetails($equity)//should be dynamic
    {
        $t=array();
        $equity=explode(',', trim($equity));
        $seg=array();
        foreach ($equity as $key) {
            $seg[]=trim($key);
        }
        foreach ($seg as $key) {
            switch ($key) {
                case 'NSE_FNO':
                    $t['nse_fno']='Yes';
                    $t['NSE F&N']='NSE F&O';
                    break;
                case 'NSE_CASH':
                    $t['nse_cash']='Yes';
                    $t['NSE CASH']='NSE Cash';
                    break;
                case 'BSE_FNO':
                    $t['bse_fno']='Yes';
                    break;
                case 'BSE_CASH':
                    $t['bse_cash']='Yes';
                    $t['BSE CASH']='BSE Cash';
            }
        }
        return $t;
    }

    private function getplanDetails($trading_details)//should be dynamic
    {
        $data=$this->pdf_generator_dao->getPlanDetails();
        $plan=$t=array();
        for ($i=0; $i<count($data); $i++) {
            $plan[$data[$i]->plan_name]=$data[$i]->plan_desc;
        }
        if ($trading_details->equity_flag==1) {
            $t['Plan1']=$plan[$trading_details->plan_equity];
        }
        if ($trading_details->commodity_flag==1) {
            $m='';
            switch ($trading_details->plan_commodity) {
                case 'PS7':
                    $m='7 Paisa';
                    break;
                case 'RS15':
                    $m='Rs 15';
                    break;
                case 'UNL':
                    $m='Unlimited';
            }
            $t['Pricing Plan']=$m;
        }
        if ($trading_details->currency_flag==1) {
            $t['Plan2']=$plan[$trading_details->plan_equity];
        }
        return $t;
    }

    private function getFatca($fatca)
    {
        $t=array();
        for ($i=0; $i<count($fatca); $i++) {
            $t['country_tax_residency'.$i]=$fatca[$i]['country'];
            $t['tax_payer_id'.$i]=$fatca[$i]['identification'];
            $t['identification_type'.$i]=$fatca[$i]['identification_type'];
        }
        return $t;
    }

    private function validateMode($req)
    {
        if (($req['status']=='Success'||$req['status']=='Pending')&&!array_key_exists('mode', $req)) {
            return [
              'status'=>FAIL_STATUS,
              'msg'=>"mode is required"
            ];
        } else {
            return [
              'status'=>SUCCESS_STATUS
            ];
        }
    }
}
