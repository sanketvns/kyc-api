<?php
namespace App\BO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Log;
use Mail;
use App\DAO\UserDAO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

/**
 *
 */

class UserBO
{
    public function __construct()
    {
        $this->user_dao = new UserDAO();
    }

    // define("CREATE", 'REG_U');
    public function create($req)
    {
        $mandatory=mandatory(REG_USER, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(REG_USER, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.CREATE.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_REG_USER, V_REG_USER_MSG);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.CREATE.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $data=$this->user_dao->create($temp1);
                        // $this->_leadentry($data['name'], $data['phone'], $data['email']);
                        if ($data) {
                            $temp1=$this->user_dao->authenticate($req['email'], $req['password']);
                            return [
                              "status"=>SUCCESS_STATUS,
                              "msg"=>CREATE_USER_SUCC,
                              "msg_code"=>SUCCESS_CODE,
                              'data'=>$temp1['data']
                            ];
                        } else {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>CREATE_USER_FAIL,
                              "msg_code"=>FUNCTION_CODE.CREATE.BO,
                              'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_create] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.CREATE.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    // define("EMAILAVAILIBILITY", 'EMAIL_AVAIL');
    public function emailAvailibity($req)
    {
        $key=array('email');
        $mandatory=mandatory($key, $req);
        if ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.EMAILAVAILIBILITY.BO,
            "data"=>$mandatory['data']
            ];
        } elseif ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required($key, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.EMAILAVAILIBILITY.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_EMAIL_AVAILABLE, ["email.unique"=>"This email ID is already registered"]);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                      "status"=>FAIL_STATUS,
                      "msg"=>FIELD_VALIDATION,
                      "msg_code"=>VALIDATION_CODE.EMAILAVAILIBILITY.BO,
                      "data"=>$return_error
                    ];
                } else {
                    return [
                    "status"=>SUCCESS_STATUS,
                    "msg"=>EMAILAVAILIBILITY_SUCC,
                    "msg_code"=>SUCCESS_CODE
                    ];
                }
            }
        }
    }

    public function callLeadentry($req)
    {
        $mandatory=mandatory(L_ENTRY, $req);
        if ($mandatory['status']==FAIL_STATUS) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>MANDATORY_VALIDATION,
              "msg_code"=>MANDATORY_CODE.CALLLEADENTRY.BO,
              "data"=>$mandatory['data']
            ];
        } elseif ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(L_ENTRY, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.CALLLEADENTRY.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_EMAIL_AVAILABLE, ["email.unique"=>"This email ID is already registered"]);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                      return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.CALLLEADENTRY.BO,
                    "data"=>$return_error
                      ];
                } else {
                    $data1=json_decode($this->_leadentry($temp1['name'], $temp1['phone'], $temp1['email']), true);
                    $data1['status']=$data1['status']=="100"?FAIL_STATUS:SUCCESS_STATUS;
                    $msg_code=$data1['status']!=SUCCESS_STATUS?FUNCTION_CODE.CALLLEADENTRY.BO:SUCCESS_CODE;
                    return [
                      "status"=>$data1['status'],
                      "msg"=>$data1['message'],
                      "msg_code"=>$msg_code
                    ];
                }
            }
        }
    }

    public function authenticate($req)
    {
        $mandatory=mandatory(LOGIN, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(LOGIN, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.AUTHENTICATE.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_LOGIN_USER, V_EXISTS);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.AUTHENTICATE.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $data=$this->user_dao->authenticate($temp1['email'], $temp1['password']);
                        if ($data['status']==SUCCESS_STATUS) {
                            return [
                              "status"=>SUCCESS_STATUS,
                              "msg"=>AUTHENTICATE_SUCC0,
                              "msg_code"=>SUCCESS_CODE,
                              "data"=>$data['data']
                            ];
                        } else {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>$data['msg']?$data['msg']:AUTHENTICATE_FAIL0,
                              "msg_code"=>FUNCTION_CODE.AUTHENTICATE.BO,
                              'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_authenticate] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.AUTHENTICATE.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    public function changePassword($req)
    {
        $mandatory=mandatory(CHANGEPASSWORD, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(CHANGEPASSWORD, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.CHANGE_PASSWORD.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_CHANGEPASSWORD);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.CHANGE_PASSWORD.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        if (!$user_id) {
                              return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthorised Access",
                              "msg_code"=>UNAUTH,
                              "data"=>null
                              ];
                        }
                        $data=$this->user_dao->changePassword($user_id, $temp1);
                        if ($data['status']==SUCCESS_STATUS) {
                            return [
                              "status"=>SUCCESS_STATUS,
                              "msg"=>CHANGE_PASSWORD_SUCC,
                              "msg_code"=>SUCCESS_CODE,
                              'data'=>$data['data']
                            ];
                        } else {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>$data['msg']?$data['msg']:CHANGE_PASSWORD_FAIL,
                              "msg_code"=>FUNCTION_CODE.CHANGE_PASSWORD.BO,
                              'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_create] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.CHANGE_PASSWORD.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    public function logout($req)
    {
        $key=array('api_token');
        $mandatory=mandatory($key, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required($key, $req);
            if (!$temp1) {
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>REQUIRED_VALIDATION,
                  "msg_code"=>REQUIRED_CODE.LOGOUT.BO,
                  "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, array('api_token'=>'required|max:300'));
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                      "status"=>FAIL_STATUS,
                      "msg"=>FIELD_VALIDATION,
                      "msg_code"=>VALIDATION_CODE.LOGOUT.BO,
                      "data"=>$return_error
                    ];
                } else {
                    try {
                      // print_r($temp1);exit;
                        $data=$this->user_dao->logout($temp1);
                        if ($data['status']==SUCCESS_STATUS) {
                            return [
                            "status"=>SUCCESS_STATUS,
                            "msg"=>LOGOUT_SUCC,
                            "msg_code"=>SUCCESS_CODE
                            ];
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>LOGOUT_FAIL,
                            "msg_code"=>FUNCTION_CODE.LOGOUT.BO,
                            'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_create] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>MANDATORY_VALIDATION,
              "msg_code"=>MANDATORY_CODE.LOGOUT.BO,
              "data"=>$mandatory['data']
            ];
        }
    }

    public function checkEmail($req)
    {
        $key=array('email');
        $mandatory=mandatory($key, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required($key, $req);
            if (!$temp1) {
                return [
                  "status"=>FAIL_STATUS,
                  "msg"=>REQUIRED_VALIDATION,
                  "msg_code"=>REQUIRED_CODE.CHECK_EMAIL.BO,
                  "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, array('email' => 'required|email|exists:users,email'), V_EXISTS);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.CHECK_EMAIL.BO,
                    "data"=>$return_error
                    ];
                } else {
                    return[
                    "status"=>SUCCESS_STATUS,
                    "msg"=>CHECK_EMAIL_SUCC,
                    "msg_code"=>SUCCESS_CODE,
                    "data"=>null
                    ];
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.CHECK_EMAIL.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    public function forgotPassword($req)
    {
        $mandatory=mandatory(FORGOT_PASSWORD, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(FORGOT_PASSWORD, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.FORGOTPASSWORD.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_FORGOT_PASSWORD, V_EXISTS);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                      "status"=>FAIL_STATUS,
                      "msg"=>FIELD_VALIDATION,
                      "msg_code"=>VALIDATION_CODE.FORGOTPASSWORD.BO,
                      "data"=>$return_error
                    ];
                } else {
                    $code = str_random(60);
                    $data=$this->user_dao->getUserViaEmailId($temp1['email']);
                    try {
                        if ($data) {
                            $token=$this->user_dao->addTokenToResetPwd($code, $temp1['email']);
                            if ($token) {
                                $temp2=['name'=>$token['name'],'token'=>$token['resetpassword'].'&data='.base64_encode($temp1['email'])];
                                $this->sendMail($temp1, $temp2);
                                return [
                                  "status"=>SUCCESS_STATUS,
                                  "msg"=>FTOKEN_SUCC,
                                  "msg_code"=>SUCCESS_CODE
                                ];
                            } else {
                                return [
                                "status"=>FAIL_STATUS,
                                "msg"=>FTOKEN_FAIL,
                                "msg_code"=>FUNCTION_CODE.FORGOTPASSWORD.BO,
                                'data'=>null
                                ];
                            }
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>EMAIL_INVALID,
                            "msg_code"=>FUNCTION_CODE.FORGOTPASSWORD.BO,
                            'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_create] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>MANDATORY_VALIDATION,
              "msg_code"=>MANDATORY_CODE.FORGOTPASSWORD.BO,
              "data"=>$mandatory['data']
            ];
        }
    }

    public function resetPassword($req)
    {
        $mandatory=mandatory(RESETPASSWORD, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required(RESETPASSWORD, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.RESETFPASSWORD.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, V_RESET_PASSWORD);
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.RESETFPASSWORD.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $data=$this->user_dao->getTokenfromDB($temp1['token']);
                        if ($data&&$this->checkFToken($temp1['token'])&&$this->user_dao->resetPassword($temp1['token'], $temp1['password'])) {
                                return [
                                "status"=>SUCCESS_STATUS,
                                "msg"=>CHANGE_PASSWORD_SUCC,
                                "msg_code"=>SUCCESS_CODE,
                                'data'=>null
                                ];
                        } else {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>INVALID_TOKEN,
                              "msg_code"=>FUNCTION_CODE.RESETFPASSWORD.BO,
                              'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[UserBO_create] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.RESETFPASSWORD.BO,
            "data"=>$mandatory['data']
            ];
        }
    }
    private function _leadentry($client_name, $mobile_no, $email_id)
    {
        $mobile_no = urlencode($mobile_no);
        $email_id = urlencode($email_id);
        $client_name = urlencode($client_name);
        $leadsource = urlencode("Online_kyc");
        $campeinName = urlencode("OnlineKyc");

        // Test Connection
        $lead_url = "http://192.168.27.33/leadentry-v2/v1/leadentry?description=&referrer_client_id=&name=$client_name&mobile=$mobile_no&email=$email_id&leadsource=Landing&campaignName=abc&city=&comb_query_str=&term=&device=&content=&call_back_flag=0&add_crm_flag=1&userId=Landing&pwd=187ph";
      // live connection
      // $lead_url = "http://api.tradesmartonline.in/leadentry/lead-entry.php?name=$client_name&phone=$mobile_no&email=$email_id&leadsource=$leadsource&campaignName=$campeinName&comb_query_str=&userId=WebsiteReferral&pwd=12def";

      // open connection
        $ch = curl_init();

        // set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $lead_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);
        return $result;
    }

    private function checkFToken($token)
    {
        return time()-substr($token, 60)<=3600;
    }

    private function sendMail($data1, $data2)
    {
        $mail_id=$data1['email'];
        $name=$data2['name'];
        Mail::send('email.forgotPassword', [
          'token' => $data2['token'],
          'name' => $data2['name'],
          'url'=>$data1['url'],
          'email'=>$data1['email']
        ], function ($message) use ($mail_id, $name) {
            $message->to($mail_id, $name)->subject('Reset Password');
            $message->from('sanket.gawde@vnsfin.com', 'Terminator');//test
        });
    }
}
