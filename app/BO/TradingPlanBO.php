<?php
namespace App\BO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Log;
use App\DAO\TradingPlanDAO;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

/**
 *
 */
class TradingPlanBO
{
    public function __construct()
    {
        $this->trading_plan_dao=new TradingPlanDAO();
    }

    public function getTradingPlanDetails($req)
    {
        $key=array('api_token');
        $mandatory=mandatory($key, $req);
        if ($mandatory['status']==SUCCESS_STATUS) {
            $temp1=required($key, $req);
            if (!$temp1) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>REQUIRED_VALIDATION,
                "msg_code"=>REQUIRED_CODE.G_TRADING_PLAN.BO,
                "data"=>null
                ];
            } else {
                $validator = Validator::make($temp1, array('api_token'=>'required|max:300'));
                if ($validator-> fails()) {
                    $errors = $validator->errors();
                    $return_error = array();
                    foreach ($errors->getMessages() as $key => $val) {
                        $return_error[$key] = $val[0];
                    }
                    return [
                    "status"=>FAIL_STATUS,
                    "msg"=>FIELD_VALIDATION,
                    "msg_code"=>VALIDATION_CODE.G_TRADING_PLAN.BO,
                    "data"=>$return_error
                    ];
                } else {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        if (!$user_id) {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthorised Access",
                              "msg_code"=>UNAUTH,
                              'data'=>null
                            ];
                        }
                        $data=$this->trading_plan_dao->getTradingPlanDetails($user_id);
                        if ($data['status']==SUCCESS_STATUS) {
                            return [
                            "status"=>SUCCESS_STATUS,
                            "msg"=>G_TRADING_PLAN_SUCC,
                            "msg_code"=>SUCCESS_CODE,
                            'data'=>[
                              'equity'=>$data['data']['equity'],
                              'equity_plan'=>$data['data']['plan_equity'],
                              'commodity'=>$data['data']['commodity'],
                              'commodity_plan'=>$data['data']['plan_commodity'],
                              'currency'=>$data['data']['currency'],
                              'currency_plan'=>$data['data']['plan_currency'],
                              'demat_acc_name'=>$data['data']['demat_name'],
                              'demat_DP_no'=>$data['data']['demat_dp'],
                              'demat_BO_no'=>$data['data']['demat_bo'],
                              'sec_demat_acc_name'=>$data['data']['sec_demat_name'],
                              'sec_demat_DP_no'=>$data['data']['sec_demat_dp'],
                              'sec_demat_BO_no'=>$data['data']['sec_demat_bo']
                            ]
                            ];
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>G_TRADING_PLAN_FAIL,
                            "msg_code"=>FUNCTION_CODE.G_TRADING_PLAN.BO,
                            'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[TradingPlanBO_getTradingPlanDetails] ".$ex);
                    }
                }
            }
        } elseif ($mandatory['status']==FAIL_STATUS) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>MANDATORY_VALIDATION,
            "msg_code"=>MANDATORY_CODE.GET_BANK_DETAILS.BO,
            "data"=>$mandatory['data']
            ];
        }
    }

    public function updateTradingPlanDetails($req)
    {
        $temp1=required(R_TRADINGPLAN, $req);
        if (!$temp1) {
            return [
            "status"=>FAIL_STATUS,
            "msg"=>REQUIRED_VALIDATION,
            "msg_code"=>REQUIRED_CODE.U_TRADING_PLAN.BO,
            "data"=>null
            ];
        } else {
            $validator = Validator::make($temp1, V_TRADINGPLAN);
            if ($validator-> fails()) {
                $errors = $validator->errors();
                $return_error = array();
                foreach ($errors->getMessages() as $key => $val) {
                    $return_error[$key] = $val[0];
                }
                return [
                "status"=>FAIL_STATUS,
                "msg"=>FIELD_VALIDATION,
                "msg_code"=>VALIDATION_CODE.U_TRADING_PLAN.BO,
                "data"=>$return_error
                ];
            } else {
                $segment=array('equity','commodity','currency');
                $temp1=$this->removeUnwanted($segment, $temp1);
                if ($this->enabledTradingPlan($segment, $temp1)) {
                    return [
                        "status"=>FAIL_STATUS,
                        "msg"=>"Atleast one segment should be selected",
                        "msg_code"=>VALIDATION_CODE.U_TRADING_PLAN.BO,
                        "data"=>null
                    ];
                }
                $return_error=array();
                foreach ($segment as $key) {
                    $raw=$this->tradingSegmentSantizer($key, $temp1);
                    if (!empty($raw)) {
                        $return_error[$key]=$raw;
                    }
                }
                if ($temp1['demat_flag']==0) {
                    if ($temp1['equity_f']==1&&$this->dematPrimaryValidation($temp1['equity_seg'])&&$this->dematPrimaryNullcheck($temp1)) {
                        $return_error['demat']="Demat details are insufficient";
                    } elseif ($temp1['equity_f']==1&&$this->dematPrimaryValidation($temp1['equity_seg'])&&!$this->dematPrimaryNullcheck($temp1)) {
                        $validator2=Validator::make($temp1, V_PRI_DEMAT, V_REG_USER_MSG);
                        $errors = $validator2->errors();
                        foreach ($errors->getMessages() as $key => $val) {
                            $return_error[$key] = $val[0];
                        }
                        // if (!$this->dematSecondaryNullcheck($temp1)&&!$this->dematSecondaryCommpletecheck($temp1)) {
                        //     $return_error['sec_demat']="Secondary Demat details are insufficient";
                        // } else {
                        //     $validator3=Validator::make($temp1, V_SEC_DEMAT, V_REG_USER_MSG);
                        //     $errors = $validator3->errors();
                        //     foreach ($errors->getMessages() as $key => $val) {
                        //         $return_error[$key] = $val[0];
                        //     }
                        // }
                    }
                }
                if (empty($return_error)) {
                    try {
                        $user_id=authUser($temp1['api_token']);
                        if (!$user_id) {
                            return [
                              "status"=>FAIL_STATUS,
                              "msg"=>"Unauthorised Access",
                              "msg_code"=>UNAUTH,
                              'data'=>null
                            ];
                        }
                        $data=$this->trading_plan_dao->updateTradingPlanDetails($user_id, $temp1);
                        if ($data['status']==SUCCESS_STATUS) {
                            return [
                            "status"=>SUCCESS_STATUS,
                            "msg"=>P_TRADING_PLAN_SUCC,
                            "msg_code"=>SUCCESS_CODE
                            ];
                        } else {
                            return [
                            "status"=>FAIL_STATUS,
                            "msg"=>P_TRADING_PLAN_FAIL,
                            "msg_code"=>FUNCTION_CODE.U_TRADING_PLAN.BO,
                            'data'=>null
                            ];
                        }
                    } catch (Exception $ex) {
                        Log::error("[TradingPlanBO_updateTradingPlanDetails] ".$ex);
                    }
                } else {
                    return [
                        "status"=>FAIL_STATUS,
                        "msg"=>FIELD_VALIDATION,
                        "msg_code"=>VALIDATION_CODE.U_TRADING_PLAN.BO,
                        "data"=>$return_error
                    ];
                }
            }
        }
    }

    private function enabledTradingPlan($segment, $postdata)
    {
        foreach ($segment as $key) {
            if ($postdata[$key.'_f']!=0) {
                return false;
            }
        }
        return true;
    }

    private function tradingSegmentSantizer($const, $postdata)
    {
        $temp1=array();
        if ($postdata[$const.'_f']==1) {
            if (empty($postdata[$const.'_seg'])) {
                $temp1[$const.'_seg']=$const.'_seg should be not empty';
            } else {
                $validator1=Validator::make($postdata, array($const.'_seg'=> 'duplicate|company_'.$const), array_merge(V_REG_USER, V_COMPANY_CODE, array('company_'.$const=>"The $const contains unregistered company code")));
                $errors = $validator1->errors();
                foreach ($errors->getMessages() as $key => $val) {
                    $temp1[$key] = $val[0];
                }
            }
            if (empty($postdata[$const.'_plan'])) {
                  $temp1[$const.'_plan']=$const.'_plan should not be empty';
            } else {
                $validator2=Validator::make($postdata, array($const.'_plan'=>'exists:plans,plan_name'));
                $errors = $validator2->errors();
                foreach ($errors->getMessages() as $key => $val) {
                    $temp1[$key] = $val[0];
                }
            }
        }
        return $temp1;
    }

    private function dematPrimaryValidation($equity)
    {
        return preg_match('/_CASH/', $equity);
    }

    private function dematPrimaryNullcheck($postdata)
    {
        $keys=array('demat_acc_name','demat_BO_no','demat_DP_no');//dynamic
        foreach ($keys as $key) {
            if ($postdata[$key]==null) {
                return true;
            }
        }
        return false;
    }

    // private function dematSecondaryNullcheck($postdata)
    // {
    //     $keys=array('sec_demat_acc_name','sec_demat_BO_no','sec_demat_DP_no');//dynamic
    //     foreach ($keys as $key) {
    //         if ($postdata[$key]!=null) {
    //             return false;
    //         }
    //     }
    //     return true;
    // }
    //
    // private function dematSecondaryCommpletecheck($postdata)
    // {
    //     $keys=array('sec_demat_acc_name','sec_demat_BO_no','sec_demat_DP_no');//dynamic
    //     foreach ($keys as $key) {
    //         if ($postdata[$key]==null) {
    //             return false;
    //         }
    //     }
    //     return true;
    // }

    private function removeUnwanted($segment, $postdata)
    {
        foreach ($segment as $key) {
            if ($postdata[$key.'_f']==0) {
                $postdata[$key.'_seg']=$postdata[$key.'_plan']=null;
            }
        }
        $keys1=array('demat_acc_name','demat_BO_no','demat_DP_no');//dynamic
        // $keys2=array('sec_demat_acc_name','sec_demat_BO_no','sec_demat_DP_no');//dynamic
        if ($postdata['demat_flag']==1) {
            foreach ($keys1 as $key) {
                $postdata[$key]="";
            }
            // foreach ($keys2 as $key) {
            //     $postdata[$key]="";
            // }
        }
        return $postdata;
    }
}
