<?php
namespace App\BO;

use Validator;
use App\DAO\NsdlDAO;
use Log;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

class NsdlBO
{
    public function __construct()
    {
        $this->nsdl_dao=new NsdlDAO();
    }
    public function getPanName($req)
    {
        $mandatory=mandatory(M_NSDL_API, $req);
        if ($mandatory['status']==FAIL_STATUS) {
            return [
                "status"=>FAIL_STATUS,
                "msg"=>MANDATORY_VALIDATION,
                "msg_code"=>MANDATORY_CODE.'getPanName'.BO,
                "data"=>$mandatory['data']
            ];
        }
        $temp1=required(M_NSDL_API, $req);
        if (empty($temp1)) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>MANDATORY_VALIDATION,
              "msg_code"=>MANDATORY_CODE.'getPanName'.BO,
              "data"=>null
            ];
        }
        $validator=Validator::make($temp1, V_NSDL_API, V_PAN);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->getMessages() as $key => $val) {
                $return_error[$key] = $val[0];
            }
            return [
                "status"=>FAIL_STATUS,
                "msg"=>FIELD_VALIDATION,
                "msg_code"=>VALIDATION_CODE."getPanName".BO,
                "data"=>$return_error
            ];
        }
        // $user_id=54;
        try {
            $user_id=authUser($temp1['api_token']);
            if (!$user_id) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>"Unauthorised Access",
                "msg_code"=>UNAUTH,
                "data"=>null
                ];
            }
            $temp1['pan']=strtoupper($temp1['pan']);
            $upload_dir=getDir1($user_id);
            // print_r($upload_dir);exit;
            if (!$upload_dir) {
                return [
                "status"=>FAIL_STATUS,
                "msg"=>"Directory not exists",
                "msg_code"=>FUNCTION_CODE."getPanName".BO,
                "data"=>null
                ];
            }
            $res=$this->callNSDLApi($upload_dir, $temp1['pan'], $user_id);
            if ($res) {
                return [
                  "status"=>SUCCESS_STATUS,
                  "msg_code"=>SUCCESS_CODE,
                  "msg"=>"Pan name is found",
                  'data'=>['name'=>$res->first_name." ".$res->middle_name." ".$res->last_name]
                ];
            } else {
                return [
                  "status"=>FAIL_STATUS,
                  "msg_code"=>FUNCTION_CODE."getPanName".BO,
                  "msg"=>"Unable fecth Pan name",
                  'data'=>null
                ];
            }
        } catch (Exception $ex) {
            Log::error("[NsdlBO_getPanName] ".$ex);
        }
    }

    private function callNSDLApi($upload_dir, $pan, $id)
    {
        try {
            $dir=substr(strrchr($upload_dir, "/"), 1);
            $nsdl_array = [
            $pan,
            NSDL_OUTPUT_JKS_FILE,
            NESIGN_CERT_PWD_NSDL,
            NSDL_USER_ID.'^',
            NSDL_REQUEST_SIG_FILE,
            public_path()."/uploads/",
            $dir
            ];
            $nsdl_params = (implode(" ", $nsdl_array));
            $check_existence=$this->nsdl_dao->checkNsdl($pan);
            if ($check_existence) {
                return $check_existence;
            }
            $output = ('cd '.NSDL_JAR_DIR.'; java -jar nsdl-ekyc-api-v2.jar '.$nsdl_params);
            $output = shell_exec($output);
            $panArray = explode("^", $output);
            if (array_key_exists(2, $panArray)&&$panArray[2] == NSDL_SUCCESS) {
                $save_response = $this->nsdl_dao->updateOrCreate($pan, $panArray);
                if ($save_response) {
                    $identity=$this->nsdl_dao->saveName($panArray[4]." ".$panArray[5]." ".$panArray[3], $id);
                }
              // Log::debug (Auth::user()->username. ' [PersonalBO] ' . ' [postPanDetails] ' . 'returning from  NSDL API.' );
                return $save_response;
            } else {
                return null;
            }
        } catch (Exception $ex) {
            Log::error("[NsdlBO_callNSDLApi]".$ex);
        }
    }
}
