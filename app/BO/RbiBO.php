<?php
namespace App\BO;

use Validator;
use App\DAO\RbiDAO;
use Log;

require_once app_path()."/helper/constants.php";
require_once app_path()."/helper/sanitizer.php";
require_once app_path()."/helper/validate.php";
require_once app_path()."/helper/common.php";

class RbiBO
{
    public function __construct()
    {
        $this->rbi_dao=new RbiDAO();
    }

    public function getBankDetails($req)
    {
        $key=array('ifsc');
        $mandatory=mandatory($key, $req);
        if ($mandatory['status']==FAIL_STATUS) {
            return [
                "status"=>FAIL_STATUS,
                "msg"=>MANDATORY_VALIDATION,
                "msg_code"=>MANDATORY_CODE.'getBankDetails'.BO,
                "data"=>$mandatory['data']
            ];
        }
        $temp1=required($key, $req);
        if (empty($temp1)) {
            return [
              "status"=>FAIL_STATUS,
              "msg"=>MANDATORY_VALIDATION,
              "msg_code"=>MANDATORY_CODE.'getBankDetails'.BO,
              "data"=>null
            ];
        }
        $validator=Validator::make($temp1, array('ifsc'=>"required|ifsc"), V_IFSC);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->getMessages() as $key => $val) {
                $return_error[$key] = $val[0];
            }
            return [
                "status"=>FAIL_STATUS,
                "msg"=>FIELD_VALIDATION,
                "msg_code"=>VALIDATION_CODE."getBankDetails".BO,
                "data"=>$return_error
            ];
        }
        try {
            $data=$this->rbi_dao->getBankDetails(strtoupper($req['ifsc']));
            if ($data) {
                return [
                  "status"=>SUCCESS_STATUS,
                  "msg_code"=>SUCCESS_CODE,
                  "msg"=>"Bank is found",
                  'data'=>[
                    'BANK'=>$data->BANK,
                    'IFSC'=>$data->IFSC,
                    'MICR'=>$data->MICR,
                    'BRANCH'=>$data->BRANCH,
                    'ADDRESS'=>$data->ADDRESS,
                    'STATE'=>$data->STATE,
                    'CITY'=>$data->CITY,
                    'CONTACT'=>$data->CONTACT,
                    'DISTRICT'=>$data->DISTRICT
                  ]
                ];
            } else {
                return [
                  "status"=>FAIL_STATUS,
                  "msg_code"=>FUNCTION_CODE."getBankDetails".BO,
                  "msg"=>"Unable fecth Bank Details",
                  'data'=>null
                ];
            }
        } catch (Exception $ex) {
            Log::error("[RbiBO_getBankDetails] ".$ex);
        }
    }
}
