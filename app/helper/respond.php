<?php
function success($status, $msg = null, $msg_code = null, $data = null)
{
    return  Response::json([
      'msg'=>$msg,
      'status'=>$status,
      'msg_code'=>$msg_code,
      'data'=>$data
    ]);
}

function fail($status, $msg = null, $msg_code = null, $data = null)
{
    return  Response::json([
      'msg'=>$msg,
      'status'=>$status,
      'msg_code'=>$msg_code,
      'data'=>$data
    ]);
}

// function error($msg, $status, $msg_code, $data = null)
// {
//     return  Response::json([
//       'msg'=>$msg,
//       'status'=>$status,
//       'msg_code'=>$msg_code,
//       'data'=>$data
//     ]);
// }
