<?php
require_once app_path().'/helper/common.php';
$data=getCompanyCode();
$equity_code=$currency_code=$commodity_code=array();
for ($i=0; $i<count($data); $i++) {
    switch ($data[$i]['segment']) {
        case 'equity':
            $equity_code=explode(',', $data[$i]['c_code']);
            break;
        case 'currency':
            $currency_code=explode(',', $data[$i]['c_code']);
            break;
        case 'commodity':
            $commodity_code=explode(',', $data[$i]['c_code']);
            break;
            // agar kuch naya add kana hai toh add case and corresponding Constants
    }
}
//master constants
define('EQUITY_CODE', $equity_code);
define('CURRENCY_CODE', $currency_code);
define('COMMODITY_CODE', $commodity_code);

//Credentials
/* Live */
// define('esign_url','https://gateway.emsigner.com/eMsecure/SignerGateway/Index');
// define('auth_token', 'Qt4hpy6NPdsHQh+D3A2E9Z5RdrCTqX/C3awNskI3lx1Bhpb+8jMRbZTZ0mDojfAT');

//test
define('esign_url', 'https://gateway.emsigner.com/eMsecure/SignerGateway/Index');
define('auth_token', '2ktry0OtTGHd1lZ16fl8RgZW36RH2R0CFoqHPbna3blX9gPKoqKepgwxyw9B8R62');



define('EKYC_ASP_ID', 'nasp10077');
define('NESIGN_CERT_PATH', '/sign/TEST_IIB_ONLY_SIGN.pfx');/**ncode Credentials**/
define('NESIGN_CERT_PWD', 'ncode');
define('NESIGN_EKYC_REQ', 'http://localhost:8080/neSignv2/EkycReq_nekycRequest');
define('NESIGN_EKYC_OTP_REQ', 'http://localhost:8080/neSignv2/EkycReq_nekycOTPRequest');
define('NCODE_API_ISSUE_CONTACT_NAME', 'TradSmart Staff');
define('NCODE_API_ISSUE_CONTACT_EMAIL', 'sanket.gawde@vnsfin.com');//ekycalert@vnsfin.com
define('FROM_EMAIL', 'contactus@vnsfin.com');
define('FROM_NAME', 'TradeSmart Online');
define('NCODE_API_ISSUE_EMAIL_SUBJECT', 'nCode API error Alert');
define('CVL_USERNAME', 'WEBVNSF');
define('CVL_POSCODE', '1400964330');
define('CVL_PASSWORD', 'EOeg1YnoQC3pjb5oB%2fl4LA%3d%3d');
define('CVL_PASSKEY', 'vns321');
define('CVL_URL', "http://www.cvlkra.com/PanInquiry.asmx/GetPanStatus");

define('ESIGN_ASP_ID', 'nasp10076');
define('NESIGN_ESIGN_REQ', 'http://localhost:8080/neSignv2/EsignReq_eSignRequestForPreverifedY');
define('ESIGN_MAILTO_VNS_BCC_EMAIL', 'email.log@vnsfin.com');
define('ESIGN_MAILTO_VNS_BCC_NAME', 'E-Sign Logs');
define('ESIGN_MAIL_SUBJECT_CLIENT', 'TradeSmart Online a/c opening form & Importance of POA');
define('ESIGN_MAILTO_VNS_1', 'ekyc.form@vnsfin.com');
define('ESIGN_MAILTO_VNS_2', 'pravin.dubey@vnsfin.com');
define('ESIGN_MAIL_SUBJECT_VNS', ' Online account opening form of ');

//NSDL
define('NSDL_USER_ID', 'V0164201');
define('NSDL_JAR_DIR', public_path().'/nsdl');
define('NSDL_OUTPUT_JKS_FILE', public_path().'/nsdl/java-pfx/deliverable/output.jks');
define('NSDL_REQUEST_SIG_FILE', 'output.sig');
define('NSDL_SUCCESS', 'E');
define('NESIGN_CERT_PATH_NSDL', public_path().'/sign/ekyc.pfx');
define('NESIGN_CERT_PWD_NSDL', 'vnsekyc#123');

//pdf url
define('EQUITY_PDF_PATH_SERVER', "http://tradesmartonline.in/wp-content/uploads/2015/05/vns-finance-account-opening-form.pdf");
define('COMMODITY_PDF_PATH_SERVER', "http://tradesmartonline.in/wp-content/uploads/2015/05/vns-commodities-account-opening-form.pdf");
define('EQUITY_PDF_PATH_LOCAL', "/pdffiles/vns-finance-account-opening-form.pdf");
define('COMMODITY_PDF_PATH_LOCAL', "/pdffiles/vns-commodities-account-opening-form.pdf");

//Controller,BO and DAO constants
define("CONTRL", '-01');
define("BO", '-02');
define("DAO", '-03');

//Method name
define("CREATE", 'REG_U');
define("EMAILAVAILIBILITY", 'EMAIL_AVAIL');
define('CALLLEADENTRY', 'LTR');
define('AUTHENTICATE', 'ATH');
define('CHANGE_PASSWORD', 'CPWD');
define('LOGOUT', 'LGT');
define('RESETFPASSWORD', 'FRSTP');
define('FORGOTPASSWORD', 'FPWD');
define('UPDATE_BANK_DETAILS', 'UBKD');
define('GET_BANK_DETAILS', 'GBKD');
define('CHECK_EMAIL', 'CKE');
define('BANK_DETAILS_AJAX', 'BKDA');
define('G_TRADING_PLAN', 'GTRP');
define('U_TRADING_PLAN', 'UTRP');
define('G_OTP_ADHAAR', 'GOTA');
define('P_OTP_ADHAAR', 'POTA');
define('G_PAN_CLV', 'GPC');
define('F_PAN_CLV', 'FPC');

//Status Constants
define("SUCCESS_STATUS", 3);
define("FAIL_STATUS", 1);
define("ERROR_STATUS", 2);

//Mandatory & Required Constants
define("REG_USER", array("name","phone","email","password","password_confirmation"));
define('L_ENTRY', array("name","phone","email"));
define('LOGIN', array('email','password'));
define('FORGOT_PASSWORD', array('email', 'url'));
define('CHANGEPASSWORD', array('api_token','old_password','password',"password_confirmation"));
define('RESETPASSWORD', array('token','password','password_confirmation'));
define('BANKDETAILS', array('api_token','bankname','branch','ifsc', 'micr','accno','accno_confirmation',
'acc_type','branch_address'));

define('BANKDETAILSR', array('api_token',
'bankname','branch','ifsc', 'micr','accno','accno_confirmation','acc_type','branch_address',
'secbankname','secbranch','secmicr','secifsc','secaccno','secaccno_confirmation','secacc_type','secbranch_address'));

define('M_TRADINGPLAN', array('api_token','equity_f','currency_f','commodity_f','DIS_flag'));
define('R_TRADINGPLAN', array('api_token','equity_f','currency_f','commodity_f',
                              'equity_seg','commodity_seg','currency_seg',
                              'equity_plan','commodity_plan','currency_plan',
                              'demat_acc_name','demat_DP_no','demat_BO_no',
                              'sec_demat_acc_name','sec_demat_DP_no','sec_demat_BO_no','demat_flag','DIS_flag'));

define('M_G_ADHAAR_OTP', array('api_token',"adhaar"));
define('M_P_ADHAAR_OTP', array('api_token',"adhaar",'pin','txn_id'));
define('M_G_PAN_CVLKRA', array('api_token','pan'));
define('M_F_PAN_CVLKRA', array('api_token','pan','dob'));

define('R_UPDATE_USER_DETAILS', array(
    'api_token','f_name','m_name','dob','gender','marital_status','annual_income','net_worth','occupation',
    'c_address1','c_address2','c_address3','c_city','c_state','c_pincode','c_address_proof',
    'p_address1','p_address2','p_address3','p_city','p_state','p_pincode','p_address_proof',
    'addr_chk_flg','occ_date','inv_exp','exp_year','exp_month','occup_other','fatca_flg',
    'country1','country2','country3','identification1','identification3','identification2',
    'identification_type1','identification_type2','identification_type3','pan','adhaar','user_group'
));

define('M_UPLOAD_IPV', array('api_token','ipv','pan'));
define('R_CALC_PAYMENT', array('email','discount'));
define('M_NSDL_API', array('api_token','pan'));

define('M_ESIGN', array('api_token','url'));

// define('M_CLIENT_CODE', array('',));

//Messages Constants
define('MANDATORY_VALIDATION', "Missing attributes");
define('FIELD_VALIDATION', "Fields Validation Failed");
define('CREATE_USER_SUCC', "Registration Successful");
define('CREATE_USER_FAIL', "Registration Failed");
define('EMAILAVAILIBILITY_SUCC', 'Email available');
define('CHANGE_PASSWORD_SUCC', 'Password successfully reset');
define('CHANGE_PASSWORD_FAIL', 'Unable to reset password');
define('INVALID_TOKEN', 'The link was invalid or has expired.');
define('AUTHENTICATE_SUCC1', "Already logged in");
define('AUTHENTICATE_SUCC0', "Login successful");
define('AUTHENTICATE_JWT_FAIL', 'Login Unsuccessful. An error occurred while performing an action!');
define('AUTHENTICATE_FAIL0', 'Invalid Email or Password');
define('LOGOUT_SUCC', "Logout successfully");
define('LOGOUT_FAIL', "Unable to logout");
define('FTOKEN_SUCC', "Resetpassword token generated");
define('FTOKEN_FAIL', "Token not generated");
define('EMAIL_INVALID', "Email doesn\'t exists");
define('BANK_DETAILS_SUCC', "Bank details Found");
define('BANK_DETAILS_FAIL', "Unable to fetch bank details");
define('U_BANK_DETAILS_SUCC', "Bank details updated");
define('U_BANK_DETAILS_FAIL', "Unable to update Bank details");
define('CHECK_EMAIL_SUCC', 'User Found');
define('BANK_DETAILS_AJAX_SUCC', 'Bank details found');
define('BANK_DETAILS_AJAX_FAIL', 'Bank details not found');
define('G_TRADING_PLAN_SUCC', "Trading plan details found");
define('G_TRADING_PLAN_FAIL', "Trading plan details not found");
define('P_TRADING_PLAN_SUCC', "Trading plan details updated");
define('P_TRADING_PLAN_FAIL', "Unable to update trading plan details");
define('G_OTP_ADHAAR_FAIL', 'Unable to send OTP adhaar regisered mobile number');
define('P_OTP_ADHAAR_FAIL', 'Unable to fetch data from Adhaar');
define('G_PAN_CLV_SUCC', 'PAN data found');
define('G_PAN_CLV_FAIL', 'PAN data not found');

//msg_code Constants
define('SUCCESS_CODE', 'VNS_SUCCESS_01');
define('MANDATORY_CODE', 'VM-');
define('REQUIRED_CODE', 'VR-');
define('VALIDATION_CODE', 'V-');
define('FUNCTION_CODE', 'F-');
define('TRYCATCH', 'C-');
define('UNAUTH', "H-UNAUTH-400");

//GLOBAL Constants
define('DEFAULT_DISCOUNT', 'default_discount');
