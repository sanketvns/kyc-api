<?php
function mandatory($keys, $postdata)
{
    foreach ($keys as $key) {
        if ((!array_key_exists($key, $postdata)) || empty(trim($postdata[$key]))) {
            return ["status"=>FAIL_STATUS,"data"=>$key." is required"];
        }
    }
    return ["status"=>SUCCESS_STATUS,'data'=>null];
}

function required($keys, $postdata)
{
    $temp=array();
    foreach ($keys as $key) {
        if (array_key_exists($key, $postdata)) {
            $temp[$key]=$postdata[$key];
        } else {
            $temp[$key]=null;
        }
    }
    return $temp;
}
