<?php
require_once app_path().'/helper/constants.php';

Validator::extend('phone_reg', function ($attribute, $value) {
    return preg_match('/^[7-9]\d{9}$/', $value);
});

Validator::extend('alpha_spaces', function ($attribute, $value) {
    return preg_match('/^[\pL\s]+$/u', $value);
});

Validator::extend('ifsc', function ($attribute, $value) {
    return preg_match('/^[A-Za-z]{4}\d{7}$/', $value);
});

Validator::extend('w_s_asp', function ($attribute, $value) {
    return preg_match('/^(\w*\s*[\-\,\/\.\(\)\&\:\+]*)+$/', $value);
});

Validator::extend('company_equity', function ($attribute, $value) {
    $equity=EQUITY_CODE;//dynamic
    $postdata=explode(',', trim($value));
    return count($postdata)==count(array_intersect($postdata, $equity));
});

Validator::extend('company_commodity', function ($attribute, $value) {
    $equity=COMMODITY_CODE;//dynamic
    $postdata=explode(',', trim($value));
    return count($postdata) == count(array_intersect($postdata, $equity));
});

Validator::extend('company_currency', function ($attribute, $value) {
    $equity=CURRENCY_CODE;//dynamic
    $postdata=explode(',', trim($value));
    return count($postdata) == count(array_intersect($postdata, $equity));
});

Validator::extend('duplicate', function ($attribute, $value) {
    $postdata=explode(',', trim($value));
    return count($postdata)==count(array_unique($postdata));
});

Validator::extend('pan', function ($attribute, $value) {
    return preg_match('/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/', $value);
});

Validator::extend('decimal', function ($attribute, $value) {
    return preg_match('/^\d+(\.\d\d)*$/', $value);
});

Validator::extend('address', function ($attribute, $value) {
    return preg_match('/^[a-zA-Z0-9\s\-,.\/\()&]*$/', $value);
});

define('V_PAN', array('pan' =>':attribute in invalid format'));

define('V_DECIMAL', array(
    'decimal'=>':attribute in invalid decimal format'
));

define('V_ADDRESS', array(
    'address' =>':attribute in wrong format'
));

define("V_REG_USER_MSG", array(
  'alpha_spaces'=>":attribute accepts only alphabets."
));

define('V_IFSC', array('ifsc' => 'the :attribute is invalid'));

define('V_EMAIL_AVAILABLE', array('email' => 'required|email|max:255|unique:users,email'));//,null,id,active_flag,0'));

define('V_EXISTS', array(
  'exists'=>"the :attribute is not registered"
));

define('V_COMPANY_CODE', array(
  'duplicate'=>'The :attribute contains duplicates.'
));

define("V_REG_USER", array (
     'name' => 'required|max:100|min:6|alpha_spaces',
     'email' => 'required|email|max:255|unique:users,email',//,null,id,active_flag,1',
     'phone' => 'required|digits:10|phone_reg',
     'password' => 'required|min:6|confirmed',
     'password_confirmation' => 'required|min:6'
));

define('V_FORGOT_PASSWORD', array('email' => 'required|email|exists:users,email', 'url'=>'required|url'));//,active_flag,1

define('V_BANK_DETAILS1', array(
  'bankname'=>'required|max:100|w_s_asp',
  'branch'=>'required|min:3|max:50|w_s_asp',
  'micr'=>'required|digits:9',
  'ifsc'=>'required|ifsc',
  'accno'=>'required|AlphaNum|min:8|max:21|confirmed',
  'accno_confirmation'=>'required|min:8',
  'acc_type'=>'required|min:7|max:10|alpha|in:Savings,Current',
  'branch_address'=>'required|address',
  'api_token' =>'required|max:300'
));

define('V_BANK_DETAILS2', array(
    'secbankname'=>'required|max:100|w_s_asp',
    'secbranch'=>"required|min:3|max:50|w_s_asp",
    'secmicr'=>'required|digits:9',
    'secifsc'=>'required|ifsc',
    'secaccno'=>'required|AlphaNum|min:8|max:21|different:accno|confirmed',
    'secacc_type'=>'required|min:7|max:10|alpha|in:Savings,Current',
    'secbranch_address'=>'required|address',
    'secaccno_confirmation'=>'required|min:8'
));
define('V_RESET_PASSWORD', array(
  'token'=>'required|min:70',
  'password' => 'required|min:6|confirmed',
  'password_confirmation' => 'required|min:6'
));

define('V_CHANGEPASSWORD', array(
  'api_token' =>'required|max:300',
  'old_password'=>'required|min:6|max:300',
  'password' => 'required|min:6|confirmed',
  'password_confirmation' => 'required|min:6'
));

define("V_LOGIN_USER", array (
      'email' => 'required|email|max:255|exists:users,email',//'exists:users,email,active_flag,1',
      'password' => 'required|min:6'
));

define('V_TRADINGPLAN', array('api_token' =>'required|max:300',
                              'equity_f'=>'in:1,0',
                              'currency_f'=>'in:1,0',
                              'commodity_f'=>'in:1,0',
                              'DIS_flag'=>'in:1,0'
                            ));

define('V_PRI_DEMAT', array(
  'demat_flag'=>'required|in:1,0',
  'demat_acc_name'=>'max:100|alpha_spaces',
  'demat_BO_no'=>'digits:8',
  'demat_DP_no'=>'AlphaNum|size:8'
));

define('V_SEC_DEMAT', array(
  'sec_demat_acc_name'=>'max:100|alpha_spaces',
  'sec_demat_BO_no'=>'digits:8|different:demat_BO_no',
  'sec_demat_DP_no'=>'AlphaNum|size:8|different:demat_DP_no'
));

define('V_G_ADHAAR_OTP', array(
  'api_token' =>'required|max:300',
  'adhaar'=>'required|digits:12'
));

define('V_P_ADHAAR_OTP', array(
  'api_token' =>'required|max:300',
  'pin'=>'required|digits:6',
  'adhaar'=>'required|digits:12',
  'txn_id'=>'required|max:45'
));

define('V_G_PAN_CVLKRA', array(
  'api_token' =>'required|max:300',
  'pan'=>'required|pan'
));

define('V_F_PAN_CVLKRA', array(
  'api_token' =>'required|max:300',
  'pan'=>'required|pan',
  'dob'=>'required|date_format:d/m/Y|before:18 years ago'
));

define('V_UPDATE_USER_DETAILS', array(
  'api_token' =>'required|max:300',
  'user_group'=>'required|in:1,2,3',
  'm_name'=>'required|max:255|alpha_spaces',
  'marital_status'=>'required|in:Married,Single',
  'addr_chk_flg'=>'required|in:0,1',
  'adhaar'=>'required|digits:12',
  'pan'=>'required|pan',
  'annual_income'=>'required|exists:cvl_kra.annual_income,description',
  'net_worth'=>'required|decimal',
  'occ_date'=>'required|date_format:d-m-Y|before:tomorrow',
  'occupation'=>'required|exists:cvl_kra.Occupation,Description',
  'inv_exp'=>'required|in:0,1',
  'fatca_flg'=>'required|in:0,1'
));

define('V_A_FIELD', array(
  'f_name' => 'required|alpha_spaces',
  'dob'=>'required|date_format:d-m-Y|before:18 years ago',
  'gender'=>'required|in:M,F',
));

define('V_INV_EXP', array(
  'exp_year'=>'required|numeric|between:0,99',
  'exp_month'=>'required|numeric|between:0,11'
));

define('V_PER_USER_DETAILS', array(
  'p_address1' =>'required|address',
  'p_address2' =>'required|address',
  'p_address3' =>'required|address',
  'p_city' =>'required|alpha_spaces',
  'p_state' =>'required|exists:cvl_kra.state,description',
  'p_pincode'=>'required|digits:6',
  'p_address_proof'=>'required|exists:cvl_kra.address_proof,description'
));

define('V_COR_USER_DETAILS', array(
  'c_address1' =>'required|address',
  'c_address2' =>'required|address',
  'c_address3' =>'required|address',
  'c_city' =>'required|alpha_spaces',
  'c_state' =>'required|exists:cvl_kra.state,description',
  'c_pincode'=>'required|digits:6',
  'c_address_proof'=>'required|exists:cvl_kra.address_proof,description'
));

define('V_FATCA1', array(
  'country1'=>'required|alpha_spaces|max:100',//|different:country2|different:country3
  'identification1'=>'required|alpha_num|max:20',//|different:identification2|different:identification3
  'identification_type1'=>'required|alpha_num|max:20'//|different:identification_type2|different:identification_type3
));

define('V_FATCA2', array(
  'country2'=>'required|alpha_spaces|max:100',//different:country3|different:country1',
  'identification2'=>'required|alpha_num|max:20',//|different:identification3|different:identification1',
  'identification_type2'=>'required|alpha_num|max:20',//different:identification_type1|different:identification_type3'
));

define('V_FATCA3', array(
  'country3'=>'required|alpha_spaces|max:100',//|different:country2|different:country1
  'identification3'=>'required|alpha_num|max:20',//|different:identification2|different:identification1
  'identification_type3'=>'required|alpha_num|max:20'//|different:identification_type2|different:identification_type1
));

// define('V_UPLOAD_IPV', array(
//   'api_token' =>'required|max:300',
//   'ipv'=>'required|mimes:pdf,jpeg,jpg,png|max:15000',
//   'pan'=>'required|pan'
// ));

define('V_DOC_REQUIRED', array(
  'api_token'=>'required|max:300',
  'pan'=>'required|pan',
  'ipv'=>'mimes:pdf,jpeg,jpg,png|max:1024',
  'pan_file'=>'required|mimes:pdf,jpeg,jpg,png|max:15000',
  'profile_picture'=>'required|mimes:pdf,jpeg,jpg,png|max:15000',
  'cancelled_cheque'=>'required|mimes:pdf,jpeg,jpg,png|max:15000',
  'sign'=>'required|mimes:pdf,jpeg,jpg,png|max:15000',
  "aadhaar"=>'required|mimes:pdf,jpeg,jpg,png|max:15000',
  'address_proof'=>"mimes:pdf,jpeg,jpg,png|max:15000",
  // 'bankstament'=>'required|array',
  // 'other_details'=>'array',
  // 'bankstament_.*'=>'required|mimes:pdf,jpeg,jpg,png|max:5120',

));

define('V_UPLOAD_DOC', array(
  'bankstatement.*'=>'required|mimes:pdf,jpeg,jpg,png|max:5120',
  'other_details.*'=>'mimes:pdf,jpeg,jpg,png|max:5120'
));

define('V_CALC_PAYMENT', array(
  'email'=>'required|email|max:255|exists:users,email',//exists:users,email,active_flag,1
  'offer'=>'alpha_dash'
));

define('V_PDFGEN', array(
  'api_token'=>'required|max:300',
  'txn_id'=>'required',
  'status'=>'required|in:Pending,Failure,Success',
  'mode'=>'in:Cheque,Online'
));

// define('V_NSDL_API', array(
//   'api_token'=>'required|max:300',
//   'pan'=>'required|pan'
// ));

define("V_ESIGN", array('api_token'=>'required|max:300','url'=>'required|url'));
