<?php
use DB;
use Exception;
use App\Identity;
use App\User;

function getDir($id)
{
    $pflag=$aflag=0;
    try {
        $user_detail=Identity::where('user_id', $id)->first();
        // if ($adhaar) {
        if ($user_detail->pan) {
            if (file_exists(public_path().'/uploads/'.($user_detail->pan))) {
                return public_path().'/uploads/'.($user_detail->pan);
            } else {
                $pflag=1;
            }
        } else {
            if (file_exists(public_path().'/uploads/'.$user_detail->adhaar_no)) {
                return public_path().'/uploads/'.$user_detail->adhaar_no;
            } else {
                    $aflag=1;
            }
        }
        // } elseif ($pan) {
        if ($user_detail->adhaar_no) {
            if (file_exists(public_path().'/uploads/'.($user_detail->adhaar_no))) {
                return public_path().'/uploads/'.($user_detail->adhaar_no);
            } else {
                $aflag=1;
            }
        } else {
            if (file_exists(public_path().'/uploads/'.$user_detail->pan)) {
                return public_path().'/uploads/'.$user_detail->pan;
            } else {
                $pflag=1;
            }
        }
        // }
        if ($pflag==1) {
            mkdir(public_path().'/uploads/'.$user_detail->pan, 0777);
            chmod(public_path().'/uploads/'.$user_detail->pan, 0777);
            return public_path().'/uploads/'.$user_detail->pan;
        }
        if ($aflag==1) {
            mkdir(public_path().'/uploads/'.$user_detail->adhaar_no, 0777);
            chmod(public_path().'/uploads/'.$user_detail->adhaar_no, 0777);
            return public_path().'/uploads/'.$user_detail->adhaar_no;
        }
        Log::error('[COMMON-F_getDir]  '.' something went wrong');
    } catch (Exception $ex) {
        Log::error('[COMMON-F_getDir] '.$ex);
    }
}

function getDir1($id)
{
    try {
        $user_detail=Identity::where('user_id', $id)->first();
        if (!empty($user_detail->pan)&&file_exists(public_path().'/uploads/'.strtoupper($user_detail->pan))) {
            return public_path().'/uploads/'.strtoupper($user_detail->pan);
        } elseif (!empty($user_detail->adhaar_no)&&file_exists(public_path().'/uploads/'.($user_detail->adhaar_no))) {
            return public_path().'/uploads/'.$user_detail->adhaar_no;
        } else {
            return getDir($id);
        }
    } catch (Exception $ex) {
        Log::error('[COMMON-F_getDir1] '.$ex);
    }
}


function uploadDoc($id, $data, $post)
{
    refreshDirectory($data);
    $input=$post->all();
    try {
        $keys=array('pan_file','cancelled_cheque','sign','ipv','aadhaar','address_proof','profile_picture');//Constant /**Single File**/
        foreach ($keys as $key) {
            if (array_key_exists($key, $input)) {
                $guessExtension = $post->file($key)->guessExtension();
                $file = $post->file($key)->move($data.'/', $key.'.'.$guessExtension);
                chmod($data.'/'.$key.'.'.$guessExtension, 0777);
            }
        }
        $keys=array('bankstatement','other_details');//Constant /**Mulitiple Files**/
        foreach ($keys as $key) {
            if (array_key_exists($key, $input)) {
                for ($i=0; $i<count($post[$key]); $i++) {
                    $guessExtension = $post->file($key.'.'.$i)->guessExtension();
                    $file = $post->file($key.'.'.$i)->move($data.'/', $key.$i.'.'.$guessExtension);
                    chmod($data.'/'.$key.$i.'.'.$guessExtension, 0777);
                }
            }
        }
        User::where('id', $id)->update(['doc_flag'=>1]);
        return [
          'msg'=>'Documents uploaded',
          'status'=>SUCCESS_STATUS,
          'msg_code'=>SUCCESS_CODE,
          'data'=>null
        ];
    } catch (Exception $ex) {
        Log::error('[COMMON-F_uploadDoc] '.$ex);
        return [
          'msg'=>'Documents upload Unsuccessful',
          'status'=>FAIL_STATUS,
          'msg_code'=>FUNCTION_CODE.'COMMON-F_uploadDoc',
          'data'=>null
        ];
    }
}

function authUser($api_token)
{
    try {
        $user = JWTAuth::toUser($api_token);
        return $user->id;
    } catch (JWTException $ex) {
        return null;
    } catch (Exception $ex) {
        Log::error(" [Commom_authUser] ".$ex);
    }
}

function convertImagesToPDF($dirname)
{
    $dirname.='/';
    $images= glob("$dirname*.png");
    $images=array_merge($images, glob("$dirname*.jpg"));
    $images=array_merge($images, glob("$dirname*.jpeg"));
    $pdf = new Imagick($images);
    $pdf->setImageFormat('pdf');
    $pdf->writeImages($dirname.'combined_images.pdf', true);
    chmod($dirname.'combined_images.pdf', 0777);
    // print_r(date('H:i:s'));//shell Logic --start--
    // $dirname.='/';
    // $images=glob($dirname."*.pdf");
    // $images=array_merge($images, glob($dirname."*.png"));
    // $images=array_merge($images, glob($dirname."*.jpg"));
    // $images=array_merge($images, glob($dirname."*.jpeg"));
    // $cmd="convert quality 100 -density 200x200 ";
    // foreach ($images as $key) {
    //     $cmd.=$key." ";
    // }
    // $cmd.=$dirname."combined_images.pdf";
    // shell_exec($cmd);
    // print_r(date('H:i:s'));
    // chmod($dirname.'combined_images.pdf', 0777);//shell Logic --end--
}

function mergePDFs($dirname)
{
    $dirname.='/';
    $fileArray= glob("{$dirname}*.pdf");
  // 	$fileArray= array("combined.pdf","pdf-sample.pdf");

    $outputName = $dirname."final_merged.pdf";

    $cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName ";

    foreach ($fileArray as $file) {
        $cmd.= $file." ";
    }
    $result = shell_exec($cmd);
    chmod($outputName, 0777);
}

function getCompanyCode()
{
    $data=DB::table('company_code_master')->select(DB::raw('segment, GROUP_CONCAT(company_code) as c_code'))
                                          ->groupBy('segment')
                                          ->get()->toArray();
    return json_decode(json_encode($data), true);
}

function getIdentityByPAN($pan,$id)
{
    try {
        return DB::table('users')
                  ->join('identity', 'users.id', "identity.user_id")
                  ->select('users.id')
                  ->where('identity.pan', strtoupper($pan))
                  ->where('users.status', "1")
                  ->where('users.id',"<>", $id)
                  ->first();//Identity::where('pan', $pan)->first();
    } catch (Exception $ex) {
          Log::error('[COMMN_getIdentityByPAN] '.$ex);
    }
}

function updatePan($id, $pan)
{
    try {
        $data=Identity::where('user_id', $id)->update(['pan'=>strtoupper($pan)]);
        if (!$data) {
            return [
              'status'=>FAIL_STATUS,
              'msg'=>'Unable to update pan',
              'msg_code'=>FUNCTION_CODE.'COMMON-F_updatePan',
              'data'=>null
            ];
        }
        return [
          'status'=>SUCCESS_STATUS,
          'data'=>$data
        ];
    } catch (Exception $ex) {
        Log::error('[COMMN_getIdentityByPAN] '.$ex);
    }
}

function refreshDirectory($dir_path)
{
    $key1=array('pan_file','cancelled_cheque','sign','ipv','aadhaar','address_proof','profile_picture');
    $key2=array('bankstatement','other_details');
    foreach($key1 as $key => $value) {
        $s=glob($dir_path."/".$value.".*");
        foreach ($s as $e => $value) {
            unlink($value);
        }
    }
    foreach($key2 as $key => $value) {
        $m=glob($dir_path."/"."$value*".'*');
        foreach ($m as $e => $value) {
            unlink($value);
        }
    }
}
