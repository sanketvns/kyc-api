<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identity extends Model
{
    protected $table='identity';

    protected $fillable=[
      'user_id','otp_timestamp','otp_txn_id','pan','adhaar_no','client_code'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
