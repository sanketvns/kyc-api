<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Esign extends Model
{
    protected $table='esign';

    protected $fillable=['user_id','Returnvalue','FileType','Referencenumber','Transactionnumber',
    'ReturnStatus','ErrorMessage','callbackurl'];
}
