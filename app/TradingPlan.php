<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TradingPlan extends Model
{
    protected $table = 'user_tradeplan_mapping';

    protected $fillable = [
      'user_id','equity_flag','commodity_flag','currency_flag','equity','commodity','currency',
      'plan_equity','plan_commodity','plan_currency','demat_name','demat_dp','demat_bo',
      'sec_demat_name','sec_demat_dp','sec_demat_bo','demat_flag','DIS_flag'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
