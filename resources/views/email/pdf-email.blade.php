<html>
<head>
</head>
<body
	style="background-color: #E4E4E4; margin: 0; width: 100%; min-width: 640px; font-family: Arial, Helvetica, sans-serif;">
	<div align="center" style="padding: 20px;">
		<table border="0" cellspacing="0" width="720px"
			style="color: #262626; background-color: #fff; position: absolute; width: 100%; height: 100%; padding: 30px 30px 30px 30px; margin: auto; border: 0px solid #e1e1e1; color: #666; font-size: 12px; margin: 0px; border: #ccc 0px solid; -moz-border-radius: 3px; -webkit-border-radius: 3px; border-radius: 3px; -moz-box-shadow: 0 1px 2px #d1d1d1; -webkit-box-shadow: 0 1px 2px #d1d1d1; box-shadow: 0 1px 2px #d1d1d1;">
			<tbody>
				<tr class="headtd">
					<td>
						<div
							style="padding-left: 0px; background: #FFFFFF; color: white; border: 2px solid #00618A; border-radius: 5px;">
							<img height="59px"
								style="margin-bottom: 5px; display: block; margin-left: auto; margin-right: auto;"
								src="https://tradesmartonline.in/wp-content/uploads/2015/06/tso_logo.png">
						</div>
					</td>
				</tr>
			</tbody>
			<tbody>
				<tr>
					<td
						style="padding: 10px 0 0 0; background: #FFFFFF; vertical-align: top; font: 14px/22px Verdana, Arial, Helvetica, sans-serif; color: #444444;">
						<p
							style="font-size: 14px; line-height: 24px; margin-top: 20px; font: 14px/22px Verdana, Arial, Helvetica, sans-serif;">


		Hello <?php print_r(ucwords(strtolower($data)));?>,<br /><br/>
		
		Thank you for your interest in opening an account with TradeSmart Online.<br/> <br />We have attached your account opening form along with the supporting documents with this email.
 
  You will receive your account opening details including your client id and password via email once your form and documents are checked and approved.
 
 <br /><br/>
 <b>Important:</b> Please note that to be able to sell shares in cash segment, you are required to send the <a href="http://help.tradesmartonline.in/wp-content/uploads/2014/05/Power-of-Attorney.pdf" download><b style="background-color:yellow">"Power of Attorney"</b></a> 
 form separately via hard copy to the below given address. <a target="_blank" href="http://help.tradesmartonline.in/why-is-a-power-of-attorney-poa-for-demat-account-required/">Click here</a> to learn why we need the Power of Attorney for your demat account.
 
 <br /><br/>
VNS Finance & Capital Services Ltd<br/>
A-401, Mangalya, Marol<br/>
Marol Maroshi Road<br/>
Andheri(East)<br/>
Mumbai – 400059<br/>
 
 
<br/>
You can also download and send us the below document if required:<br/>
<ul>
<?php /*?>
<li>
<b style="background-color:yellow">Add/change your bank account</b> registered with us: If you'd like to map more than one bank accounts with your trading account.
</li>
<?php */?>
<li>
<b><a download href="http://help.tradesmartonline.in/wp-content/uploads/2014/05/Nomination.pdf" style="background-color:yellow">Nomination form</a></b>: Applicable only if you'd like to nominate someone for your demat account. 
</li>
</ul> 
 
 
 
  <br/>For further assistance please contact us at 022-42878000/61208000 or email us at contactus@vnsfin.com.
		</td>
				</tr>
				
					<tr>
					<td
						style="padding: 10px 0 0 0; background: #FFFFFF; vertical-align: top; font: 14px/22px Verdana, Arial, Helvetica, sans-serif; color: #444444;">
						<p
							style="font-size: 14px; line-height: 24px; margin-top: 20px; font: 14px/22px Verdana, Arial, Helvetica, sans-serif;">
					 Cheers!<br> TradeSmart Online<br> Ph. 022-42878000/61208000
					</td>
				</tr>
				
				

				<tr align="center"
					style="align: center; margin: 0px; align-content: center; margin-left: auto; margin-right: auto;">
					<td style="margin: 0px; padding: 0px; align-content: center;">
						<div align="center"
							style="align-content: center; align: center; margin-left: auto; margin-right: auto; font: 14px/22px Verdana, Arial, Helvetica, sans-serif; color: #FFFFFF; background: #00618A; padding: 10px; font-size: 11px; font-weight: bold; border-radius: 5px;">
							<p style="align: center; align-content: center;">
								VNS Finance & Capital Service LTD<br> A-401/402 Mangalya, Marol
								Maroshi Road, opp Marol Fire Brigade, Andheri (E).<br> Tel. - 022
								42878000 / 61208000
							</p>
						</div>
					</td>
				</tr>

			</tbody>
		</table>
	</div>
</body>
</html>



